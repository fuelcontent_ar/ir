<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$adminSections = array(
    array('title' => 'Coverage', 'sections' => array(
        array('title' => 'A. C. Equity', 'link' => 'acequity', 'active' => false),
        array('title' => 'A. C. Fixed Income', 'link' => 'acfixedincome', 'active' => false)
    )),
    array('title' => 'Financials', 'sections' => array(
        array('title' => 'Sec. Filings', 'link' => 'secfilings', 'active' => false),
        array('title' => 'Finanzas', 'link' => 'financials', 'active' => false),
        array('title' => 'CVM', 'link' => 'cvm', 'active' => false),
        array('title' => 'AGM', 'link' => 'agm', 'active' => false),
        array('title' => 'Popup', 'link' => 'popup', 'active' => false),
        array('title' => 'Entrevistas', 'link' => 'entrevistas', 'active' => false)
    )),
    array('title' => 'Education', 'sections' => array(
        array('title' => 'Factsheets', 'link' => 'factsheets', 'active' => false),
        array('title' => 'Our Numbers', 'link' => 'ournumbers', 'active' => false),
        array('title' => 'Inst. Presentation', 'link' => 'ipresentations', 'active' => false),
        array('title' => 'I. Education', 'link' => 'ieducation', 'active' => false),
        array('title' => 'Gráficos', 'link' => 'graficos', 'active' => false),
        array('title' => 'Números', 'link' => 'numeros', 'active' => false),
        array('title' => 'I. Day', 'link' => 'iday', 'active' => false),
        array('title' => 'Our Assets', 'link' => 'ourassets', 'active' => false),
        array('title' => 'Header', 'link' => 'header', 'active' => false)
    )),
    array('title' => 'News', 'sections' => array(
        array('title' => 'Noticias', 'link' => 'noticias', 'active' => false),
        array('title' => 'Calendar', 'link' => 'events', 'active' => false)
    ))
);

$route = str_replace('panel/', '', \Request::path());
if (strpos($route, '/') !== false)
    $route = substr($route, 0, strpos($route, '/'));
if ($route == 'quarterly')
    $route = 'financials';
    
foreach ($adminSections as $k => $group)
    foreach ($group['sections'] as $index => $section)
        if ($section['link'] == $route) {
            $adminSections[$k]['sections'][$index]['active'] = true;
            break;
        }

$frontSections = array(
    '/company/business',
    '/company/education',
    '/company/investor',
    '/company/numbers',
    '/company/organizational',
    '/company/presentation',
    '/company/strategy',
    '/contacto',
    '/calendar',
    '/policy',
    '/terms',
    '/financials/analyst',
    '/financials/cvm',
    '/financials/filings',
    '/financials/quarterly-earnings',
    '/governance/board-of-directors',
    '/governance/annual',
    '/governance/bylaws',
    '/governance/ethics',
    '/governance/insider',
    '/governance/officers',
    '/governance/policy',
    '/our-assets'
);
View::share('sections', $adminSections);

foreach ($frontSections as $section)
    Route::get($section, function () use ($section) {
        return view('front' . $section);
    });

Route::get('/', function () {
    return view('front/index');
});

Route::middleware('auth')->prefix('/panel')->group(function () {
    Route::resources([
        'acequity' => AcEquityController::class,
        'acfixedincome' => AcFixedIncomeController::class,
        'agm' => AnnualGeneralMeetingsController::class,
        'cvm' => CVMController::class,
        'events' => EventsController::class,
        'graficos' => GraficosController::class,
        'header' => HeaderController::class,
        'iday' => InvestorDayController::class,
        'ieducation' => InvestorEducationController::class,
        'noticias' => NoticiasController::class,
        'secfilings' => SecFilingsController::class
    ]);

    Route::resource('entrevistas', EntrevistaController::class)->only(['index', 'edit', 'update']);
    Route::resource('factsheets', FactsheetController::class)->only(['index', 'edit', 'update']);
    Route::resource('financials', FinancialFilesController::class)->only(['index', 'edit', 'update']);
    Route::resource('ipresentations', InstitutionalPresentationController::class)->only(['index', 'edit', 'update']);
    Route::resource('legales', LegalesController::class)->only(['update']);
    Route::resource('numeros', NumerosController::class)->only(['index', 'edit', 'update']);
    Route::resource('ourassets', OurAssetsController::class)->only(['index', 'edit', 'update']);
    Route::resource('ournumbers', OurNumberController::class)->only(['index', 'edit', 'update']);
    Route::resource('popup', PopupController::class)->only(['index', 'edit', 'update']);
    Route::resource('quarterly', FinancialQuartersController::class)->only([
        'create', 'store', 'edit', 'update', 'destroy'
    ]);

    Route::get('/', '\App\Http\Controllers\AcEquityController@index');
    Route::get('/downloadFile', '\App\Http\Controllers\FileDownloadController@download');

    // Route::get('/importQuarters', '\App\Http\Controllers\FinancialQuartersController@import');
});

Auth::routes();