<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

$controller = '\App\Http\Controllers\EndpointController';
Route::get('/acequity', $controller . '@acequity');
Route::get('/acfixedincome', $controller . '@acfixedincome');
Route::get('/annualGeneralMeetings', $controller . '@annualGeneralMeetings');
Route::post('/contact',  $controller . '@contact');
Route::get('/cvm', $controller . '@cvm');
Route::get('/entrevistas',  $controller . '@entrevistas');
Route::get('/events',  $controller . '@events');
Route::get('/factsheets',  $controller . '@factsheets');
Route::get('/graficos',  $controller . '@graficos');
Route::get('/headers',  $controller . '@headers');
Route::get('/institutionalPresentation', $controller . '@institutionalPresentation');
Route::get('/investorDay', $controller . '@investorDay');
Route::get('/investorEducation', $controller .'@investorEducation');
Route::get('/noticias',  $controller . '@noticias');
Route::get('/numeros',  $controller . '@numeros');
Route::get('/ourAssets', $controller . '@ourAssets');
Route::get('/ourNumbers', $controller . '@ourNumbers');
Route::get('/popup', $controller . '@popup');
Route::get('/quarterlyEarnings',  $controller . '@quarterlyEarnings');
Route::get('/SECFilings',  $controller . '@SECFilings');
