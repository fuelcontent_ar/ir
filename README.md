# IR
Instrucciones de instalación para el proyecto.

## Códigos a ejecutar para la instalación y testeo en local
```
git clone https://bitbucket.org/fuelcontent_ar/ir/src/master/
composer install
npm install
php artisan migrate
php artisan db:seed
```

### Testeo
```
php artisan serve
npm run watch
```

## RUTAS CREADAS
```
/company/business
/company/education
/company/investor
/company/numbers
/company/organizational
/company/presentation
/company/strategy
/contacto
/financials/analyst
/financials/cvm
/financials/filings
/financials/quarterly-earnings
/governance/board-of-directors
/governance/annual
/governance/bylaws
/governance/ethics
/governance/insider
/governance/officers
/governance/policy
/our-assets
```

## API ENDPOINTS
```
/api/acequity: devuelve los registros para "Financials -> Analyst Coverage -> Equity'.
/api/acfixedincome: devuelve los registros para "Financials -> Analyst Coverage -> Fixed Income'.
/api/annualGeneralMeetings: devuelve la lista de archivos correspondiente a la sección "Annual General Meetings".
/api/contact: POST endpoint para enviar emails. Recibe first_name, last_name, email, phone, company y message.
/api/cvm?year=XXXX: devuelve los registros para la sección "Financials >> CVM" con especificación opcional del año.
/api/events?year=XXXX: devuelve los eventos a ser impactados en el calendario con filtrado opcional por año.
/api/entrevistas: devuelve la entrevista de la sección "Financials".
/api/factsheets: devuelve el registro Factsheet.
AGREGAR GRAFICOS Y NOTICIAS
/api/graficos: devuelve el listado de gráficos del home.
/api/headers: devuelve todos los headers
/api/noticias: devuelve el listado de noticias ordenado por fecha descendiente (sección Home).
/api/numeros: devuelve los seis números animados del index
/api/quarterlyEarnings?lastQuarter=1: devuelve las entradas para la sección "Quarter Release Pack" del index y para Conference Call, Financial Statements y Earnings Release de la sección "Financials".
/api/quarterlyEarnings?year=XXXX: devuelve las entradas para la sección "Financials >> Quarterly Earnings" agrupadas por cuatrimestre.
/api/quarterlyEarnings?getYears=1: devuelve los años para los cuales hay registros ordenados por orden descendiente.
/api/institutionalPresentation: devuelve el archivo correspondiente a la sección "Institutional Presentation".
/api/investorDay: devuelve los registros de la sección Investor Day. Los campos fileType y fileType2 pueden devolver "doc", "xls", "pdf" o "audio" (en base a este valor se puede elegir el ícono a usar en el front). Si el campo "video" no está vacío, debe tomarse su valor para incrustar el video de Vimeo en el formato https://vimeo.com/VIDEO_ID/.
/api/investorEducation: devuelve los registros de la sección Investor Education. El campo fileType puede devolver "doc", "xls", "pdf" o "audio" (en base a este valor se puede elegir el ícono a usar en el front). Si el campo "video" no está vacío, debe tomarse su valor para incrustar el video de Vimeo en el formato https://vimeo.com/VIDEO_ID/.
/api/ourAssets: devuelve el listado de archivos rrespondiente a la sección "Our Assets".
/api/ourNumbers: devuelve el archivo correspondiente a la sección "Our Numbers".
/api/popup: devuelve los datos del popup.
/api/SECFilings?year=XXXX: devuelve los SEC Filings con especificación opcional del año.
```