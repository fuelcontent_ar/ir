<?php

namespace App\Http\Controllers;

use App\SecFiling;
use Illuminate\Http\Request;
use Carbon\Carbon;

class SecFilingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = 
        return view('panel/secfilings')->with('entries', SecFiling::orderBy('date', 'desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $t = new SecFiling;
        return view('panel/secfilings')->with(['edit' => true, 'entry' => new SecFiling]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->update($request, new SecFiling);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AcEquity  $acEquity
     * @return \Illuminate\Http\Response
     */
    public function show(SecFiling $secfiling)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AcEquity  $acEquity
     * @return \Illuminate\Http\Response
     */
    public function edit(SecFiling $secfiling)
    {
        $secFiling = SecFiling::findOrFail($secfiling->id);
        // $secFiling->date = \Carbon\Carbon::parse($secfiling->date)->format('d-m-Y');
        return view('panel/secfilings')->with(['edit' => true, 'entry' => $secFiling]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AcEquity  $acEquity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SecFiling $secfiling)
    {
        $validated = $request->validate([
            'date' => 'required|date_format:m-d-Y',
            'type' => 'required|filled',
            'description' => 'nullable',
            'link' => 'required|url'
        ]);

        $error = '';
        $count = $secfiling->id > 0 ? SecFiling::where(function ($query) use ($request) {
            $query->where('link', $request->link);
        })->where('id', '!=', $secfiling->id)->count() : 0;

        if ($count > 0)
            $error = 'Ya existe otro registro con algún dato en común.';
        else {
            $data = array('date' => Carbon::createFromFormat('m-d-Y', $request->date),
            'type' => $request->type,
            'description' => $request->description,
            'link' => $request->link
            );

            $state = SecFiling::updateOrCreate(['id' => $secfiling->id], $data);
            if ($state) {
                $action = $secfiling->id > 0 ? 'actualizado' : 'agregado';
                return redirect('panel/secfilings')->with('success', 'Registro ' . $action . ' con éxito');
            }
            else
                $error = 'Ocurrió un error al actualizar el registro.';
        }

        return back()->with('error', $error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AcEquity  $acEquity
     * @return \Illuminate\Http\Response
     */
    public function destroy(SecFiling $secfiling)
    {
        $secFiling = SecFiling::findOrFail($secfiling->id);
        $secFiling->delete();
        return back()->with('success', 'Registro borrado con éxito.');
    }
}
