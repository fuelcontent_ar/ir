<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileDownloadController extends Controller
{
    function download(Request $request) {
        $file = str_replace(array('..'), '', $request->file);
        $storagePath  = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix();
        
        if ($file[0] == '/')
            $file = substr($file, 1);

        if (substr_count($file, '/') > 1)
            return 'Operación denegada.';

        $file = $storagePath . $file;
        if (file_exists($file)) {
            $name = basename($file);
            return response()->download($file, $name);
        }
    }
}
