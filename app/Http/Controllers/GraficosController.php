<?php

namespace App\Http\Controllers;

use App\Grafico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GraficosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('panel/graficos')->with('entries', Grafico::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel/graficos')->with('entry', new Grafico);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->update($request, new Grafico);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AnnualGeneralMeeting  $annualGeneralMeeting
     * @return \Illuminate\Http\Response
     */
    public function edit(Grafico $grafico)
    {
        $grafico = Grafico::findOrFail($grafico->id);
        return view('panel/graficos')->with(['edit' => true, 'entry' => $grafico]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnnualGeneralMeeting  $annualGeneralMeeting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Grafico $grafico)
    {
        $requirements = ['title' => 'required'];
        
        if (!$grafico->exists)
            $requirements['input'] = 'required|image|mimes:jpeg,jpg,png|max:2048';
        else
            $requirements['input'] = 'image|mimes:jpeg,jpg,png|max:2048';
            
        $validated = $request->validate($requirements);
        $error = '';
        $action = $grafico->exists ? 'actualizar' : 'agregar';
        $done = $grafico->exists ? 'actualizado' : 'agregado';

        $grafico->title = $request->title;
        // REFS
        $previousName = $grafico->exists ? grafico::where('id', $grafico->id)->value('file') : '';
        
        if ($request->file('input')) {
            // var_dump($request->file('input'));
            $grafico->file = $request->file('input')->getClientOriginalName();

            if (!empty($previousName))
                Storage::disk('uploads')->delete('graficos/' . $previousName);
            
            Storage::disk('uploads')->putFileAs(
                'graficos/', $request->file('input'), $grafico->file
            );
        }
        
        if ($grafico->save()) {
            return redirect('panel/graficos')->with('success', 'Registro ' . $done . ' con éxito');
        }
        else
            $error = 'Ocurrió un error al ' . $action . ' el registro.';

        return back()->with('error', $error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AnnualGeneralMeeting  $annualGeneralMeeting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Grafico $grafico)
    {
        $grafico = Grafico::findOrFail($grafico->id);
        $grafico->delete();
        Storage::disk('uploads')->delete('graficos/' . $grafico->file);

        return redirect('panel/graficos')->with('success', 'Registro borrado con éxito.');
    }
}
