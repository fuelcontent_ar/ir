<?php

namespace App\Http\Controllers;

use App\OurNumber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OurNumberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('panel/ournumbers')->with('entries', OurNumber::get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function edit(OurNumber $ournumber)
    {
        $ournumber = OurNumber::findOrFail($ournumber->id);
        return view('panel/ournumbers')->with(['edit' => true, 'entry' => $ournumber]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OurNumber $ournumber)
    {
        $requirements = [
            'input' => 'required|file|max:51200'
        ];

        $validated = $request->validate($requirements);
        $error = '';

        $previousName = $ournumber->exists ? $ournumber->file : '';

        $ournumber->file = $request->file('input')->getClientOriginalName();

        Storage::disk('uploads')->delete('our_numbers/' . $previousName);
        Storage::disk('uploads')->putFileAs('our_numbers/', $request->file('input'), $ournumber->file);

        if ($ournumber->save()) {
            return redirect('panel/ournumbers')->with('success', 'Registro actualizado con éxito');
        }
        else
            $error = 'Ocurrió un error al actualizar el registro.';

        return back()->with('error', $error);
    }
}
