<?php

namespace App\Http\Controllers;

use App\Numero;
use App\Legales;
use Illuminate\Http\Request;

class NumerosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('panel/numeros')->with('entries', Numero::get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AcEquity  $acEquity
     * @return \Illuminate\Http\Response
     */
    public function edit(Numero $numero)
    {
        $numero = Numero::findOrFail($numero->id);
        return view('panel/numeros')->with(['edit' => true, 'entry' => $numero]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AcEquity  $acEquity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Numero $numero)
    {
        $validated = $request->validate([
            'title' => 'required',
            'number' => 'required|numeric',
        ]);
        $error = '';
        $count = $numero->id > 0 ? numero::where(function ($query) use ($request) {
            $query->where('title', $request->title);
        })->where('id', '!=', $numero->id)->count() : 0;

        if ($count > 0)
            $error = 'Ya existe otro registro con algún dato en común.';
        else {
            $data = array('title' => $request->title,
            'number' => $request->number
            );

            $state = Numero::updateOrCreate(['id' => $numero->id], $data);
            if ($state) {
                $action = $numero->id > 0 ? 'actualizado' : 'agregado';
                return redirect('panel/numeros')->with('success', 'Registro ' . $action . ' con éxito');
            }
            else
                $error = 'Ocurrió un error al actualizar el registro.';
        }

        return back()->with('error', $error);
    }
}
