<?php

namespace App\Http\Controllers;

use App\InvestorDay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class InvestorDayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('panel/iday')->with('entries', InvestorDay::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel/iday')->with('entry', new InvestorDay);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->update($request, new InvestorDay);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AnnualGeneralMeeting  $annualGeneralMeeting
     * @return \Illuminate\Http\Response
     */
    public function edit(InvestorDay $iday)
    {
        $iday = InvestorDay::findOrFail($iday->id);
        return view('panel/iday')->with(['edit' => true, 'entry' => $iday]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnnualGeneralMeeting  $annualGeneralMeeting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvestorDay $iday)
    {
        $requirements = ['title' => 'required',
        'description' => 'required|filled',
        'video' => 'numeric|nullable'];
        
        if (!$iday->exists) {
            // $requirements['input'] = 'file|required|max:51200';
            $requirements['thumb'] = 'required|image|mimes:jpeg,jpg,png|max:2048';
        }
        else
            $requirements['thumb'] = 'image|mimes:jpeg,jpg,png|max:2048';
        
        $validated = $request->validate($requirements);
        $error = '';
        $action = $iday->exists ? 'actualizar' : 'agregar';
        $done = $iday->exists ? 'actualizado' : 'agregado';

        $iday->title = $request->title;
        $iday->description = $request->description;
        $iday->video = $request->video;
        $previousThumbName = $iday->exists ? $iday->thumb : '';
        $previousFileName = $iday->exists ? $iday->file : '';
        $previousFile2Name = $iday->exists ? $iday->file2 : '';
        
        if ($request->file('input')) {
            $iday->file = $request->file('input')->getClientOriginalName();

            if (!empty($previousFileName))
                Storage::disk('uploads')->delete('iday/' . $previousFileName);
            
            Storage::disk('uploads')->putFileAs(
                'iday/', $request->file('input'), $iday->file
            );
        }

        if ($request->file('input2')) {
            $iday->file2 = $request->file('input2')->getClientOriginalName();

            if (!empty($previousFile2Name))
                Storage::disk('uploads')->delete('iday/' . $previousFile2Name);
            
            Storage::disk('uploads')->putFileAs(
                'iday/', $request->file('input2'), $iday->file2
            );
        }

        if ($request->file('thumb')) {
            $iday->thumb = $request->file('thumb')->getClientOriginalName();

            if (!empty($previousThumbName))
                Storage::disk('uploads')->delete('iday/' . $previousThumbName);
            
            Storage::disk('uploads')->putFileAs(
                'iday/', $request->file('thumb'), $iday->thumb
            );
        }
        
        if ($iday->save()) {
            return redirect('panel/iday')->with('success', 'Registro ' . $done . ' con éxito');
        }
        else
            $error = 'Ocurrió un error al ' . $action . ' el registro.';

        return back()->with('error', $error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AnnualGeneralMeeting  $annualGeneralMeeting
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvestorDay $iday)
    {
        $iday = InvestorDay::findOrFail($iday->id);
        
        if (!empty($iday->file))
            Storage::disk('uploads')->delete('iday/' . $iday->file);
        if (!empty($iday->file2))
            Storage::disk('uploads')->delete('iday/' . $iday->file2);
        
        Storage::disk('uploads')->delete('iday/' . $iday->thumb);
        $iday->delete();
        return redirect('panel/iday')->with('success', 'Registro borrado con éxito.');
    }
}
