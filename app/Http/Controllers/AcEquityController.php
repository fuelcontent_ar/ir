<?php

namespace App\Http\Controllers;

use App\AcEquity;
use App\Legales;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AcEquityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('panel/acequity')->with('entries', AcEquity::orderBy('date', 'desc')->get())
        ->with('legales', Legales::get()->first());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel/acequity')->with(['edit' => true, 'entry' => new AcEquity]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->update($request, new AcEquity);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AcEquity  $acEquity
     * @return \Illuminate\Http\Response
     */
    public function show(AcEquity $acEquity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AcEquity  $acEquity
     * @return \Illuminate\Http\Response
     */
    public function edit(AcEquity $acequity)
    {
        $acEquity = AcEquity::findOrFail($acequity->id);
        return view('panel/acequity')->with(['edit' => true, 'entry' => $acEquity]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AcEquity  $acEquity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AcEquity $acequity)
    {
        $validated = $request->validate([
            'firm' => 'required|filled',
            'analyst' => 'required|filled',
            'phone' => 'required|min:8|max:24|regex:/^([0-9 \+\-\(\)]+)$/',
            'email' => 'required|email',
            'recommendation' => 'nullable',
            'tp' => 'required|filled',
            'date' => 'required|date_format:m-d-Y',
        ]);
        $error = '';
        $count = $acequity->id > 0 ? AcEquity::where(function ($query) use ($request) {
            $query->where('firm', $request->firm);
        })->where('id', '!=', $acequity->id)->count() : 0;

        if ($count > 0)
            $error = 'Ya existe otro registro con algún dato en común.';
        else {
            $data = array('firm' => $request->firm,
            'analyst' => $request->analyst,
            'phone' => $request->phone,
            'email' => $request->email,
            'recommendation' => $request->recommendation,
            'tp' => $request->tp,
            'date' => Carbon::createFromFormat('m-d-Y', $request->date)
            );

            $state = AcEquity::updateOrCreate(['id' => $acequity->id], $data);

            if ($state) {
                $action = $acequity->id > 0 ? 'actualizado' : 'agregado';
                return redirect('panel/acequity')->with('success', 'Registro ' . $action . ' con éxito');
            }
            else
                $error = 'Ocurrió un error al actualizar el registro.';
        }

        return back()->with('error', $error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AcEquity  $acEquity
     * @return \Illuminate\Http\Response
     */
    public function destroy(AcEquity $acequity)
    {
        $acequity = AcEquity::findOrFail($acequity->id);
        $acequity->delete();
        return back()->with('success', 'Registro borrado con éxito.');
    }
}
