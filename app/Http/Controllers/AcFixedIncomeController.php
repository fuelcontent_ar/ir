<?php

namespace App\Http\Controllers;

use App\AcFixedIncome;
use App\Legales;
use Illuminate\Http\Request;

class AcFixedIncomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('panel/acfixedincome')->with('entries', AcFixedIncome::get())
        ->with('legales', Legales::get()->first());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel/acfixedincome')->with(['edit' => true, 'entry' => new AcFixedIncome]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->update($request, new AcFixedIncome);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AcEquity  $acEquity
     * @return \Illuminate\Http\Response
     */
    public function show(AcFixedIncome $acfixedincome)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AcEquity  $acEquity
     * @return \Illuminate\Http\Response
     */
    public function edit(AcFixedIncome $acfixedincome)
    {
        $acFixed = AcFixedIncome::findOrFail($acfixedincome->id);
        return view('panel/acfixedincome')->with(['edit' => true, 'entry' => $acFixed]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AcEquity  $acEquity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AcFixedIncome $acfixedincome)
    {
        $validated = $request->validate([
            'firm' => 'required|filled',
            'analyst' => 'required|filled',
            'phone' => 'required|min:8|max:24|regex:/^([0-9 \+\-\(\)]+)$/',
            'email' => 'required|email'
        ]);
        $error = '';
        $count = $acfixedincome->id > 0 ? AcFixedIncome::where(function ($query) use ($request) {
            $query->where('firm', $request->firm);
        })->where('id', '!=', $acfixedincome->id)->count() : 0;

        if ($count > 0)
            $error = 'Ya existe otro registro con algún dato en común.';
        else {
            $data = array('firm' => $request->firm,
            'analyst' => $request->analyst,
            'phone' => $request->phone,
            'email' => $request->email
            );
            /* $acequity->firm = $request->firm;
            $acequity->analyst = $request->analyst;
            $acequity->phone = $request->phone;
            $acequity->email = $request->email;
            $acequity->recommendation = $request->recommendation;
            $acequity->tp = $request->tp;
            $acequity->date = $request->date; */

            $state = AcFixedIncome::updateOrCreate(['id' => $acfixedincome->id], $data);
            if ($state) {
                $action = $acfixedincome->id > 0 ? 'actualizado' : 'agregado';
                return redirect('panel/acfixedincome')->with('success', 'Registro ' . $action . ' con éxito');
            }
            else
                $error = 'Ocurrió un error al actualizar el registro.';
        }

        return back()->with('error', $error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AcEquity  $acEquity
     * @return \Illuminate\Http\Response
     */
    public function destroy(AcFixedIncome $acfixedincome)
    {
        $acfixed = AcFixedIncome::findOrFail($acfixedincome->id);
        $acfixed->delete();
        return back()->with('success', 'Registro borrado con éxito.');
    }
}
