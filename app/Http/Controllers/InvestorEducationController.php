<?php

namespace App\Http\Controllers;

use App\InvestorEducation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class InvestorEducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('panel/ieducation')->with('entries', InvestorEducation::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel/ieducation')->with('entry', new InvestorEducation);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->update($request, new InvestorEducation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AnnualGeneralMeeting  $annualGeneralMeeting
     * @return \Illuminate\Http\Response
     */
    public function edit(InvestorEducation $ieducation)
    {
        $ieducation = InvestorEducation::findOrFail($ieducation->id);
        return view('panel/ieducation')->with(['edit' => true, 'entry' => $ieducation]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnnualGeneralMeeting  $annualGeneralMeeting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvestorEducation $ieducation)
    {
        $requirements = ['title' => 'required',
        'description' => 'required|filled',
        'video' => 'numeric'];
        
        if (!$ieducation->exists) {
            // $requirements['input'] = 'file|required|max:51200';
            $requirements['thumb'] = 'required|image|mimes:jpeg,jpg,png|max:2048';
        }
        else
            $requirements['thumb'] = 'image|mimes:jpeg,jpg,png|max:2048';
        
        $validated = $request->validate($requirements);
        $error = '';
        $action = $ieducation->exists ? 'actualizar' : 'agregar';
        $done = $ieducation->exists ? 'actualizado' : 'agregado';

        $ieducation->title = $request->title;
        $ieducation->description = $request->description;
        $ieducation->video = $request->video;
        $previousFileName = $ieducation->exists ? $ieducation->file : '';
        $previousThumbName = $ieducation->exists ? $ieducation->thumb : '';
        
        if ($request->file('input')) {
            $ieducation->file = $request->file('input')->getClientOriginalName();

            if (!empty($previousFileName))
                Storage::disk('uploads')->delete('ieducation/' . $previousFileName);
            
            Storage::disk('uploads')->putFileAs(
                'ieducation/', $request->file('input'), $ieducation->file
            );
        }

        if ($request->file('thumb')) {
            $ieducation->thumb = $request->file('thumb')->getClientOriginalName();

            if (!empty($previousThumbName))
                Storage::disk('uploads')->delete('ieducation/' . $previousThumbName);
            
            Storage::disk('uploads')->putFileAs(
                'ieducation/', $request->file('thumb'), $ieducation->thumb
            );
        }
        
        if ($ieducation->save()) {
            return redirect('panel/ieducation')->with('success', 'Registro ' . $done . ' con éxito');
        }
        else
            $error = 'Ocurrió un error al ' . $action . ' el registro.';

        return back()->with('error', $error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AnnualGeneralMeeting  $annualGeneralMeeting
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvestorEducation $ieducation)
    {
        $ieducation = InvestorEducation::findOrFail($ieducation->id);
        
        if (!empty($ieducation->file))
            Storage::disk('uploads')->delete('ieducation/' . $ieducation->file);
        
        Storage::disk('uploads')->delete('ieducation/' . $ieducation->thumb);
        $ieducation->delete();
        return redirect('panel/ieducation')->with('success', 'Registro borrado con éxito.');
    }
}
