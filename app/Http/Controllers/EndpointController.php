<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\AcEquity;
use App\AcFixedIncome;
use App\AnnualGeneralMeeting;
use App\CVM;
use App\Entrevista;
use App\Event;
use App\FinancialQuarter;
use App\Header;
use App\Factsheet;
use App\Grafico;
use App\institutionalPresentation;
use App\InvestorDay;
use App\InvestorEducation;
use App\Noticia;
use App\Numero;
use App\OurAsset;
use App\OurNumber;
use App\Popup;
use App\SecFiling;
use App\Mail\ContactForm;
use Illuminate\Support\Facades\Mail;
use DB;

class EndpointController extends Controller
{
    public function acequity() {
        $data = AcEquity::get();

        return response()->json($data);
    }

    public function acfixedincome() {
        $data = AcFixedIncome::get();

        return response()->json($data);
    }

    public function annualGeneralMeetings(Request $request) {
        $agm = AnnualGeneralMeeting::get();
        
        foreach ($agm as $k => $row)
            $agm[$k]->file = Storage::disk('uploads')->url('agm/' . $row->file);

        return response()->json($agm);
    }

    public function contact(Request $request) {
        $entry = new \stdClass;
        $entry->date = date("Y-m-d H:i:s");
        $entry->email = $request->email;
        $entry->full_name = $request->first_name . ' ' . $request->last_name;
        $entry->phone = $request->phone;
        $entry->message = $request->message;
        $entry->company = $request->company;
        
        // Mail::to('ir@adecoagro.com')->send($entry);
        Mail::to('ir@adecoagro.com')->queue(new ContactForm($entry));

        return response()->json(array('state' => 'ok'));
    }

    public function cvm(Request $request) {
        $year = $request->year ?? false;
        $data = CVM::when($year, function ($query) use ($year) {
            return $query->whereYear('date', $year);
        })->orderBy('date', 'desc')->get();

        foreach ($data as $k => $row)
            $data[$k]->file = Storage::disk('uploads')->url('cvm/' . $row->file);

        return response()->json($data);
    }

    public function entrevistas() {
        $data = Entrevista::orderBy('date', 'desc')->get()->first();
        $data->file = Storage::disk('uploads')->url('entrevistas/' . $data->file);
        
        return response()->json($data);
    }

    public function events(Request $request) {
        $year = $request->year ?? false;
        $data = Event::when($year, function ($query) use ($year) {
            return $query->whereYear('date', $year);
        })->orderBy('date', 'desc')->get();

        return response()->json($data);
    }

    public function factsheets() {
        $factsheet = Factsheet::get()->first();
        $factsheet->file = Storage::disk('uploads')->url('factsheets/' . $factsheet->file);
        return response()->json($factsheet);
    }

    public function graficos() {
        $graficos = Grafico::get();

        foreach ($graficos as $k => $row)
            $graficos[$k]->file = Storage::disk('uploads')->url('graficos/' . $row->file);

        return response()->json($graficos);
    }

    public function headers() {
        $headers = Header::get();
        foreach ($headers as $k => $header)
            $headers[$k]->image = Storage::disk('uploads')->url('headers/' . $header->id . '.jpg');
        return response()->json($headers);
    }

    public function investorDay(Request $request) {
        $data = InvestorDay::get();

        foreach ($data as $k => $row) {
            $data[$k]->thumb = Storage::disk('uploads')->url('iday/' . $row->thumb);
            $fileType = '';
            $fileType2 = '';

            if (!empty($data[$k]->file) || !empty($data[$k]->file2)) {
                $extension = substr($data[$k]->file, strrpos($data[$k]->file, '.') + 1);
                $extension2 = substr($data[$k]->file2, strrpos($data[$k]->file2, '.') + 1);
                $data[$k]->file = !empty($data[$k]->file) ? Storage::disk('uploads')->url('iday/' . $row->file) : '';
                $data[$k]->file2 = !empty($data[$k]->file2) ? Storage::disk('uploads')->url('iday/' . $row->file2) : '';
                
                switch ($extension) {
                    case 'xlsx':
                    case 'xls':
                        $fileType = 'xls';
                    break;
                    case 'doc':
                    case 'docx':
                        $fileType = 'doc';
                    break;
                    case 'mp3':
                    case 'wav';
                    case 'm4a':
                        $fileType = 'audio';
                    break;
                    case 'pdf':
                        $fileType = 'pdf';
                    break;
                    case 'mp4':
                    case 'mkv':
                    case 'avi':
                        $fileType = 'video';
                    break;
                }

                switch ($extension2) {
                    case 'xlsx':
                    case 'xls':
                        $fileType2 = 'xls';
                    break;
                    case 'doc':
                    case 'docx':
                        $fileType2 = 'doc';
                    break;
                    case 'mp3':
                    case 'wav';
                    case 'm4a':
                        $fileType2 = 'audio';
                    break;
                    case 'pdf':
                        $fileType2 = 'pdf';
                    break;
                    case 'mp4':
                    case 'mkv':
                    case 'avi':
                        $fileType2 = 'video';
                    break;
                }
            }

            $data[$k]->fileType = $fileType;
            $data[$k]->fileType2 = $fileType2;
        }
        return response()->json($data);
    }

    public function investorEducation(Request $request) {
        $data = InvestorEducation::orderBy('title', 'asc')->get();

        foreach ($data as $k => $row) {
            $data[$k]->thumb = Storage::disk('uploads')->url('ieducation/' . $row->thumb);
            $fileType = '';

            if (!empty($data[$k]->file)) {
                $extension = substr($data[$k]->file, strrpos($data[$k]->file, '.') + 1);
                $data[$k]->file = !empty($data[$k]->file) ? Storage::disk('uploads')->url('ieducation/' . $row->file) : '';
                
                switch ($extension) {
                    case 'xlsx':
                    case 'xls':
                        $fileType = 'xls';
                    break;
                    case 'doc':
                    case 'docx':
                        $fileType = 'doc';
                    break;
                    case 'mp3':
                    case 'wav';
                    case 'm4a':
                        $fileType = 'audio';
                    break;
                    case 'pdf':
                        $fileType = 'pdf';
                    break;
                    case 'mp4':
                    case 'mkv':
                    case 'avi':
                        $fileType = 'video';
                    break;
                }
            }

            $data[$k]->fileType = $fileType;
        }
        return response()->json($data);
    }

    public function institutionalPresentation() {
        $ipresentation = institutionalPresentation::get()->first();
        $ipresentation->file = Storage::disk('uploads')->url('ipresentation/' . $ipresentation->file);
        return response()->json($ipresentation);
    }

    public function noticias() {
        $data = Noticia::orderBy('date', 'desc')->get();

        foreach ($data as $k => $row)
            $data[$k]->image = Storage::disk('uploads')->url('noticias/' . $row->image);
        
        return response()->json($data);
    }

    public function numeros() {
        return response()->json(Numero::get());
    }

    public function ourAssets() {
        $data = OurAsset::get();

        foreach ($data as $k => $row)
            $data[$k]->file = Storage::disk('uploads')->url('our_assets/' . $row->file);

        return response()->json($data);
    }

    public function ourNumbers() {
        $ourNumber = OurNumber::get()->first();
        $ourNumber->file = Storage::disk('uploads')->url('our_numbers/' . $ourNumber->file);
        return response()->json($ourNumber);
    }

    public function popup() {
        $data = Popup::get()->first();

        return response()->json($data);
    }

    public function quarterlyEarnings(Request $request) {
        $limit = $request->limit ?? 200;
        $year = $request->year ?? false;
        $getYears = $request->has('getYears');
        // $groupByQuarter = $request->groupByQuarter ?? false;
        $groupByQuarter = true;
        $lastQuarter = $request->has('lastQuarter');
        
        $data = FinancialQuarter::when($year, function ($query) use ($year) {
            return $query->where('year', $year);
        })
        ->orderBy('year', 'desc')->limit($limit)
        ->when(!$getYears, function ($query) { return $query->orderBy('quarter', 'desc')->get(); })
        ->when($getYears, function ($query) {
            return $query->distinct('year')->get(['year']);
        });
        $result = array();

        if (!$getYears && $year) {
            $result = array('1Q' => array(), '2Q' => array(), '3Q' => array(), '4Q' => array());

            foreach ($data as $row) {
                $row->file = Storage::disk('uploads')->url('financial_quarters/' . $row->file);
                $result[$row->quarter][] = $row;
            }
        }
        else {
            for ($i=0;$i<count($data);$i++) {
                if ($i > 0 && $lastQuarter)
                    if ($data[$i]->year != $data[$i-1]->year || $data[$i]->quarter != $data[$i-1]->quarter)
                        break;
                
                if (!$getYears)
                    $data[$i]->file = Storage::disk('uploads')->url('financial_quarters/' . $data[$i]->file);
                $result[] = $data[$i];
            }
        }

        return response()->json($result);
    }

    public function SECFilings(Request $request) {
        $year = $request->year ?? false;
        $data = SecFiling::when($year, function ($query) use ($year) {
            return $query->whereYear('date', $year);
        })->orderBy('date', 'desc')->get();

        return response()->json($data);
    }
}
