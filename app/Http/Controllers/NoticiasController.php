<?php

namespace App\Http\Controllers;

use App\Noticia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class NoticiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Noticia $noticia)
    {
        return view('panel/noticias')->with('entries', Noticia::orderBy('date', 'desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel/noticias')->with(['edit' => true, 'entry' => new Noticia]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->update($request, new Noticia);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Header  $header
     * @return \Illuminate\Http\Response
     */
    public function edit(Noticia $noticia)
    {
        $noticia = Noticia::findOrFail($noticia->id);

        return view('panel/noticias')->with(['edit' => true, 'entry' => $noticia]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Header  $header
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Noticia $noticia)
    {
        $validated = $request->validate([
            'title' => 'required',
            'subtitle' => 'nullable',
            'copete' => 'nullable',
            'text' => 'nullable',
            'image' => 'nullable|image|mimes:jpeg,jpg,png|max:2048',
            'date' => 'date_format:m-d-Y',
            'link' => 'nullable|url'
        ]);
        $error = '';
        $action = $noticia->exists ? 'actualizar' : 'agregar';
        $done = $noticia->exists ? 'actualizado' : 'agregado';
        $previousImageName = $noticia->exists ? $noticia->image : '';

        $count = Noticia::where(function ($query) use ($request) {
            $query->where('title', $request->title)
                ->orWhere('text', $request->text);
        })->where('id', '!=', $noticia->id)->count();

        if ($count > 0)
            $error = 'Ya existe otro registro con el mismo texto o título';
        else {
            $noticia->title = $request->title;
            $noticia->subtitle = $request->subtitle;
            $noticia->copete = $request->copete;
            $noticia->text = $request->text;
            $noticia->date = Carbon::createFromFormat('m-d-Y', $request->date);
            $noticia->link = $request->link;

            if ($request->file('image')) {
                $noticia->image = $request->file('image')->getClientOriginalName();
    
                if (!empty($previousImageName))
                    Storage::disk('uploads')->delete('noticias/' . $previousImageName);
                
                Storage::disk('uploads')->putFileAs(
                    'noticias/', $request->file('image'), $noticia->image
                );
            }

            if ($noticia->save()) {
                return redirect('panel/noticias')->with('success', 'Registro actualizado con éxito');
            }
            else
                $error = 'Ocurrió un error al actualizar el registro.';
        }

        return back()->with('error', $error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Header  $header
     * @return \Illuminate\Http\Response
     */
    public function destroy(Noticia $noticia)
    {
        $noticia = Noticia::findOrFail($noticia->id);
        $noticia->delete();

        if (!empty($noticia->image))
            Storage::disk('uploads')->delete('noticias/' . $noticia->image);

        return back()->with('success', 'Registro borrado con éxito.');
    }
}
