<?php

namespace App\Http\Controllers;

use App\Entrevista;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class EntrevistaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('panel/entrevistas')->with('entries', Entrevista::get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function edit(Entrevista $entrevista)
    {
        $entrevista = Entrevista::findOrFail($entrevista->id);
        return view('panel/entrevistas')->with(['edit' => true, 'entry' => $entrevista]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entrevista $entrevista)
    {
        $requirements = [
            'footer' => 'required|filled',
            'content' => 'required|filled', 
            'date' => 'required|date_format:m-d-Y',
            'active' => 'required|filled'
        ];

        if ($request->file('input'))
            $requirements['input'] = 'required|image|mimes:jpeg,jpg,png|max:2048';

        $validated = $request->validate($requirements);
        
        $error = '';
        $entrevista->footer = $request->footer;
        $entrevista->content = $request->content;
        $entrevista->date = Carbon::createFromFormat('m-d-Y', $request->date);
        $entrevista->active = $request->active;

        $previousName = $entrevista->exists ? $entrevista->file : '';

        if ($request->file('input')) {
            // var_dump($request->file('input'));
            $entrevista->file = $request->file('input')->getClientOriginalName();
            if (!empty($previousName))
                Storage::disk('uploads')->delete('entrevistas/' . $previousName);
            
            Storage::disk('uploads')->putFileAs(
                'entrevistas/', $request->file('input'), $entrevista->file
            );
        }

        if ($entrevista->save()) {
            return redirect('panel/entrevistas')->with('success', 'Registro actualizado con éxito');
        }
        else
            $error = 'Ocurrió un error al actualizar el registro.';

        return back()->with('error', $error);
    }
}
