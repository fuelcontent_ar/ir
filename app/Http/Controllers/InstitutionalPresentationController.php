<?php

namespace App\Http\Controllers;

use App\InstitutionalPresentation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class InstitutionalPresentationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('panel/ipresentations')->with('entries', InstitutionalPresentation::get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function edit(InstitutionalPresentation $ipresentation)
    {
        $ipresentation = InstitutionalPresentation::findOrFail($ipresentation->id);
        return view('panel/ipresentations')->with(['edit' => true, 'entry' => $ipresentation]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InstitutionalPresentation $ipresentation)
    {
        $requirements = [
            'input' => 'required|file|max:51200'
        ];

        $validated = $request->validate($requirements);
        $error = '';

        $previousName = $ipresentation->exists ? $ipresentation->file : '';

        $ipresentation->file = $request->file('input')->getClientOriginalName();

        Storage::disk('uploads')->delete('ipresentation/' . $previousName);
        Storage::disk('uploads')->putFileAs('ipresentation/', $request->file('input'), $ipresentation->file);

        if ($ipresentation->save()) {
            return redirect('panel/ipresentations')->with('success', 'Registro actualizado con éxito');
        }
        else
            $error = 'Ocurrió un error al actualizar el registro.';

        return back()->with('error', $error);
    }
}
