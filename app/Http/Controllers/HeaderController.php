<?php

namespace App\Http\Controllers;

use App\Header;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Header $header)
    {
        return view('panel/header')->with('entries', Header::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel/header')->with(['edit' => true, 'entry' => new Header]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = array();
        $error = '';
        $validated = $request->validate([
            'title' => 'required|filled',
            'text' => 'required|filled',
            'image' => 'required|image|mimes:jpeg,jpg|max:2048',
        ]);

        $title = $request->title;
        $text = $request->text;
        $token = $request->session()->token();

        
        if (Header::where('title', $title)->orWhere('text', $text)->count() == 0) {
            $header = new Header;
            $header->title = $title;
            $header->text = $text;

            if ($request->file('image')) {
                $header->save();
                Storage::disk('uploads')->putFileAs(
                    'headers', $request->file('image'), $header->id . '.jpg'
                );
                
                return redirect('panel/header')->with('entries', Header::get())
                ->with('success', 'Registro agregado con éxito.');
            }
            else
                $error = 'Por favor, ingresa una imagen';
        }
        else
            $error = 'Ya existe un registro con el mismo título o texto.';
        
        return back()->withInput()->with('error', $error);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Header  $header
     * @return \Illuminate\Http\Response
     */
    public function show(Header $header)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Header  $header
     * @return \Illuminate\Http\Response
     */
    public function edit(Header $header)
    {
        $header = Header::findOrFail($header->id);

        return view('panel/header')->with(['edit' => true, 'entry' => $header]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Header  $header
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Header $header)
    {
        $validated = $request->validate([
            'title' => 'required|filled',
            'text' => 'required|filled',
            'image' => 'nullable|image|mimes:jpeg,jpg|max:2048',
        ]);
        $error = '';
        $count = Header::where(function ($query) use ($request) {
            $query->where('title', $request->title)
                ->orWhere('text', $request->text);
        })->where('id', '!=', $header->id)->count();

        if ($count > 0)
            $error = 'Ya existe otro registro con algún dato en común.';
        else {
            $header->title = $request->title;
            $header->text = $request->text;

            if ($header->save()) {
                if ($request->file('image')) {
                    Storage::disk('uploads')->putFileAs(
                        'headers', $request->file('image'), $header->id . '.jpg'
                    );
                }
                return redirect('panel/header')->with('success', 'Registro actualizado con éxito');
            }
            else
                $error = 'Ocurrió un error al actualizar el registro.';
        }

        return back()->with('error', $error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Header  $header
     * @return \Illuminate\Http\Response
     */
    public function destroy(Header $header)
    {
        $header = Header::findOrFail($header->id);
        $header->delete();
        Storage::disk('uploads')->delete('headers/' . $header->id . '.jpg');
        return back()->with('success', 'Registro borrado con éxito.');
        //
    }
}
