<?php

namespace App\Http\Controllers;

use App\Popup;
use Illuminate\Http\Request;

class PopupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('panel/popup')->with('entries', Popup::get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function edit(Popup $popup)
    {
        $popup = Popup::findOrFail($popup->id);
        return view('panel/popup')->with(['edit' => true, 'entry' => $popup]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Popup $popup)
    {
        $validated = $request->validate([
            'title' => 'required|filled',
            'text' => 'required|filled',
            'active' => 'required|filled',
        ]);
        
        $error = '';
        $popup->title = $request->title;
        $popup->text = $request->text;
        $popup->active = $request->active;

        if ($popup->save()) {
            return redirect('panel/popup')->with('success', 'Registro actualizado con éxito');
        }
        else
            $error = 'Ocurrió un error al actualizar el registro.';

        return back()->with('error', $error);
    }
}
