<?php

namespace App\Http\Controllers;

use App\AnnualGeneralMeeting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AnnualGeneralMeetingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('panel/agm')->with('entries', AnnualGeneralMeeting::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel/agm')->with('entry', new AnnualGeneralMeeting);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->update($request, new AnnualGeneralMeeting);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AnnualGeneralMeeting  $annualGeneralMeeting
     * @return \Illuminate\Http\Response
     */
    public function show(AnnualGeneralMeeting $annualGeneralMeeting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AnnualGeneralMeeting  $annualGeneralMeeting
     * @return \Illuminate\Http\Response
     */
    public function edit(AnnualGeneralMeeting $agm)
    {
        $agm = AnnualGeneralMeeting::findOrFail($agm->id);
        return view('panel/agm')->with(['edit' => true, 'entry' => $agm]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnnualGeneralMeeting  $annualGeneralMeeting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AnnualGeneralMeeting $agm)
    {
        $requirements = ['title' => 'required|filled',];
        if (!$agm->exists)
            $requirements['input'] = 'file|required|max:51200';
        
        $validated = $request->validate($requirements);
        $error = '';
        $action = $agm->exists ? 'actualizar' : 'agregar';
        $done = $agm->exists ? 'actualizado' : 'agregado';

        $agm->title = $request->title;
        $previousName = $agm->exists ? AnnualGeneralMeeting::where('id', $agm->id)->value('file') : '';
        
        if ($request->file('input')) {
            // var_dump($request->file('input'));
            $agm->file = $request->file('input')->getClientOriginalName();
            if (!empty($previousName))
                Storage::disk('uploads')->delete('agm/' . $previousName);
            
            Storage::disk('uploads')->putFileAs(
                'agm/', $request->file('input'), $agm->file
            );
        }
        
        if ($agm->save()) {
            return redirect('panel/agm')->with('success', 'Registro ' . $done . ' con éxito');
        }
        else
            $error = 'Ocurrió un error al ' . $action . ' el registro.';

        return back()->with('error', $error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AnnualGeneralMeeting  $annualGeneralMeeting
     * @return \Illuminate\Http\Response
     */
    public function destroy(AnnualGeneralMeeting $agm)
    {
        $agm = AnnualGeneralMeeting::findOrFail($agm->id);
        $agm->delete();
        Storage::disk('uploads')->delete('agm/' . $agm->file);

        return redirect('panel/agm')->with('success', 'Registro borrado con éxito.');
    }
}
