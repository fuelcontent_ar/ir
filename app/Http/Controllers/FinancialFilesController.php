<?php

namespace App\Http\Controllers;

use App\FinancialFile;
use App\FinancialQuarter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FinancialFilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(FinancialFile $financialfile)
    {
        return view('panel/financials')->with('entries', FinancialFile::get())
        ->with('quarters', FinancialQuarter::orderBy('year', 'desc')->orderBy('quarter', 'desc')->get());
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Header  $header
     * @return \Illuminate\Http\Response
     */
    public function edit(FinancialFile $financial)
    {
        $financialfile = $financial;
        $financialfile = FinancialFile::findOrFail($financialfile->id);
        return view('panel/financials')->with(['edit' => true, 'entry' => $financialfile]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Header  $header
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FinancialFile $financial)
    {
        $financialfile = $financial;
        $validated = $request->validate([
            'type' => 'required|filled',
            'file' => 'file|max:51200'
        ]);
        $error = '';
        $count = FinancialFile::where(function ($query) use ($request) {
            $query->where('type', $request->type);
        })->where('id', '!=', $financialfile->id)->count();

        if ($count > 0)
            $error = 'Ya existe otro registro con el mismo tipo.';
        else {
            $financialfile->type = $request->type;
            $previousName = FinancialFile::where('id', $financialfile->id)->value('file');
            
            if ($request->file('input')) {
                // var_dump($request->file('input'));
                $financialfile->file = $request->file('input')->getClientOriginalName();
                Storage::disk('uploads')->delete('financial_files/' . $previousName);
                // return $financialfile->file . ' ';
                
                Storage::disk('uploads')->putFileAs(
                    'financial_files', $request->file('input'), $financialfile->file
                );
            }
            
            if ($financialfile->save()) {
                return redirect('panel/financials')->with('success', 'Registro actualizado con éxito');
            }
            else
                $error = 'Ocurrió un error al actualizar el registro.';
        }

        return back()->with('error', $error);
    }
}
