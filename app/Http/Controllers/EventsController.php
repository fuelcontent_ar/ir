<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Event $event)
    {
        return view('panel/events')->with('entries', Event::orderBy('date', 'desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel/events')->with(['edit' => true, 'entry' => new Event]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->update($request, new Event);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Header  $header
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        $event = Event::findOrFail($event->id);

        return view('panel/events')->with(['edit' => true, 'entry' => $event]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Header  $header
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $validated = $request->validate([
            'title' => 'required',
            /* 'subtitle' => 'nullable',
            'copete' => 'nullable',
            'text' => 'nullable',
            'image' => 'nullable|image|mimes:jpeg,jpg|max:2048', */
            'date' => 'date_format:m-d-Y|required',
            // 'link' => 'nullable'
        ]);
        $error = '';
        $action = $event->exists ? 'actualizar' : 'agregar';
        $done = $event->exists ? 'actualizado' : 'agregado';
        // $previousImageName = event->exists ? event->image : '';

        $count = Event::where(function ($query) use ($request) {
            $query->where('title', $request->title);
        })->where('id', '!=', $event->id)->count();

        if ($count > 0)
            $error = 'Ya existe otro registro con algún dato en común.';
        else {
            $event->title = $request->title;
            $event->date = Carbon::createFromFormat('m-d-Y', $request->date);
            /* event->subtitle = $request->subtitle;
            $event->copete = $request->copete;
            $event->text = $request->text;
            $event->link = $request->link; */

            /* if ($request->file('image')) {
                $event->image = $request->file('image')->getClientOriginalName();
    
                if (!empty($previousImageName))
                    Storage::disk('uploads')->delete('noticias/' . $previousImageName);
                
                Storage::disk('uploads')->putFileAs(
                    'events/', $request->file('image'), event->image
                );
            } */

            if ($event->save()) {
                return redirect('panel/events')->with('success', 'Registro actualizado con éxito');
            }
            else
                $error = 'Ocurrió un error al actualizar el registro.';
        }

        return back()->with('error', $error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Header  $header
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $event = Event::findOrFail($event->id);
        $event->delete();

        /* if (!empty($event->image))
            Storage::disk('uploads')->delete('events/' . $event->image); */

        return back()->with('success', 'Registro borrado con éxito.');
    }
}
