<?php

namespace App\Http\Controllers;

use App\FinancialQuarter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;

class FinancialQuartersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function import() {
        $data = DB::connection('mysql2')->table('financials_quarters')->get();
        $out = "array(";
        foreach ($data as $row) {
            // var_dump($row);
            $from = public_path() . '/../../ir_viejo/uploads/' . $row->ARCHIVO;
            $to = storage_path('app/public/financial_quarters/') . $row->ARCHIVO;
            /* if (file_exists($from) && !file_exists($to)) {
                copy($from, $to);
                print $from . " " . $to . " " . $row->ARCHIVO . "<br />";
            } */
            $our_assets = strlen($row->OUR_ASSETS) > 0 ? 1 : 0;
            $out .= "\n\tarray('title' => '" . $row->TITULO . "', 'file' => '" . $row->ARCHIVO . "', 'year' => '" . $row->ANIO . "', 'quarter' => '" . $row->QUARTER . "', 'our_assets' => '" . $our_assets . "'),";
        }
        print $out;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel/quarterly')->with('entry', new FinancialQuarter);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->update($request, new FinancialQuarter);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FinancialQuarter  $financialQuarter
     * @return \Illuminate\Http\Response
     */
    public function edit(FinancialQuarter $quarterly)
    {
        $quarterly = FinancialQuarter::findOrFail($quarterly->id);
        return view('panel/quarterly')->with(['edit' => true, 'entry' => $quarterly]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FinancialQuarter  $financialQuarter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FinancialQuarter $quarterly)
    {
        $requirements = [
            'title' => 'required|filled',
            'year' => 'required|digits:4|integer|min:1900|max:'.(date('Y')+1),
            'quarter' => 'required|filled'
        ];

        if (!$quarterly->exists)
            $requirements['input'] = 'file|required|max:51200';
        
        $validated = $request->validate($requirements);
        $error = '';
        $action = $quarterly->exists ? 'actualizar' : 'agregar';
        $done = $quarterly->exists ? 'actualizado' : 'agregado';

        $quarterly->title = $request->title;
        $quarterly->year = $request->year;
        $quarterly->quarter = $request->quarter;
        
        $previousName = $quarterly->exists ? FinancialQuarter::where('id', $quarterly->id)->value('file') : '';
        
        if ($request->file('input')) {
            // var_dump($request->file('input'));
            $quarterly->file = $request->file('input')->getClientOriginalName();
            if (!empty($previousName))
                Storage::disk('uploads')->delete('financial_quarters/' . $previousName);
            
            Storage::disk('uploads')->putFileAs(
                'financial_quarters', $request->file('input'), $quarterly->file
            );
        }
        
        if ($quarterly->save()) {
            return redirect('panel/financials')->with('success', 'Registro ' . $done . ' con éxito');
        }
        else
            $error = 'Ocurrió un error al ' . $action . ' el registro.';

        return back()->with('error', $error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FinancialQuarter  $financialQuarter
     * @return \Illuminate\Http\Response
     */
    public function destroy(FinancialQuarter $quarterly)
    {
        $quarterly = FinancialQuarter::findOrFail($quarterly->id);
        $quarterly->delete();
        Storage::disk('uploads')->delete('financial_quarters/' . $quarterly->file);
        return redirect('panel/financials')->with('success', 'Registro borrado con éxito.');
    }
}
