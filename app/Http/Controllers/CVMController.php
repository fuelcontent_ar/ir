<?php

namespace App\Http\Controllers;

use App\CVM;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class CVMController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('panel/cvm')->with('entries', CVM::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel/cvm')->with('entry', new CVM);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->update($request, new CVM);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AnnualGeneralMeeting  $annualGeneralMeeting
     * @return \Illuminate\Http\Response
     */
    public function edit(CVM $cvm)
    {
        $cvm = CVM::findOrFail($cvm->id);
        return view('panel/cvm')->with(['edit' => true, 'entry' => $cvm]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnnualGeneralMeeting  $annualGeneralMeeting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CVM $cvm)
    {
        $requirements = ['date' => 'date_format:m-d-Y|required',
        'description' => 'required|filled'];
        
        if (!$cvm->exists)
            $requirements['input'] = 'file|required|max:51200';
        
        $validated = $request->validate($requirements);
        $error = '';
        $action = $cvm->exists ? 'actualizar' : 'agregar';
        $done = $cvm->exists ? 'actualizado' : 'agregado';

        $cvm->date = Carbon::createFromFormat('m-d-Y', $request->date);
        $cvm->description = $request->description;
        $previousName = $cvm->exists ? CVM::where('id', $cvm->id)->value('file') : '';
        
        if ($request->file('input')) {
            // var_dump($request->file('input'));
            $cvm->file = $request->file('input')->getClientOriginalName();

            if (!empty($previousName))
                Storage::disk('uploads')->delete('cvm/' . $previousName);
            
            Storage::disk('uploads')->putFileAs(
                'cvm/', $request->file('input'), $cvm->file
            );
        }
        
        if ($cvm->save()) {
            return redirect('panel/cvm')->with('success', 'Registro ' . $done . ' con éxito');
        }
        else
            $error = 'Ocurrió un error al ' . $action . ' el registro.';

        return back()->with('error', $error);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AnnualGeneralMeeting  $annualGeneralMeeting
     * @return \Illuminate\Http\Response
     */
    public function destroy(CVM $cvm)
    {
        $cvm = CVM::findOrFail($cvm->id);
        $cvm->delete();
        Storage::disk('uploads')->delete('cvm/' . $cvm->file);

        return redirect('panel/cvm')->with('success', 'Registro borrado con éxito.');
    }
}
