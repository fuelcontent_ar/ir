<?php

namespace App\Http\Controllers;

use App\Legales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class LegalesController extends Controller
{

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Legales $legale)
    {
        $legale->equity = $request->has('equity') ? $request->equity : $legale->equity;
        $legale->fixed_income = $request->has('fixed_income') ? $request->fixed_income : $legale->fixed_income;

        if ($legale->save()) {
            $view = $request->equity ? 'acequity' : 'acfixedincome';
            return redirect('panel/' . $view)->with('success_legales', 'Legales actualizado con éxito.');
        }
        else
            $error = 'Ocurrió un error al actualizar los legales.';

        return back()->with('error_legales', $error);
    }
}
