<?php

namespace App\Http\Controllers;

use App\OurAsset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OurAssetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(OurAsset $ourasset)
    {
        return view('panel/ourassets')->with('entries', OurAsset::get());
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Header  $header
     * @return \Illuminate\Http\Response
     */
    public function edit(OurAsset $ourasset)
    {
        $ourasset = OurAsset::findOrFail($ourasset->id);
        return view('panel/ourassets')->with(['edit' => true, 'entry' => $ourasset]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Header  $header
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OurAsset $ourasset)
    {
        $validated = $request->validate([
            'title' => 'required|filled',
            'file' => 'file|max:51200'
        ]);
        $error = '';
        $count = OurAsset::where(function ($query) use ($request) {
            $query->where('title', $request->title);
        })->where('id', '!=', $ourasset->id)->count();

        if ($count > 0)
            $error = 'Ya existe otro registro con el mismo tipo.';
        else {
            $ourasset->title = $request->title;
            $previousName = OurAsset::where('id', $ourasset->id)->value('file');
            
            if ($request->file('input')) {
                // var_dump($request->file('input'));
                $ourasset->file = $request->file('input')->getClientOriginalName();
                if (!empty($previousName))
                    Storage::disk('uploads')->delete('our_assets/' . $previousName);
                
                Storage::disk('uploads')->putFileAs('our_assets/', $request->file('input'), $ourasset->file
                );
            }
            
            if ($ourasset->save()) {
                return redirect('panel/ourassets')->with('success', 'Registro actualizado con éxito');
            }
            else
                $error = 'Ocurrió un error al actualizar el registro.';
        }

        return back()->with('error', $error);
    }
}
