<?php

namespace App\Http\Controllers;

use App\Factsheet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FactsheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('panel/factsheets')->with('entries', Factsheet::get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function edit(Factsheet $factsheet)
    {
        $factsheet = Factsheet::findOrFail($factsheet->id);
        return view('panel/factsheets')->with(['edit' => true, 'entry' => $factsheet]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Factsheet $factsheet)
    {
        $requirements = [
            'input' => 'required|file|max:51200'
        ];

        $validated = $request->validate($requirements);
        $error = '';

        $previousName = $factsheet->exists ? $factsheet->file : '';

        $factsheet->file = $request->file('input')->getClientOriginalName();

        Storage::disk('uploads')->delete('factsheets/' . $previousName);
        Storage::disk('uploads')->putFileAs('factsheets/', $request->file('input'), $factsheet->file);

        if ($factsheet->save()) {
            return redirect('panel/factsheets')->with('success', 'Registro actualizado con éxito');
        }
        else
            $error = 'Ocurrió un error al actualizar el registro.';

        return back()->with('error', $error);
    }
}
