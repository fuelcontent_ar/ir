<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public function getDateAttribute($date){
        return $date ? \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('m-d-Y') : '';
    }
}
