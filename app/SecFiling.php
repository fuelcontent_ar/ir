<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecFiling extends Model
{
    protected $fillable = ['date', 'type', 'link', 'description'];
    
    public function getDateAttribute($date){
        return $date ? \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('m-d-Y') : '';
    }
}
