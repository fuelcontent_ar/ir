<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcFixedIncome extends Model
{
    protected $fillable = ['firm', 'analyst', 'phone', 'email'];
}
