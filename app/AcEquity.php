<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcEquity extends Model
{
    protected $table = 'ac_equities';
    protected $fillable = ['firm', 'analyst', 'phone', 'email', 'recommendation', 'tp', 'date'];

    public function getDateAttribute($date){
        return $date ? \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('m-d-Y') : '';
    }
}
