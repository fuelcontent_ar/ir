<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entrevista extends Model
{
    public function getDateFormattedAttribute(){
        return $this->date ? \Carbon\Carbon::createFromFormat('Y-m-d', $this->date)->format('F jS Y') : '';
    }
}
