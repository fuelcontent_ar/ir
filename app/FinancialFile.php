<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinancialFile extends Model
{
    protected $fillable = ['type', 'file'];
}
