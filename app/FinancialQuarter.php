<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinancialQuarter extends Model
{
    protected $fillable = ['title', 'file', 'year', 'quarter', 'our_assets'];
}
