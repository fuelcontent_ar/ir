<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;
    private $entry;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($entry)
    {
        $this->entry = $entry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('ir@adecoagro.com', 'Contacto Web')
        ->view('mailing.contact')
        ->with(['entry' => $this->entry]);
        /* ->attach(storage_path('app/public/files/agm/1616542467_Proxy Card - blank.pdf'), [
            'as' => 'sample.pdf',
            'mime' => 'application/pdf',
        ]); */
    }
}
