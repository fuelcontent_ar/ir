<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcEquitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ac_equities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firm');
            $table->string('analyst');
            $table->string('phone');
            $table->string('email');
            $table->string('recommendation')->nullable();
            $table->string('tp');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ac_equities');
    }
}
