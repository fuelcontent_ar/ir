<?php

use Illuminate\Database\Seeder;

class FactsheetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('factsheets')->insert(['file' => '1616696270_FactSheet.pdf']);
    }
}
