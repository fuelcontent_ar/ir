<?php

use Illuminate\Database\Seeder;

class EventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('title' => 'Evento de prueba', 'date' => '2021-10-21')
        );

        foreach ($data as $row)
            DB::table('events')->insert($row);
    }
}
