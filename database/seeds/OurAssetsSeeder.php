<?php

use Illuminate\Database\Seeder;

class OurAssetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('title' => 'Cushman & Wakefield independent valuation appraisal.', 'file' => 'Executive summary Adecoagro 2021.pdf'),
            array('title' => 'Area under management breakdown', 'file' => 'Landbank_Adecoagro.pdf'),
            array('title' => 'Industrial Assets breakdown', 'file' => 'Industrial Assets.pdf')
        );

        foreach ($data as $row)
            DB::table('our_assets')->insert($row);
    }
}
