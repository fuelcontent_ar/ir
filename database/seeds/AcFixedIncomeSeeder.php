<?php

use Illuminate\Database\Seeder;

class AcFixedIncomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('firm' => 'JP Morgan',
            'analyst' => 'Julie Murphy',
            'phone' => '1 212 834 8580',
            'email' => 'Julie.G.Murphy@jpmorgan.com'),
            array('firm' => 'Merryl Lynch',
            'analyst' => 'Barbara Halberstadt',
            'phone' => '1 646 855 3262',
            'email' => 'Barbara.Halberstadt@baml.com'),
            array('firm' => 'Debtwire',
            'phone' => '1 212 500 1392',
            'analyst' => 'Marcelo Burone',
            'email' => 'Marcelo.Burone@acuris.com')
        );
        foreach ($data as $row)
            DB::table('ac_fixed_incomes')->insert($row);
    }
}
