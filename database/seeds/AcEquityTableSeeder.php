<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AcEquityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('firm' => 'BTG Pactual',
            'analyst' => 'Thiago Duarte',
            'phone' => '55 11 3383-2366',
            'email' => 'Thiago.Duarte@btgpactual.com',
            'recommendation' => 'Neutral',
            'tp' => '$5.7',
            'date' => '08/14/2020'),
            array('firm' => 'Credit Suisse',
            'analyst' => 'Victor Saragiotto	',
            'phone' => '55 11 3701-6303',
            'email' => 'Victor.Saragiotto@credit-suisse.com',
            'recommendation' => 'Neutral',
            'tp' => '$10.0',
            'date' => '11/13/2019'),
            array('firm' => 'HSBC Securities',
            'analyst' => 'Alexandre Falcao',
            'phone' => '55 11 3371-8203',
            'email' => 'Alexandre.p.Falcao@hsbc.com.br',
            'recommendation' => 'Buy',
            'tp' => '$6.0',
            'date' => '07/14/2020'),
            array('firm' => 'ITAU BBA',
            'analyst' => 'Antonio Barreto',
            'phone' => '55 11 3073-3060',
            'email' => 'Antonio.Barreto@itaubba.com',
            'recommendation' => 'Neutral',
            'tp' => '$8.5',
            'date' => '06/25/2019'),
            array('firm' => 'Merryl Lynch',
            'analyst' => 'Isabella Simonato',
            'phone' => '55 11 2188-4243',
            'email' => 'Isabella.Simonato@baml.com',
            'recommendation' => 'Neutral',
            'tp' => '$5.5',
            'date' => '08/14/2020'),
            array('firm' => 'Morgan Stanley',
            'analyst' => 'Javier Martinez de Olcoz C.',
            'phone' => '1 212 761-4542',
            'email' => 'Javier.Martinez.Olcoz@morganstanley.com',
            'recommendation' => 'Overweight',
            'tp' => '$6.5',
            'date' => '08/14/2020'),
            array('firm' => 'Santander',
            'analyst' => 'Gustavo Allevato',
            'phone' => '55 11 3012-6042',
            'email' => 'Gallevato@santander.com.br',
            'recommendation' => 'Neutral',
            'tp' => '$8.5',
            'date' => '05/02/2019'),
            array('firm' => 'JP Morgan',
            'analyst' => 'Lucas Ferreira',
            'phone' => '55 11 4950-3629',
            'email' => 'Lucas.x.Ferreira@jpmorgan.com',
            'recommendation' => 'Overweight',
            'tp' => '$7.0',
            'date' => '08/13/2020'),
            array('firm' => 'Citi',
            'analyst' => 'Fernanda Cunha',
            'phone' => '55-21-4009-0443',
            'email' => 'fernanda.cunha@citi.com',
            'recommendation' => 'Buy',
            'tp' => '$4.9',
            'date' => '08/13/2020'),
            array('firm' => 'TPGC',
            'analyst' => 'Santiago Barros Moss(h)',
            'phone' => '+54 11 4898 6629',
            'email' => 'sbarros@tpcgco.com',
            'recommendation' => 'Buy',
            'tp' => '$11.0',
            'date' => '05/06/2019')
        );

        foreach ($data as $row) {
            $row['date'] = Carbon::createFromFormat('m/d/Y', $row['date'])->format('y-m-d');
            DB::table('ac_equities')->insert($row);
        }
    }
}
