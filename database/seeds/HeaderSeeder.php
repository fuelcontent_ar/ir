<?php

use Illuminate\Database\Seeder;

class HeaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('headers')->insert([
            'title' => 'Sugarcane',
            'text' => 'Fully integrated operation and state of the art industrial facilities

We successfully developed a fully integrate and highly efficient operation in Mato Grosso do Sul

'
        ]);
        DB::table('headers')->insert([
            'title' => 'Dairy',
            'text' => 'Sustainable and fully scalable production model

Among the most productive milk producers in the region'
        ]);
        DB::table('headers')->insert([
            'title' => 'Rice',
            'text' => 'Among the largest producers in the region

With more than 40.000 hectares we have developed a fully integrated business implementing cutting edge technology'
        ]);
        DB::table('headers')->insert([
            'title' => 'Crops',
            'text' => 'Diversified operations throughout Argentina and Uruguay

Low cost producer with a diversified asset base'
        ]);
    }
}
