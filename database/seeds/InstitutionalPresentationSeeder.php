<?php

use Illuminate\Database\Seeder;

class InstitutionalPresentationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('institutional_presentations')->insert(['file' => 'Adecoagro Institutional Presentation.pdf']);
    }
}
