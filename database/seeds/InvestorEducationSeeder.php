<?php

use Illuminate\Database\Seeder;

class InvestorEducationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('title' => 'S&E Current Market Dynamics',
            'description' => 'In this video, we analize the sugar and ethanol dynamics for the 2018/19 harvest year and stress how well positioned Adecoagro is to benefit from current relative prices as a result of the high flexibility in production mix achieved. Policies such as RenovaBio in Brazil and E10 in China are considered.',
            'thumb' => '1545163219_S&E.png',
            'video' => '289985380',
            'file' => ''
            ),
            array('title' => 'Biological Asset',
            'description' => 'The "myth" of the biological asset is dismantled in this video. The rationale behind this accounting line as well as the benefits for the user of the financial statements are addressed.',
            'thumb' => '1545163608_the myth of the biol.png',
            'video' => '302273936',
            'file' => ''
            ),
            array('title' => 'How do RBOB price movements impact Adecoagro\'s EBITDA?',
            'description' => 'With this file, you should be able to have a broad idea of how movements in international oil prices (specifically, RBOB) impact Adecoagro´s EBITDA generation. To that end, we first go through the build-up of the ex-mill ethanol price in Mato Grosso do Sul, starting from the international gasoline price. Once the ethanol price sensitivity is constructed, based on our estimated ethanol production it´s straightforward to calculate the EBITDA sensitivity.',
            'thumb' => '1584036705_1584036417_RBOB.png',
            'video' => '',
            'file' => 'How_do_RBOB_price_movements_impact_Adecoagros_EBITDA.xlsx'
            ),
            array('title' => 'SE&E Total Cash Cost',
            'description' => 'The file contains information such as: -How to build total cash cost -Total cash cost breakdown 2016-2019 -EBITDA price breakdown 2017-2019 -How to build EBITDA from total cash cost and EBITDA price.',
            'thumb' => '',
            'video' => '',
            'file' => 'Cash_Cost_EBITDA 2017-2019_Final_Version_Web_link.xlsx'
            ),
            array('title' => 'IFRS 16 vs. IAS 17',
            'description' => 'The file contains an illustrative example to account for the implications of the standard IFRS 16 in our Financial Statements.',
            'thumb' => '1557779167_Caratula.jpg',
            'video' => '',
            'file' => '1557779167_IFRS 16 vs  IAS 17.xlsx'
            ),
            array('title' => 'Foreign Exchange Variations | Financial Impact',
            'description' => 'In this video we explain how foreign exchange variations impact our financial statements with two illustrative examples. In addition, we explain the rationale behind the decision to use ¨Adjusted Net Income¨ in order to better assess Adecoagro´s financial performance.',
            'thumb' => '1558122066_Mini portada.jpg',
            'video' => '336822235',
            'file' => '1558122066_Fx Variations Video - Examples.xlsx'
            ),
            array('title' => 'Adecoagro´s Hedging Policy',
            'description' => 'In this video, we go through the rationale behind Adecoagro´s hedging policy and how related results (Other Operating Income) flow through the Company´s P&L.',
            'thumb' => '1565387568_foto2.jpg',
            'video' => '353022622',
            'file' => ''
            ),
            array('title' => 'Sugar, Ethanol & Energy Main Competitive Advantages',
            'description' => 'In this tutorial, we will walk you through Adecoagro’s Sugar Ethanol & Energy Business Main Competitive Advantages. We will show you how they impact our business and position us as one of the most competitive players in Brazil.',
            'thumb' => '1580496917_miniatura-investor-relations.jpg',
            'video' => '386942294',
            'file' => ''
            ),
            array('title' => 'BRL/m3 prices into cts/lb Sugar Equivalent',
            'description' => 'With this file, you´ll be able to convert the price of ethanol in BRL/m3 in Paulinia (available on Bloomberg) to the ex-mill price in cts/lb sugar equivalent, in Mato Grosso do Sul',
            'thumb' => '1584035160_brl.png',
            'video' => '',
            'file' => 'BRL_perm3_prices_into_cts_per_lb_Sugar_Equivalent.xlsx'
            )
        );

        foreach ($data as $row)
            DB::table('investor_education')->insert($row);
    }
}
