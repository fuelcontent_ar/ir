<?php

use Illuminate\Database\Seeder;

class OurNumberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('our_numbers')->insert(['file' => 'Our_Numbers_2020_FV_PV.xlsx']);
    }
}
