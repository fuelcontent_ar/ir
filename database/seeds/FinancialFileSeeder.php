<?php

use Illuminate\Database\Seeder;

class FinancialFileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('file' => '1636717273_3Q21 Presentation.pdf',
                'type' => 'Webcast'
            ),
            array('file' => '1636578415_EX 99.1 FS 09.30.2021 (2).pdf',
                'type' => 'Press'
            ),
            array('file' => 'ER',
                'type' => '1636578439_6K ER 09.30.2021_FV.pdf'
            )
        );
        
        foreach ($data as $row)
            DB::table('financial_files')->insert($row);
    }
}
