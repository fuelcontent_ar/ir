<?php

use Illuminate\Database\Seeder;

class GraficosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'title' => 'Adecoagro´s 2020 Sales Distribution by Business',
                'file' => '1616692940_Sales dist.jpg'
            ),
            array(
                'title' => 'Adecoagro´s 2020 EBITDA Distribution by Business',
                'file' => '1616693035_EBITDA dist.jpg'
            )
        );

        foreach ($data as $row)
            DB::table('graficos')->insert($row);
    }
}
