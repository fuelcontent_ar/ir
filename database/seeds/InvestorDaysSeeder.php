<?php

use Illuminate\Database\Seeder;

class InvestorDaysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('title' => 'Adecoagro Investor Day 2018 at NYSE',
                'description' => 'Watch the entire presentation of Adecoagro´s 2018 Investor Day. Our Senior Management discusses in detail all of our existing businesses as well as our growth plans.',
                'thumb' => '1547127900_cover inv day.png',
                'video' => '294169604',
                'file' => '1547127900_AGRO Investor Day 2018.pdf',
                'file2' => ''
            ),
            array('title' => 'AGRO Investor Day 2017 in Sao Pablo',
                'description' => 'Listen to the entire presentation of Adecoagro´s 2017 Investor Day.',
                'video' => '',
                'thumb' => '1547481038_Portada Inv Day 2017.png',
                'file' => '1547480983_Adecoagro Investor Day Presentation.pdf',
                'file2' => ''
            )
        );

        foreach ($data as $row)
            DB::table('investor_days')->insert($row);
    }
}
