<?php

use Illuminate\Database\Seeder;

class LegalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('legales')->insert([
            'fixed_income' => '',
            'equity' => 'Adecoagro is covered by the analyst(s) listed above. Please note that any opinions, estimates or forecasts regarding Adecoagro?s performance made by these analysts are theirs alone and do not represent opinions, forecasts or predictions of Adecoagro or its management. Adecoagro does not by its reference above or distribution imply its endorsement of or concurrence with such information, conclusions or recommendations.'
        ]);
    }
}
