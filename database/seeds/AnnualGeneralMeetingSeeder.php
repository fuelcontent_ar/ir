<?php

use Illuminate\Database\Seeder;

class AnnualGeneralMeetingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('title' => 'Agenda AGM 2021', 'file' => '1617114337_Agenda AGM 2021 v2.pdf'),
            array('title' => 'Proxy Card for AGM 2021', 'file' => '1616542467_Proxy Card - blank.pdf'),
            array('title' => 'Convening Notice with Board of Director´s recommendations', 'file' => '	1616561530_Adecoagro SA Convening Notice AGM 2021 (with BOD recommendations).pdf'),
            array('title' => 'Annual accounts and annual report as of December 31, 2020 and Consolidated Financial Statements as of and for the years ended December 31, 2020, 2019, and 2018',
            'file' => 'Annual accounts and report 2020 and Consolidated FFSS 20, 19, and 18.pdf'),
            array('title' => 'Minute AGM 2021', 'file' => '1622131918_Adecoagro SA - 2021.04.21 - AGM.PDF')
        );

        foreach ($data as $row)
            DB::table('annual_general_meetings')->insert($row);
    }
}
