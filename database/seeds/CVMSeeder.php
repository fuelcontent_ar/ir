<?php

use Illuminate\Database\Seeder;

class CVMSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cvm')->insert([
            'date' => '2021-03-31',
            'description' => 'AVI Balance Sheet Br GAAP as of December 31, 2020',
            'file' => 'DF_AVI_conso_Dez20.pdf'
        ]);
    }
}
