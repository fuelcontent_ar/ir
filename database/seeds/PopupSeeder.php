<?php

use Illuminate\Database\Seeder;

class PopupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('popups')->insert(['title' => '3Q21 Earnings Release Calendar',
        'text' => 'Earnings Release Date: November 10, 2021 (after closing market).

Conference Call: November 12, 2021 at 8am US EST (10am Buenos Aires/Sao Paulo). 
        
Webcast Link: https://webcastlite.mziq.com/cover.html?webcastId=7948fd7b-269b-4958-a4ed-ebba91e2f646 
        ']);
    }
}
