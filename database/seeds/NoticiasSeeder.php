<?php

use Illuminate\Database\Seeder;

class NoticiasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('title' => 'Renato Junqueira (Director of SE&E) talks about innovation and technology in our industry',
            'subtitle' => '',
            'copete' => 'Renato Junqueira participated in an article for Revista Opiniões were he talked about the importance of innovation and technology in the Sugar & Ethanol industry. Check out the article.',
            'text' => '',
            'date' => '2021-08-31',
            'image' => '1630600035_Opinoes.jpeg',
            'link' => ''),
            array('title' => 'We updated our 2021 AGM information',
            'subtitle' => '',
            'copete' => '',
            'text' => '',
            'date' => '2021-03-31',
            'image' => '',
            'link' => 'https://ir.adecoagro.com/index.php?s=annual-general-meeetings'),
            array('title' => 'Renato Junqueira (Director of SE&E) talks to Valor Econômico about our biomethane project',
            'subtitle' => '',
            'copete' => 'In a world transitioning to a low-carbon economy, Adecoagro makes its contribution by producing biogas and biomethane from vinasse. Click on',
            'text' => '',
            'date' => '2021-09-01',
            'image' => '1630599496_Biometano.jpg',
            'link' => 'https://valor.globo.com/agronegocios/noticia/2021/09/01/adecoagro-da-primeiro-passo-em-biometano.ghtml')
        );

        foreach ($data as $row)
            DB::table('noticias')->insert($row);
    }
}
