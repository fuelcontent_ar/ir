<?php

use Illuminate\Database\Seeder;

class NumerosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('title' => 'EBITDA USD MM', 'number' => '342'),
            array('title' => 'Total Sales USD MM', 'number' => '822'),
            array('title' => 'Sugarcane Crushed MM tons', 'number' => '11.1'),
            array('title' => 'Area Under Management Th. Hectares', 'number' => '485'),
            array('title' => 'Milk Production MM Liters', 'number' => '145'),
            array('title' => 'Crop & Rice Production Th. Tons', 'number' => '998')
        );

        foreach ($data as $row)
            DB::table('numeros')->insert($row);
    }
}
