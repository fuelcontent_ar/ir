<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(HeaderSeeder::class);
        $this->call(AcEquityTableSeeder::class);
        $this->call(AcFixedIncomeSeeder::class);
        $this->call(SecFilingSeeder::class);
        $this->call(FinancialFileSeeder::class);
        $this->call(FinancialQuartersSeeder::class);
        $this->call(AnnualGeneralMeetingSeeder::class);
        $this->call(PopupSeeder::class);
        $this->call(EntrevistaSeeder::class);
        $this->call(FactsheetSeeder::class);
        $this->call(OurNumberSeeder::class);
        $this->call(InstitutionalPresentationSeeder::class);
        $this->call(LegalesSeeder::class);
        $this->call(CVMSeeder::class);
        $this->call(InvestorEducationSeeder::class);
        $this->call(GraficosSeeder::class);
        $this->call(NumerosSeeder::class);
        $this->call(OurAssetsSeeder::class);
        $this->call(InvestorDaysSeeder::class);
        $this->call(NoticiasSeeder::class);
        $this->call(EventsSeeder::class);
    }
}
