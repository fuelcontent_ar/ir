<?php

use Illuminate\Database\Seeder;

class EntrevistaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('entrevistas')->insert([
            'content' => '"2021 marked the 10-year anniversary of Adecoagro’s IPO. In the last 10 years our Adjusted EBITDA grew from $100 million to roughly $400 million and our cash generation became structurally positive. Since the IPO we expanded our businesses and conducted investments that allowed us to vertically integrate our operations, improve efficiencies and enhance our competitive advantages. We have also become a source of carbon credits and a referent in the sustainable production of food and renewable energy. Adecoagro is now a more mature company with a healthy capital structure, and the capacity to commit to a systematic distribution policy. Starting in 2022 we will distribute annually at least 40% of the Adjusted Free Cash from Operations generated during the previous year. "',
            'footer' => 'Mariano Bosch, Co-founder and CEO',
            'date' => '2021-11-12',
            'active' => true,
            'file' => '1569591326_mariano_bosch.jpg'
        ]);
    }
}
