<?php

use Illuminate\Database\Seeder;

class SecFilingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = 
        array(
            array(
                'date' => '2018-9-13',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000167276418000019/6KApril22.htm',
                'description' => 'Sancor: New Investment Proposal'),
            array(
                'date' => '2018-05-14',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000162828018006779/agro6k051418boardofdirecto.htm',
                'description' => 'New Chairman Designation'),
            array(
                'date' => '2018-04-27',
                'type' => '20-F',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000162828018005268/0001628280-18-005268-index.htm',
                'description' => '20-F'),
            array(
                'date' => '2018-03-26',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000162828018003564/a6-kannualmeeting03262018.htm',
                'description' => 'Annual General Meeting of Shareholders'),
            array(
                'date' => '2018-03-23',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000167276418000006/p59990.htm',
                'description' => 'Sancor: Investment Proposal'),
            array(
                'date' => '2017-06-22',
                'type' => '20-F/A',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000167276417000074/0001672764-17-000074-index.htm',
                'description' => '20-F/A'),
            array(
                'date' => '2017-12-04',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000162828017012091/0001628280-17-012091-index.htm',
                'description' => 'MD&A 3Q17'),
            array(
                'date' => '2017-09-06',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000095010317008657/0000950103-17-008657-index.htm',
                'description' => 'Bond Offering'),
            array(
                'date' => '2017-09-26',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000167276417000091/0001672764-17-000091-index.htm',
                'description' => 'Adecoagro Enhances Capital Structure and Improves Long Term Financial Flexibility'),
            array(
                'date' => '2017-09-15',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000095010317008902/0000950103-17-008902-index.htm',
                'description' => 'Offering of 6.00% of Senior Notes'),
            array(
                'date' => '2017-06-13',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000167276417000066/0001672764-17-000066-index.htm',
                'description' => 'MD&A 1Q17'),
            array(
                'date' => '2017-04-24',
                'type' => '20-F',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000167276417000046/t1701194_20f.htm',
                'description' => '20-F'),
            array(
                'date' => '2017-04-04',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000167276417000023/t1700978_6k.htm',
                'description' => 'Adecoagro Announces Ammendment of Share Repurchase Program'),
            array(
                'date' => '2017-03-28',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000167276417000017/t1700674_x2-6k.htm',
                'description' => 'Annual General Meeting of Shareholders'),
            array(
                'date' => '2016-12-08',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000167276416000059/0001672764-16-000059-index.htm',
                'description' => 'MD&A 3Q16'),
            array(
                'date' => '2016-08-31',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000167276416000047/0001672764-16-000047-index.htm',
                'description' => 'MD&A 2Q16'),
            array(
                'date' => '2016-04-27',
                'type' => '20-F',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000167276416000005/0001672764-16-000005-index.htm',
                'description' => '20-F'),
            array(
                'date' => '2016-04-27',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000167276416000004/0001672764-16-000004-index.htm',
                'description' => 'Adecoagro  S.A. Announces Exercise of Option to Purchase Additional Shares'),
            array(
                'date' => '2016-03-24',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000156761916002080/s001206x5_6k.htm',
                'description' => 'Annual Geeral Meeting of Shareholders'),
            array(
                'date' => '2016-03-21',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000156761916002057/s001248x1_6k.htm',
                'description' => 'Adecoagro Announces Offering Of its Common Shares by Certain Selling Shareholders'),
            array(
                'date' => '2016-03-17',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000114036116058288/0001140361-16-058288-index.htm',
                'description' => 'MD&A 1Q16'),
            array(
                'date' => '2015-11-30',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000114036115043076/0001140361-15-043076-index.htm',
                'description' => 'MD&A 3Q15'),
            array(
                'date' => '2015-09-18',
                'type' => 'S-8',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000156761915001188/s001017x2_s8.htm',
                'description' => 'S-8'),
            array(
                'date' => '2015-09-01',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000156761915001144/0001567619-15-001144-index.htm',
                'description' => 'MD&A 2Q15'),
            array(
                'date' => '2015-06-09',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000156761915000810/0001567619-15-000810-index.htm',
                'description' => 'MD&A 1Q15'),
            array(
                'date' => '2015-04-30',
                'type' => '20-F',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000156761915000620/s000878x1_20f.htm',
                'description' => '20-F'),
            array(
                'date' => '2015-04-01',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000156761915000401/s000827x1_6k.htm',
                'description' => 'Annual General Meeting of Shareholders'),
            array(
                'date' => '2014-11-21',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000156761914000602/0001567619-14-000602-index.htm',
                'description' => 'MD&A 3Q14'),
            array(
                'date' => '2014-09-03',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000119312514329292/0001193125-14-329292-index.htm',
                'description' => 'MD&A 2Q14'),
            array(
                'date' => '2014-06-04',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000119312514225396/0001193125-14-225396-index.htm',
                'description' => 'MD&A 1Q14'),
            array(
                'date' => '2014-04-30',
                'type' => '20-F',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000119312514171434/d718218d20f.htm',
                'description' => '20-F'),
            array(
                'date' => '2014-04-16',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000119312514144595/d711998d6k.htm',
                'description' => 'Annual General Meeting of Shareholders'),
            array(
                'date' => '2014-04-02',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000119312514128005/0001193125-14-128005-index.htm',
                'description' => 'MD&A 4Q13'),
            array(
                'date' => '2013-12-06',
                'type' => 'F-3/A',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000119312513465284/d590219df3a.htm',
                'description' => 'Amendment No.2'),
            array(
                'date' => '2013-10-23',
                'type' => 'F-3/A',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000119312513407903/d590219df3a.htm',
                'description' => 'Amendment No.1'),
            array(
                'date' => '2013-09-24',
                'type' => 'F-3',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000119312513374918/d590219df3.htm',
                'description' => 'F-3'),
            array(
                'date' => '2013-04-29',
                'type' => '20-F',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000119312513181965/0001193125-13-181965-index.htm',
                'description' => '20-F'),
            array(
                'date' => '2013-04-10',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000119312513147901/d519310d6k.htm',
                'description' => 'Annual General Meeting of Shareholders'),
            array(
                'date' => '2013-01-24',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000119312513022193/d471727d6k.htm',
                'description' => 'Adecoagro Announces Offering Of its Common Shares by Certain Selling Shareholders'),
            array(
                'date' => '2013-01-23',
                'type' => 'F-3/A',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000119312513020716/d458508df3a.htm',
                'description' => 'Amendment No.1'),
            array(
                'date' => '2013-03-01',
                'type' => 'F-3',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000119312513002456/d458508df3.htm',
                'description' => 'F-3'),
            array(
                'date' => '2017-09-06',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000167276417000089/0001672764-17-000089-index.htm',
                'description' => 'MD&A 2Q17'),
            array(
                'date' => '2013-03-28',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000119312512137585/d323949d6k.htm',
                'description' => 'Annual General Meeting of Shareholders'),
            array(
                'date' => '2012-12-02',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000095012311101771/y93713e6vk.htm',
                'description' => 'Announcement of Sale of Farm in Argentina'),
            array(
                'date' => '2012-08-19',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000095012311078667/y92428ae6vk.htm',
                'description' => 'Press Release Regarding the Acquisition of the â€œEl Coloradoâ€ Farm in Argentina'),
            array(
                'date' => '2011-06-29',
                'type' => '20-F',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000095012311062709/0000950123-11-062709-index.htm',
                'description' => '20-F'),
            array(
                'date' => '2011-04-07',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000095012311033480/y90641e6vk.htm',
                'description' => 'Annual General Meeting of Shareholders'),
            array(
                'date' => '2011-04-06',
                'type' => 'S-8',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000095012311033297/y89795sv8.htm',
                'description' => 'S-8'),
            array(
                'date' => '2011-01-13',
                'type' => 'F-1',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000095012311002460/0000950123-11-002460-index.htm',
                'description' => 'F-1'),
            array(
                'date' => '2018-06-01',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000162828018007501/0001628280-18-007501-index.htm',
                'description' => 'MD&A 1Q18'),
            array(
                'date' => '2018-09-07',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000162828018011687/0001628280-18-011687-index.htm',
                'description' => 'MD&A 2Q18'),
            array(
                'date' => '2012-04-30',
                'type' => '20-F',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000119312512192183/0001193125-12-192183-index.htm',
                'description' => '20-F'),
            array(
                'date' => '2018-12-11',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000162828018015006/0001628280-18-015006-index.htm',
                'description' => 'MD&A 3Q18'),
            array(
                'date' => '2019-02-11',
                'type' => '6K',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000114036119002893/form6k.htm',
                'description' => 'Acquisition of two milk processing plants and two trademarks'),
            array(
                'date' => '2019-04-29',
                'type' => '20-F',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000162828019004963/agro1231201820-ffinal.htm',
                'description' => '20-F'),
            array(
                'date' => '2020-04-28',
                'type' => '20-F',
                'link' => 'https://www.sec.gov/Archives/edgar/data/1499505/000162828020005757/agro1231201920-ffinal.htm',
                'description' => '20-F'),
            array(
                'date' => '2021-04-28',
                'type' => '20-F',
                'link' => 'https://www.sec.gov/ix?doc=/Archives/edgar/data/1499505/000162828021007998/agro-20201231.htm',
                'description' => '20-F')
        );

        foreach ($data as $row)
            DB::table('sec_filings')->insert($row);
    }
}
