<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Panel de Administración') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- jQuery, DataTable y DatePicker -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js" defer></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.3/r-2.2.9/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.3/r-2.2.9/datatables.min.js" defer></script>

    <script type="text/javascript">
      $(document).ready( function () {
        $('.custom-file-input').change(function() {
          // var i = $(this).next('label').clone();
          var file = $('.custom-file-input')[0].files[0].name;
          $('.filename').text(file);
          // $(this).prev('label').text(file);
        });
        $('.thumb-container').hover(() => {
          $(this).find('.thumb-change-bg').animate({opacity: 0.5}, 300);
          $(this).find('.thumb-change').animate({opacity: 1}, 300);
        }, () => {
          $(this).find('.thumb-change-bg').animate({opacity: 0}, 300);
          $(this).find('.thumb-change').animate({opacity: 0}, 300);
        });
        // if ($(window).width() < 768)
        $('.table').DataTable({ responsive: true });
        $('input[name="date"]').datepicker( { dateFormat: "mm-dd-yy" });
      });
    </script>
    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('css/admin.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
      <nav class="navbar navbar-light sticky-top bg-white flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="#">
          <img src="{{ asset('/img/logo.svg') }}" class="w-75" />
        </a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <!-- <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search"> -->
        <ul class="navbar-nav px-3">
          <li class="nav-item text-nowrap">
            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar sesión</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
          </form>
          </li>
        </ul>
      </nav>

      <div class="container-fluid">
        <div class="row">
          <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="sidebar-sticky pt-3">
              @foreach ($sections as $group)
              <ul class="nav flex-column">
              <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                <span>{{ $group['title']}}</span>
              </h6>
              @foreach ($group['sections'] as $section)
                <li class="nav-item">
                  <a class="nav-link {{ $section['active'] ? 'active' : ''}}" href="/panel/{{ $section['link'] }}">
                    {{ $section['title'] }}
                  </a>
                </li>
                @endforeach
              </ul>
              @endforeach
            </div>
          </nav>

          <main role="main" class="col-md-9 ml-sm-auto col-lg-10">
            @yield('content')
          </main>
        </div>
      </div>
    </div>
</body>
</html>
