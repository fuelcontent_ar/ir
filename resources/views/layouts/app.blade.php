<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- font-awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" integrity="sha512-Fo3rlrZj/k7ujTnHg4CGR2D7kSs0v4LLanw2qksYuRlEzO+tcaEPQogQ0KaoGN26/zrn20ImR1DfuLWnOo7aBA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="body">
    <div id="app">
        @section('header')
        <nav class="nav">
            <div class="container navigation">
                <div class="logo">
                   <a href="/">
                      <img src="{{asset('img/logo.svg')}}" class="logo-img" alt="logo">
                    </a>
                    <i class="fas fa-bars" id="hambur"></i>
                </div>
                <ul class="menu ocultarmenu" id="menu">
                    <li  style="background-color:#8ec549">
                        <a href="" class="company">Company <i class="fas fa-angle-down"></i>
                        <ul class=" container ocultar" id="submenuCompany">
                             <li><a href="/company/strategy">Strategy</a></li>
                             <li><a href="/company/organizational">Organizational Structure</a></li>
                             <li><a id="insti" href="/storage/files/ipresentation/Adecoagro Institutional Presentation.pdf" download>Institutional Presentation</a></li>
                             <li><a href="/company/business">Business Divisions</a></li>
                             <li><a href="/company/education">Investor Education</a></li>
                             <li><a href="/company/investor">Investor Day</a></li>
                             <li><a href="/storage/files/our_numbers/Our_Numbers_2020_FV_PV.xlsx">Our Numbers</a></li>
                        </ul>
                    </a>
                    </li>
                    <li style="background-color:#2d9067"><a href="" class="governance">Governance <i class="fas fa-angle-down"></i>
                    <ul class="container ocultar" id="submenuGovernance">
                        <li><a href="/governance/board-of-directors">Board of Directors</a></li>
                        <li><a href="/governance/officers">Executive Officers</a></li>
                        <li><a href="/governance/policy">Dividend policy</a></li>
                        <li><a href="/governance/bylaws">Bylaws</a></li>
                        <li><a href="/governance/ethics">Code of Ethics</a></li>
                        <li><a href="/governance/insider">Insider Trading Policy</a></li>
                        <li><a href="/governance/annual">Annual General Meeting</a></li>
                    </ul>
                    </a></li>
                    <li style="background-color:#005838"><a href="" class="financials">Financials <i class="fas fa-angle-down"></i>
                    <ul class="container ocultar" id="submenuFinancials">
                       <li><a href="/financials/quarterly-earnings">Quarterly Earnings</a></li>
                       <li><a href="/financials/quarterly-earnings#filings">SEC Filings</a></li>
                       <li><a href="/financials/analyst">Analyst Coverage</a></li>
                       <li><a href="/financials/quarterly-earnings#cvm">CVM</a></li>
                    </ul>
                   </a></li>
                    <li style="background-color:#666666"><a href="/our-assets">Our Assets </a></li>
                    <li style="background-color:#bbbbbb"><a href="https://www.adecoagro.com/" target="_blank"><img src="" alt=""><img src="{{asset('img/home/iso-adecoagro.png')}}" style="width:30px" alt=""> Institutional </a></li>
                </ul>
            </div>
        </nav>
        <div class="sub sub-company ocultar" id="sub">

        </div>
        @show
        <main class="">
            @yield('content')
        </main>
        @section('footer')
        <footer class="footer">
            <div class="container privacy">
               <div>
                 <h3>adecoagro</h3>
                 <a href="https://www.adecoagro.com/" target="_blank">Institucional</a>
                 <a href="/company/business">Business Divisions</a>
                 <a href="/contacto">Contact</a>
                </div>
               <div class="legal">
                 <h3>Legal + Privacy</h3>
                 <a href="/policy">Provacy Policy</a>
                 <a href="/terms">Terms of Use</a>
                </div>
            </div>
                <div class="copy container mt-4">
                    <p>©Copyright 2002-2018 Adecoagro. <br>
                    All Rights Reserved.
                    </p>
                    <img src="{{ asset('img/home/footer-marks.png') }}" alt="">
                </div>
        </footer>
        @show
    </div>
</body>
</html>
