@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="https://unpkg.com/external-svg-loader@1.0.0/svg-loader.min.js" async></script>
<h2>Popup</h2>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endisset

@if(isset($entry))
<a href="{{ action('PopupController@index') }}" class="back-link">Volver atrás</a>
    <div class="col-md-8 offset-md-2 text-center">
        <h3>Actualizar registro</h3>
        <form class="px-2 w-100 form-strong-labels" method="POST" action="{{ action('PopupController@update', $entry->id) }}" enctype="multipart/form-data">
            @csrf
            @if ($entry->exists)
                @method('PATCH')
            @endif
            @if (session('error'))
            <div class="alert-danger error-box">
                    <span>{{ session('error') }}</span>
            </div>
            @endif
            <div class="form-row text-left">
                <div class="form-group w-75">
                <label for="title">Título</label>
                <input type="text" class="form-control w-100 @error('title') is-invalid @enderror" id="title" value="{{ old('title', $entry->title) }}" name="title" placeholder="Título">
                @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="text">Activo</label>
                <select name="active" id="active" class="form-control">
                    <option value="1" @if (old('active', $entry->active) == 1) selected @endif>Sí</option>
                    <option value="0" @if (old('active', $entry->active) == 0) selected @endif>No</option>
                </select>
                @error('active')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left w-100">
                <div class="form-group w-100">
                <label for="text">Texto</label>
                <textarea class="form-control @error('text') is-invalid @enderror" name="text" cols="40" rows="6" id="text" placeholder="Texto">{{ old('text', $entry->text) }}</textarea>
                @error('text')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-success">{{ $entry->exists ? 'Actualizar' : 'Agregar'}}</button>
        </form>
    </div>
@else
    <div class="table-responsive">
        <table class="table table-striped table-sm table-abm">
        <thead>
            <tr>
            <th>Título</th>
            <th>Texto</th>
            <th>Activo</th>
            <th>Editar</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($entries as $entry)
            <tr>
            <td>{{ $entry->title }}</td>
            <td>{{ $entry->text }}</td>
            <td>{{ $entry->active == 1 ? 'Sí' : 'No' }}</td>
            <td>
                <form method="GET" action="{{ action('PopupController@edit', $entry->id) }}">
                <a href="#" onclick="this.closest('form').submit();return false;">
                    <svg class="icon-hover" data-src="{{ asset('/img/icons/pencil-square.svg') }}" type="image/svg+xml"></svg>
                </a>
                </form>
            </td>
            </tr>
        @endforeach
        </tbody>
        </table>
    </div>
@endif
@stop
