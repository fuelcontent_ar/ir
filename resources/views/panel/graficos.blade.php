@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="https://unpkg.com/external-svg-loader@1.0.0/svg-loader.min.js" async></script>
<h2>Gráficos</h2>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endisset

@if(isset($entry))
<a href="{{ action('GraficosController@index') }}" class="back-link">Ir a la lista de Gráficos</a>
    <div class="col-md-8 offset-md-2 text-center">
        <h3>{{ $entry->exists ? 'Actualizar' : 'Nueva' }} gráfico</h3>
        <form class="px-2 w-100 form-strong-labels" method="POST" action="{{ $entry->exists ? action('GraficosController@update', $entry->id) : action('GraficosController@store') }}" enctype="multipart/form-data">
            @csrf
            @if ($entry->exists)
                @method('PATCH')
            @endif
            @if (session('error'))
            <div class="alert-danger error-box">
                    <span>{{ session('error') }}</span>
            </div>
            @endif
            <div class="form-row text-left pr-4">
                <div class="form-group w-100">
                <label for="title">Título</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" value="{{ old('title', $entry->title) }}" name="title" placeholder="Título">
                @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                    @if ($entry->exists)
                        <label for="input">Imagen (JPG/PNG, máximo 2 MB's): </label><span class="filename">&nbsp;</span><br /> 
                        <div class="thumb-container">
                            <div class="thunb-preview">
                                <img src="{{ asset('storage/files/graficos/' . $entry->file) }}" />
                            </div>
                            <div class="thumb-change-bg">&nbsp;</div>
                            <div class="thumb-change">
                                <label>
                                    <input type="file" class="form-control @error('input') is-invalid @enderror custom-file-input" name="input" />
                                    <span class="btn btn-primary">Reemplazar</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-12 text-center pt-2">
                            <a class="btn btn-primary w-100" href="{{ action('FileDownloadController@download', 'file=graficos/' . $entry->file) }}" target="_blank">Descargar</a>
                        </div>
                        @error('input')
                            <div class="alert alert-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </div>
                        @enderror
                    @else
                    <label for="input">Imagen</label>
                        <input type="file" name="input" class="form-control @error('input') is-invalid @enderror" name="input" />
                        @error('input')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    @endif
                </div>
            </div>
            <button type="submit" class="btn btn-success">{{ $entry->exists ? 'Actualizar' : 'Agregar'}}</button>
        </form>
    </div>
@else
    @if (count($entries) < 2) 
        <div class="col-md-3 offset-md-9 text-right px-0">
            <a href="{{ action('GraficosController@create') }}" type="submit" class="btn btn-success">+ Nuevo</a>
        </div>
    @endif
    <div class="table-responsive">
        <table class="table table-striped table-sm table-abm">
        <thead>
            <tr>
            <th>Título</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($entries as $entry)
            <tr>
            <td>{{ $entry->title }}</td>
            <td>
                <form method="GET" action="{{ action('GraficosController@edit', $entry->id) }}">
                <a href="#" onclick="this.closest('form').submit();return false;">
                    <svg class="icon-hover" data-src="{{ asset('/img/icons/pencil-square.svg') }}" type="image/svg+xml"></svg>
                </a>
                </form>
            </td>
            <td>
                <form method="POST" action="{{ action('GraficosController@destroy', $entry->id) }}">
                    @csrf
                    {{ method_field('DELETE') }}
                    <a href="#" onclick="this.closest('form').submit();return false;">
                        <svg class="icon-hover" data-src="{{ asset('/img/icons/trash.svg') }}"></svg>
                    </a>
                </form>
            </td>
            </tr>
        @endforeach
        </tbody>
        </table>
    </div>
@endif
@stop