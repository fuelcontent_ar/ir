@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="https://unpkg.com/external-svg-loader@1.0.0/svg-loader.min.js" async></script>
<h2>A. C. Equity</h2>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endisset

@if(isset($entry))
<a href="{{ action('AcEquityController@index') }}" class="back-link">Ir a la lista de A. C. Equity</a>
    <div class="col-md-8 offset-md-2 text-center">
        <h3>{{ $entry->exists ? 'Actualizar' : 'Nuevo' }} registro</h3>
        <form class="px-2 w-100 form-strong-labels" method="POST" action="{{ $entry->exists ? action('AcEquityController@update', $entry->id) : action('AcEquityController@store') }}" enctype="multipart/form-data">
            @csrf
            @if ($entry->exists)
                @method('PATCH')
            @endif
            @if (session('error'))
                <div class="alert-danger error-box">
                        <span>{{ session('error') }}</span>
                </div>
            @endif
            <div class="form-row text-left">
                <div class="form-group">
                <label for="title">Firm</label>
                <input type="text" class="form-control @error('firm') is-invalid @enderror" id="firm" value="{{ old('firm', $entry->firm) }}" name="firm" placeholder="Firm">
                @error('firm')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="title">Analyst</label>
                <input type="text" class="form-control @error('analyst') is-invalid @enderror" id="analyst" value="{{ old('analyst', $entry->analyst) }}" name="analyst" placeholder="Analyst">
                @error('analyst')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="title">Phone</label>
                <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" value="{{ old('phone', $entry->phone) }}" name="phone" placeholder="Phone">
                @error('phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="title">Email</label>
                <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email', $entry->email) }}" name="email" placeholder="Email">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="title">Recommendation</label>
                <input type="text" class="form-control @error('recommendation') is-invalid @enderror" id="recommendation" value="{{ old('recommendation', $entry->recommendation) }}" name="recommendation" placeholder="Recommendation">
                @error('recommendation')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="title">TP</label>
                <input type="text" class="form-control @error('tp') is-invalid @enderror" id="tp" value="{{ old('tp', $entry->tp) }}" name="tp" placeholder="TP">
                @error('tp')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="title">Date</label>
                <input type="text" class="form-control @error('date') is-invalid @enderror" id="date" value="{{ old('date', $entry->date) }}" name="date" placeholder="Date">
                @error('date')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-success">{{ $entry->exists ? 'Actualizar' : 'Agregar'}}</button>
        </form>
    </div>
@else
    <div class="col-md-3 offset-md-9 text-right px-0">
            <a href="{{ action('AcEquityController@create') }}" type="submit" class="btn btn-success">+ Nuevo</a>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-sm table-abm">
        <thead>
            <tr>
            <th>Firm</th>
            <th>Analyst</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Recommendation</th>
            <th>TP</th>
            <th>Date</th>
            <th>Editar</th>
            <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($entries as $entry)
            <tr>
            <td>{{ $entry->firm }}</td>
            <td>{{ $entry->analyst }}</td>
            <td>{{ $entry->phone }}</td>
            <td>{{ $entry->email }}</td>
            <td>{{ $entry->recommendation }}</td>
            <td>{{ $entry->tp }}</td>
            <td>{{ $entry->date }}</td>
            <td>
                <form method="GET" action="{{ action('AcEquityController@edit', $entry->id) }}">
                <a href="#" onclick="this.closest('form').submit();return false;">
                    <svg class="icon-hover" data-src="{{ asset('/img/icons/pencil-square.svg') }}" type="image/svg+xml"></svg>
                </a>
                </form>
            </td>
            <td>
                <form method="POST" action="{{ action('AcEquityController@destroy', $entry->id) }}">
                    @csrf
                    {{ method_field('DELETE') }}
                    <a href="#" onclick="this.closest('form').submit();return false;">
                        <svg class="icon-hover" data-src="{{ asset('/img/icons/trash.svg') }}"></svg>
                    </a>
                </form>
            </td>
            </tr>
        @endforeach
        </tbody>
        </table>
        <br /><br />
        <h4>Legales:</h4>
        @if (session('success_legales'))
            <div class="alert alert-success">
                {{ session('success_legales') }}
            </div>
        @endisset
        <form class="px-2 w-100 form-strong-labels" method="POST" action="{{ action('LegalesController@update', $legales->id) }}" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            @if (session('error_legales'))
            <div class="alert-danger error-box">
                    <span>{{ session('error_legales') }}</span>
            </div>
            @endif
            <div class="form-row text-left">
                <div class="form-group">

                <textarea cols="70" rows="8" class="form-control @error('equity') is-invalid @enderror" id="equity" name="equity" placeholder="Legales">{{ old('equity', $legales->equity) }}</textarea>
                @error('equity')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-success">Actualizar Legales</button>
        </form>
    </div>
@endif
@stop