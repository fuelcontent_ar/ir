@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="https://unpkg.com/external-svg-loader@1.0.0/svg-loader.min.js" async></script>
<h2>Entrevistas</h2>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endisset

@if(isset($entry))
<a href="{{ action('EntrevistaController@index') }}" class="back-link">Volver atrás</a>
    <div class="col-md-8 offset-md-2 text-center">
        <h3>Actualizar registro</h3>
        <form class="px-2 w-100 form-strong-labels" method="POST" action="{{ action('EntrevistaController@update', $entry->id) }}" enctype="multipart/form-data">
            @csrf
            @if ($entry->exists)
                @method('PATCH')
            @endif
            @if (session('error'))
            <div class="alert-danger error-box">
                    <span>{{ session('error') }}</span>
            </div>
            @endif
            <div class="row">
                <div class="col-md-4 text-left">
                    <div class="form-group w-75">
                    <label for="footer">Pie</label>
                    <input type="text" class="form-control w-100 @error('footer') is-invalid @enderror" id="footer" value="{{ old('footer', $entry->footer) }}" name="footer" placeholder="Pie">
                    @error('footer')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>
                <div class="col-md-4 text-left">
                    <div class="form-group">
                    <label for="date">Fecha</label>
                    <input type="text" class="form-control w-100 @error('date') is-invalid @enderror" id="date" value="{{ old('date', \Carbon\Carbon::createFromFormat('Y-m-d', $entry->date)->format('m-d-Y')) }}" name="date" placeholder="Fecha">
                    @error('date')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>
                <div class="col-md-4 text-left">
                    <div class="form-group">
                    <label for="text">Activo</label>
                    <select name="active" id="active" class="form-control">
                        <option value="1" @if (old('active', $entry->active) == 1) selected @endif>Sí</option>
                        <option value="0" @if (old('active', $entry->active) == 0) selected @endif>No</option>
                    </select>
                    @error('active')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-left">
                    <div class="form-group">
                    <label for="input">Foto (JPG/PNG, máximo 2 MB's): </label><span class="filename">&nbsp;</span><br /> 
                    <div class="thumb-container">
                        <div class="thunb-preview">
                            <img src="{{ asset('storage/files/entrevistas/' . $entry->file) }}" />
                        </div>
                        <div class="thumb-change-bg">&nbsp;</div>
                        <div class="thumb-change">
                            <!-- <label for="input">Foto (JPG/PNG, máximo 2 MB's)</label><br /> -->
                            <label>
                                <input type="file" class="form-control @error('input') is-invalid @enderror custom-file-input" name="input" />
                                <span class="btn btn-primary">Reemplazar</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 text-center pt-2">
                        <a class="btn btn-primary w-100" href="{{ action('FileDownloadController@download', 'file=entrevistas/' . $entry->file) }}" target="_blank">Descargar</a>
                    </div>
                    @error('input')
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                    @enderror
                    <!-- @if ($entry->exists)
                    <span role="alert">
                        <div class="container w-100">
                        <div class="row">
                            <div class="col-6">
                            <span><b>Imagen actual:</b> </span><br />
                            <a class="btn btn-primary" href="{{ action('FileDownloadController@download', 'file=entrevistas/' . $entry->file) }}" target="_blank">Descargar</a>
                            </div>
                            <div class="col-6 text-left">
                                <img class="img-thumb" src="{{ asset('storage/files/entrevistas/' . $entry->file) }}" />
                            </div>
                        </div>
                        </div>
                    </span>
                    @endif -->
                    </div>
                </div>
                <div class="col-md-8 text-left">
                    <div class="form-group w-100 h-100">
                        <label for="content">Contenido</label>
                        <textarea class="form-control @error('content') is-invalid @enderror" name="content" cols="40" rows="10" id="content" placeholder="Contenido">{{ old('content', $entry->content) }}</textarea>
                        @error('content')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-success">Actualizar</button>
        </form>
    </div>
@else
    <div class="table-responsive">
        <table class="table table-striped table-sm table-abm">
        <thead>
            <tr>
            <th>Entrevista</th>
            <th>Activo</th>
            <th>Editar</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($entries as $entry)
            <tr>
            <td>{{ $entry->footer }}</td>
            <td>{{ $entry->active == 1 ? 'Sí' : 'No' }}</td>
            <td>
                <form method="GET" action="{{ action('EntrevistaController@edit', $entry->id) }}">
                <a href="#" onclick="this.closest('form').submit();return false;">
                    <svg class="icon-hover" data-src="{{ asset('/img/icons/pencil-square.svg') }}" type="image/svg+xml"></svg>
                </a>
                </form>
            </td>
            </tr>
        @endforeach
        </tbody>
        </table>
    </div>
@endif
@stop
