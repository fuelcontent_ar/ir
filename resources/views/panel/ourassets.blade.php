@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="https://unpkg.com/external-svg-loader@1.0.0/svg-loader.min.js" async></script>
<h2>Our Assets</h2>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endisset

@if(isset($entry))
<a href="{{ action('OurAssetsController@index') }}" class="back-link">Ir a la lista de Our Assets</a>
    <div class="col-md-8 offset-md-2 text-center">
        <h3>{{ $entry->exists ? 'Actualizar' : 'Nuevo'}} archivo</h3>
        <form class="px-2 w-100 form-strong-labels" method="POST" action="{{ action('OurAssetsController@update', $entry->id) }}" enctype="multipart/form-data">
            @csrf
            @if ($entry->exists)
                @method('PATCH')
            @endif
            @if (session('error'))
            <div class="alert-danger error-box">
                    <span>{{ session('error') }}</span>
            </div>
        @endif
            <div class="form-row text-left w-100">
                <div class="form-group w-100">
                <label for="title">Título</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" value="{{ old('title', $entry->title) }}" name="title" placeholder="Título">
                @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="text">Archivo</label>
                <input type="file" name="input" class="form-control @error('input') is-invalid @enderror" />
                @error('input')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                
                @if ($entry->exists)
                <span role="alert">
                    <span><b>Archivo actual:</b> <a href="{{ asset('storage/files/our_assets/' . $entry->file) }}" target="_blank">{{ $entry->file }}</a></span><br />
                    <strong>Deja este campo en blanco si no deseas modificar el archivo</strong>
                </span>
                @endif
                </div>
            </div>
            <button type="submit" class="btn btn-success">{{ $entry->exists ? 'Actualizar' : 'Agregar'}}</button>
        </form>
    </div>
@else
    <div class="table-responsive">
        <table class="table table-striped table-sm table-abm">
        <thead>
            <tr>
            <th>Título</th>
            <th>Archivo</th>
            <th>Editar</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($entries as $entry)
            <tr>
            <td>{{ $entry->title }}</td>
            <td><a href="{{ asset('storage/files/our_assets/' . $entry->file) }}" target="_blank">{{ $entry->file }}</a></td>
            <td>
                <form method="GET" action="{{ action('OurAssetsController@edit', $entry->id) }}">
                <a href="#" onclick="this.closest('form').submit();return false;">
                    <svg class="icon-hover" data-src="{{ asset('/img/icons/pencil-square.svg') }}" type="image/svg+xml"></svg>
                </a>
                </form>
            </td>
            </tr>
        @endforeach
        </tbody>
        </table>
    </div>
@endif
@stop