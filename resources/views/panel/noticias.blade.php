@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="https://unpkg.com/external-svg-loader@1.0.0/svg-loader.min.js" async></script>
<h2>Noticias</h2>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endisset

@if(isset($entry))
<a href="{{ action('NoticiasController@index') }}" class="back-link">Ir a la lista de noticias</a>
    <div class="col-md-8 offset-md-2 text-center">
        <h3 class="text-center">{{ $entry->exists ? 'Actualizar' : 'Nueva'}} noticia</h3>
        <form class="px-2 w-100 form-strong-labels" method="POST" action="{{ $entry->exists ? action('NoticiasController@update', $entry->id) : action('NoticiasController@store') }}" enctype="multipart/form-data">
            @csrf
            @if ($entry->exists)
                @method('PATCH')
            @endif
            @if (session('error'))
                <div class="alert-danger error-box">
                        <span>{{ session('error') }}</span>
                </div>
            @endif
            <div class="form-row text-left">
                <div class="form-group w-75">
                <label for="title">Título</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" value="{{ old('title', $entry->title) }}" name="title" placeholder="Título">
                @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="subtitle">Subtítulo</label>
                <input type="text" class="form-control @error('subtitle') is-invalid @enderror" id="subtitle" value="{{ old('subtitle', $entry->subtitle) }}" name="subtitle" placeholder="Subtítulo">
                @error('subtitle')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left"  style="clear: left;">
                <div class="form-group w-75">
                <label for="copete">Copete</label>
                <input type="text" class="form-control @error('copete') is-invalid @enderror" id="copete" value="{{ old('copete', $entry->copete) }}" name="copete" placeholder="Copete">
                @error('copete')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="link">Link</label>
                <input type="text" class="form-control @error('link') is-invalid @enderror" id="link" value="{{ old('link', $entry->link) }}" name="link" placeholder="Link">
                @error('link')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left w-100">
                <div class="form-group w-100">
                <label for="text">Texto</label>
                <textarea class="form-control @error('text') is-invalid @enderror" name="text" cols="40" rows="6" id="text" placeholder="Texto">{{ old('text', $entry->text) }}</textarea>
                @error('text')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="date">Fecha</label>
                <input type="text" class="form-control @error('date') is-invalid @enderror" id="date" value="{{ $entry->exists || strlen(old('date')) > 0 ? $entry->date : ''}}" name="date" placeholder="MM-DD-YYYY">
                @error('date')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                @if ($entry->exists)
                    <label for="input">Imagen (JPG/PNG, máximo 2 MB's): </label><span class="filename">&nbsp;</span><br /> 
                    <div class="thumb-container">
                        <div class="thunb-preview">
                            <img src="{{ asset('storage/files/noticias/' . $entry->image) }}" />
                        </div>
                        <div class="thumb-change-bg">&nbsp;</div>
                        <div class="thumb-change">
                            <label>
                                <input type="file" name="image" class="form-control @error('image') is-invalid @enderror custom-file-input" />
                                <span class="btn btn-primary">Reemplazar</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 text-center pt-2">
                        <a class="btn btn-primary w-100" href="{{ action('FileDownloadController@download', 'file=noticias/' . $entry->image) }}" target="_blank">Descargar</a>
                    </div>
                    @error('image')
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                    @enderror
                @else
                    <label for="thumb">Imagen (JPG/PNG, máximo 2 MBs)</label>
                    <input type="file" name="image" class="form-control @error('thumb') is-invalid @enderror" />
                    @error('image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                @endif
                </div>
            </div>
            <button type="submit" class="btn btn-success">{{ $entry->exists ? 'Actualizar' : 'Agregar'}}</button>
        </form>
    </div>
@else
    <div class="col-md-3 offset-md-9 text-right px-0">
        <a href="{{ action('NoticiasController@create') }}" type="submit" class="btn btn-success">+ Nueva noticia</a>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-sm table-abm">
        <thead>
            <tr>
            <th>Título</th>
            <th>Subtítulo</th>
            <th>Copete</th>
            <th>Fecha</th>
            <th>Link</th>
            <th>Editar</th>
            <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($entries as $entry)
            <tr>
            <td>{{ $entry->title }}</td>
            <td>{{ $entry->subtitle }}</td>
            <td>{{ \Illuminate\Support\Str::limit($entry->copete, 50) }}</td>
            <td>{{ $entry->date }}</td>
            <td><a href="{{ $entry->link }}" target="_blank">{{ $entry->link }}</a></td>
            <td>
                <form method="GET" action="{{ action('NoticiasController@edit', $entry->id) }}">
                <a href="#" onclick="this.closest('form').submit();return false;">
                    <svg class="icon-hover" data-src="{{ asset('/img/icons/pencil-square.svg') }}" type="image/svg+xml"></svg>
                </a>
                </form>
            </td>
            <td>
                <form method="POST" action="{{ action('NoticiasController@destroy', $entry->id) }}">
                    @csrf
                    {{ method_field('DELETE') }}
                    <a href="#" onclick="this.closest('form').submit();return false;">
                        <svg class="icon-hover" data-src="{{ asset('/img/icons/trash.svg') }}"></svg>
                    </a>
                </form>
            </td>
            </tr>
        @endforeach
        </tbody>
        </table>
    </div>
@endif
@stop
