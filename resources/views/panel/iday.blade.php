@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="https://unpkg.com/external-svg-loader@1.0.0/svg-loader.min.js" async></script>
<h2>Investor Day</h2>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endisset

@if(isset($entry))
<a href="{{ action('InvestorDayController@index') }}" class="back-link">Ir a la lista de contenidos</a>
    <div class="col-md-8 offset-md-2 text-center">
        <h3>{{ $entry->exists ? 'Actualizar' : 'Nuevo' }} contenido</h3>
        <form class="px-2 w-100 form-strong-labels" method="POST" action="{{ $entry->exists ? action('InvestorDayController@update', $entry->id) : action('InvestorDayController@store') }}" enctype="multipart/form-data">
            @csrf
            @if ($entry->exists)
                @method('PATCH')
            @endif
            @if (session('error'))
            <div class="alert-danger error-box">
                    <span>{{ session('error') }}</span>
            </div>
            @endif
            <div class="form-row text-left">
                <div class="form-group">
                <label for="title">Título</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" value="{{ old('title', $entry->title) }}" name="title" placeholder="Título">
                @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="video">Video (código de Vimeo)</label>
                <input type="number" class="form-control @error('video') is-invalid @enderror" id="video" value="{{ old('video', $entry->video) }}" name="video" placeholder="Código de Vimeo">
                @error('video')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left w-100">
                <div class="form-group w-100">
                <label for="description">Descripción</label>
                <textarea class="form-control @error('description') is-invalid @enderror" name="description" cols="40" rows="6" id="description" placeholder="Descripción">{{ old('description', $entry->description) }}</textarea>
                @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left pr-4">
                <div class="form-group">
                <label for="file">Archivo 1</label>
                <input type="file" name="input" class="form-control @error('input') is-invalid @enderror" name="input" />
                @error('input')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                @if ($entry->exists)
                <span role="alert">
                    <span><b>Archivo actual:</b> <a href="{{ asset('storage/files/iday/' . $entry->file) }}" target="_blank">{{ $entry->file }}</a></span><br />
                    <strong>Deja este campo en blanco si no deseas modificar el archivo</strong>
                </span>
                @endif
                </div>
            </div>
            <div class="form-row text-left pr-4">
                <div class="form-group">
                <label for="file">Archivo 2</label>
                <input type="file" name="input2" class="form-control @error('input2') is-invalid @enderror" name="input2" />
                @error('input2')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                @if ($entry->exists)
                <span role="alert">
                    <span><b>Archivo actual:</b> <a href="{{ asset('storage/files/iday/' . $entry->file2) }}" target="_blank">{{ $entry->file2 }}</a></span><br />
                    <strong>Deja este campo en blanco si no deseas modificar el archivo</strong>
                </span>
                @endif
                </div>
            </div>
            <div class="form-row w-100"></div>
            <div class="form-row text-left pr-4">
                <div class="form-group">
                @if ($entry->exists)
                    <label for="input">Miniatura (JPG/PNG, máximo 2 MB's): </label><span class="filename">&nbsp;</span><br /> 
                    <div class="thumb-container">
                        <div class="thunb-preview">
                            <img src="{{ asset('storage/files/iday/' . $entry->thumb) }}" />
                        </div>
                        <div class="thumb-change-bg">&nbsp;</div>
                        <div class="thumb-change">
                            <label>
                                <input type="file" name="thumb" class="form-control @error('input') is-invalid @enderror custom-file-input" />
                                <span class="btn btn-primary">Reemplazar</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 text-center pt-2">
                        <a class="btn btn-primary w-100" href="{{ action('FileDownloadController@download', 'file=iday/' . $entry->thumb) }}" target="_blank">Descargar</a>
                    </div>
                    @error('thumb')
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                    @enderror
                @else
                    <label for="thumb">Miniatura</label>
                    <input type="file" name="thumb" class="form-control @error('thumb') is-invalid @enderror" />
                    @error('thumb')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                @endif
                </div>
            </div>
            <button type="submit" class="btn btn-success">{{ $entry->exists ? 'Actualizar' : 'Agregar'}}</button>
        </form>
    </div>
@else
    <div class="col-md-3 offset-md-9 text-right px-0">
            <a href="{{ action('InvestorDayController@create') }}" type="submit" class="btn btn-success">+ Nuevo</a>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-sm table-abm">
        <thead>
            <tr>
            <th>Título</th>
            <th>Descripción</th>
            <th>Video</th>
            <th>Archivo</th>
            <th>Archivo 2</th>
            <th>Editar</th>
            <th>Borrar</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($entries as $entry)
            <tr>
            <td>{{ $entry->title }}</td>
            <td>{{ $entry->description }}</td>
            <td>{{ $entry->video }}</td>
            <td><a href="{{ asset('storage/files/iday/' . $entry->file) }}" target="_blank">{{ $entry->file }}</a></td>
            <td><a href="{{ asset('storage/files/iday/' . $entry->file2) }}" target="_blank">{{ $entry->file2 }}</a></td>
            <td>
                <form method="GET" action="{{ action('InvestorDayController@edit', $entry->id) }}">
                <a href="#" onclick="this.closest('form').submit();return false;">
                    <svg class="icon-hover" data-src="{{ asset('/img/icons/pencil-square.svg') }}" type="image/svg+xml"></svg>
                </a>
                </form>
            </td>
            <td>
                <form method="POST" action="{{ action('InvestorDayController@destroy', $entry->id) }}">
                    @csrf
                    {{ method_field('DELETE') }}
                    <a href="#" onclick="this.closest('form').submit();return false;">
                        <svg class="icon-hover" data-src="{{ asset('/img/icons/trash.svg') }}"></svg>
                    </a>
                </form>
            </td>
            </tr>
        @endforeach
        </tbody>
        </table>
    </div>
@endif
@stop