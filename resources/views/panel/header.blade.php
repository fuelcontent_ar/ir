@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="https://unpkg.com/external-svg-loader@1.0.0/svg-loader.min.js" async></script>
<h2>Headers</h2>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endisset

@if(isset($entry))
<a href="{{ action('HeaderController@index') }}" class="back-link">Ir a la lista de headers</a>
    <div class="col-md-8 offset-md-2 text-center">
        <h3>{{ $entry->exists ? 'Actualizar' : 'Nuevo'}} header</h3>
        <form class="px-2 w-100 form-strong-labels" method="POST" action="{{ $entry->exists ? action('HeaderController@update', $entry->id) : action('HeaderController@store') }}" enctype="multipart/form-data">
            @csrf
            @if ($entry->exists)
                @method('PATCH')
            @endif
            @if (session('error'))
            <div class="alert-danger error-box">
                    <span>{{ session('error') }}</span>
            </div>
        @endif
            <div class="form-row text-left">
                <div class="form-group w-75">
                <label for="title">Título</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" value="{{ old('title', $entry->title) }}" name="title" placeholder="Título">
                @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                    @if ($entry->exists)
                        <label for="image">Imagen (JPG, máximo 2 MB's): </label><span class="filename">&nbsp;</span><br /> 
                        <div class="thumb-container">
                            <div class="thunb-preview">
                                <img src="{{ asset('storage/files/headers/' . $entry->id . '.jpg') }}" />
                            </div>
                            <div class="thumb-change-bg">&nbsp;</div>
                            <div class="thumb-change">
                                <label>
                                    <input type="file" name="image" class="form-control @error('image') is-invalid @enderror custom-file-input" />
                                    <span class="btn btn-primary">Reemplazar</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-12 text-center pt-2">
                            <a class="btn btn-primary w-100" href="{{ action('FileDownloadController@download', 'file=headers/' . $entry->id . '.jpg') }}" target="_blank">Descargar</a>
                        </div>
                        @error('image')
                            <div class="alert alert-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </div>
                        @enderror
                    @else
                        <label for="thumb">Imagen (JPG, máximo 2 MB's)</label>
                        <input type="file" name="thumb" class="form-control @error('thumb') is-invalid @enderror" />
                        @error('thumb')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    @endif
                </div>
            </div>
            <div class="form-row text-left w-100">
                <div class="form-group w-100">
                <label for="text">Texto</label>
                <textarea class="form-control @error('text') is-invalid @enderror" name="text" cols="40" rows="6" id="text" placeholder="Texto">{{ old('text', $entry->text) }}</textarea>
                @error('text')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-success">{{ $entry->exists ? 'Actualizar' : 'Agregar'}}</button>
        </form>
    </div>
@else
    <div class="col-md-3 offset-md-9 text-right px-0">
            <a href="{{ action('HeaderController@create') }}" type="submit" class="btn btn-success">+ Nuevo header</a>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-sm table-abm">
        <thead>
            <tr>
            <th>Título</th>
            <th>Texto</th>
            <th>Editar</th>
            <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($entries as $entry)
            <tr>
            <td>{{ $entry->title }}</td>
            <td>{{ $entry->text }}</td>
            <td>
                <form method="GET" action="{{ action('HeaderController@edit', $entry->id) }}">
                <a href="#" onclick="this.closest('form').submit();return false;">
                    <svg class="icon-hover" data-src="{{ asset('/img/icons/pencil-square.svg') }}" type="image/svg+xml"></svg>
                </a>
                </form>
            </td>
            <td>
                <form method="POST" action="{{ action('HeaderController@destroy', $entry->id) }}">
                    @csrf
                    {{ method_field('DELETE') }}
                    <a href="#" onclick="this.closest('form').submit();return false;">
                        <svg class="icon-hover" data-src="{{ asset('/img/icons/trash.svg') }}"></svg>
                    </a>
                </form>
            </td>
            </tr>
        @endforeach
        </tbody>
        </table>
    </div>
@endif
@stop