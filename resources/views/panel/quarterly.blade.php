@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="https://unpkg.com/external-svg-loader@1.0.0/svg-loader.min.js" async></script>
<h2>Financials - Quarterly Earning</h2>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endisset

@if(isset($entry))
<a href="{{ action('FinancialFilesController@index') }}" class="back-link">Ir a la lista de archivos</a>
    <div class="col-md-8 offset-md-2">
        <h3>{{ $entry->exists ? 'Actualizar' : 'Nuevo'}} archivo</h3>
        <form class="px-2 w-100 form-strong-labels" method="POST" action="{{ $entry->exists ? action('FinancialQuartersController@update', $entry->id) : action('FinancialQuartersController@store') }}" enctype="multipart/form-data">
            @csrf
            @if ($entry->exists)
                @method('PATCH')
            @endif
            @if (session('error'))
            <div class="alert-danger error-box">
                    <span>{{ session('error') }}</span>
            </div>
        @endif
            <div class="form-row text-left">
                <div class="form-group">
                <label for="title">Título</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" value="{{ old('title', $entry->title) }}" name="title" placeholder="Título">
                @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="year">Year</label>
                <input type="text" class="form-control @error('year') is-invalid @enderror" id="year" value="{{ old('year', $entry->year) }}" name="year" placeholder="Año">
                @error('year')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="text">Archivo</label>
                <input type="file" name="input" class="form-control @error('input') is-invalid @enderror" name="input" />
                @error('input')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                @if ($entry->exists)
                <span role="alert">
                    <span><b>Archivo actual:</b> <a href="{{ asset('storage/files/financial_quarters/' . $entry->file) }}" target="_blank">{{ $entry->file }}</a></span><br />
                    <strong>Deja este campo en blanco si no deseas modificar la imagen</strong>
                </span>
                @endif
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="title">Quarter</label>
                <select name="quarter" id="quarter" class="form-control">
                    <option value="1Q"  @if (old('quarter') == '1Q' || $entry->quarter == '1Q') selected @endif>1Q</option>
                    <option value="2Q" @if (old('quarter') == '2Q' || $entry->quarter == '2Q') selected @endif>2Q</option>
                    <option value="3Q" @if (old('quarter') == '3Q' || $entry->quarter == '3Q') selected @endif>3Q</option>
                    <option value="4Q" @if (old('quarter') == '4Q' || $entry->quarter == '4Q') selected @endif>4Q</option>
                </select>
                @error('quarter')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
        <button type="submit" class="btn btn-success col-4 offset-4">{{ $entry->exists ? 'Actualizar' : 'Agregar'}}</button>
        </form>
    </div>
@endif
@stop