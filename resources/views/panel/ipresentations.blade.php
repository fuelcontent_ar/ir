@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="https://unpkg.com/external-svg-loader@1.0.0/svg-loader.min.js" async></script>
<h2>Institutional Presentation</h2>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endisset

@if(isset($entry))
<a href="{{ action('InstitutionalPresentationController@index') }}" class="back-link">Volver atrás</a>
    <div class="col-md-8 offset-md-2 text-center">
        <h3>Actualizar registro</h3>
        <form class="px-2 w-100 form-strong-labels" method="POST" action="{{ action('InstitutionalPresentationController@update', $entry->id) }}" enctype="multipart/form-data">
            @csrf
            @if ($entry->exists)
                @method('PATCH')
            @endif
            @if (session('error'))
            <div class="alert-danger error-box">
                    <span>{{ session('error') }}</span>
            </div>
            @endif
            <div class="form-row text-left">
                <div class="form-group">
                <label for="text">Archivo</label>
                <input type="file" name="input" class="form-control @error('input') is-invalid @enderror" name="input" />
                @error('input')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                @if ($entry->exists)
                <span role="alert">
                    <span><b>Archivo actual:</b> <a href="{{ asset('storage/files/ipresentation/' . $entry->file) }}" target="_blank">{{ $entry->file }}</a></span><br />
                </span>
                @endif
                </div>
            </div>
            <button type="submit" class="btn btn-success">Actualizar</button>
        </form>
    </div>
@else
    <div class="table-responsive">
        <table class="table table-striped table-sm table-abm">
        <thead>
            <tr>
            <th>Archivo</th>
            <th>Editar</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($entries as $entry)
            <tr>
            <td><a href="{{ asset('storage/files/ipresentation/' . $entry->file) }}" target="_blank">{{ $entry->file }}</a></td>
            <td>
                <form method="GET" action="{{ action('InstitutionalPresentationController@edit', $entry->id) }}">
                <a href="#" onclick="this.closest('form').submit();return false;">
                    <svg class="icon-hover" data-src="{{ asset('/img/icons/pencil-square.svg') }}" type="image/svg+xml"></svg>
                </a>
                </form>
            </td>
            </tr>
        @endforeach
        </tbody>
        </table>
    </div>
@endif
@stop