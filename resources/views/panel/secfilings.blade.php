@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="https://unpkg.com/external-svg-loader@1.0.0/svg-loader.min.js" async></script>
<h2>Sec Filings</h2>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endisset

@if(isset($entry))
<a href="{{ action('SecFilingsController@index') }}" class="back-link">Ir a la lista de Sec Filings</a>
    <div class="col-md-8 offset-md-2 text-center">
        <h3>{{ $entry->exists ? 'Actualizar' : 'Nuevo' }} registro</h3>
        <form class="px-2 w-100 form-strong-labels" method="POST" action="{{ $entry->exists ? action('SecFilingsController@update', $entry->id) : action('SecFilingsController@store') }}" enctype="multipart/form-data">
            @csrf
            @if ($entry->exists)
                @method('PATCH')
            @endif
            @if (session('error'))
                <div class="alert-danger error-box">
                        <span>{{ session('error') }}</span>
                </div>
            @endif
            <div class="form-row text-left">
                <div class="form-group">
                <label for="title">Fecha</label>
                <input type="text" class="form-control @error('date') is-invalid @enderror" id="date" value="{{ old('date', $entry->date) }}" name="date" placeholder="MM-DD-AAAA">
                @error('date')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="title">Tipo</label>
                <input type="text" class="form-control @error('type') is-invalid @enderror" id="type" value="{{ old('type', $entry->type) }}" name="type" placeholder="Tipo">
                @error('type')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="title">Descripción</label>
                <textarea cols="40" rows="8" class="form-control @error('description') is-invalid @enderror" id="description" name="description" placeholder="Descripción">{{ old('description', $entry->description) }}</textarea>
                @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group w-100">
                <label for="title">Link</label>
                <input type="text" class="form-control @error('link') is-invalid @enderror" id="link" value="{{ old('link', $entry->link) }}" name="link" placeholder="link">
                @error('link')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-success">{{ $entry->exists ? 'Actualizar' : 'Agregar'}}</button>
        </form>
    </div>
@else
    <div class="col-md-3 offset-md-9 text-right px-0">
        <a href="{{ action('SecFilingsController@create') }}" type="submit" class="btn btn-success">+ Nuevo</a>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-sm table-abm">
        <thead>
            <tr>
            <th>Fecha</th>
            <th>Tipo</th>
            <th>Descripción</th>
            <th>Link</th>
            <th>Editar</th>
            <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($entries as $entry)
            <tr>
            <td>{{ $entry->date }}</td>
            <td>{{ $entry->type }}</td>
            <td>{{ $entry->description }}</td>
            <td><a href="{{ $entry->link }}" target="_blank">{{ $entry->link }}</a></td>
            <td>
                <form method="GET" action="{{ action('SecFilingsController@edit', $entry->id) }}">
                <a href="#" onclick="this.closest('form').submit();return false;">
                    <svg class="icon-hover" data-src="{{ asset('/img/icons/pencil-square.svg') }}" type="image/svg+xml"></svg>
                </a>
                </form>
            </td>
            <td>
                <form method="POST" action="{{ action('SecFilingsController@destroy', $entry->id) }}">
                    @csrf
                    {{ method_field('DELETE') }}
                    <a href="#" onclick="this.closest('form').submit();return false;">
                        <svg class="icon-hover" data-src="{{ asset('/img/icons/trash.svg') }}"></svg>
                    </a>
                </form>
            </td>
            </tr>
        @endforeach
        </tbody>
        </table>
    </div>
@endif
@stop