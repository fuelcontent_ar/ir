@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="https://unpkg.com/external-svg-loader@1.0.0/svg-loader.min.js" async></script>
<h2>Financials</h2>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endisset

@if(isset($entry))
<a href="{{ action('FinancialFilesController@index') }}" class="back-link">Ir a la lista de Financials</a>
    <div class="col-md-8 offset-md-2">
        <h3>{{ $entry->exists ? 'Actualizar' : 'Nuevo'}} archivo</h3>
        <form class="px-2 w-100 form-strong-labels" method="POST" action="{{ $entry->exists ? action('FinancialFilesController@update', $entry->id) : action('FinancialFilesController@store') }}" enctype="multipart/form-data">
            @csrf
            @if ($entry->exists)
                @method('PATCH')
            @endif
            @if (session('error'))
            <div class="alert-danger error-box">
                    <span>{{ session('error') }}</span>
            </div>
        @endif
            <div class="form-row text-left">
                <div class="form-group">
                <label for="type">Título</label>
                <input type="text" class="form-control @error('type') is-invalid @enderror" id="type" value="{{ old('type', $entry->type) }}" name="type" placeholder="Tipo">
                @error('type')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-row text-left">
                <div class="form-group">
                <label for="text">Archivo </label>
                <input type="file" name="input" class="form-control @error('input') is-invalid @enderror" />
                @error('input')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                
                @if ($entry->exists)
                <span role="alert">
                    <span><b>Archivo actual:</b> <a href="{{ asset('storage/files/financial_files/' . $entry->file) }}" target="_blank">{{ $entry->file }}</a></span><br />
                    <strong>Deja este campo en blanco si no deseas modificar el archivo</strong>
                </span>
                @endif
                </div>
            </div>
            <button type="submit" class="btn btn-success col-4 offset-4">{{ $entry->exists ? 'Actualizar' : 'Agregar'}}</button>
        </form>
    </div>
@else
    <div class="table-responsive">
        <h4>Financial Files</h3>
        <table class="table table-striped table-sm table-abm">
        <thead>
            <tr>
            <th>Tipo</th>
            <th>Archivo</th>
            <th>Editar</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($entries as $entry)
            <tr>
            <td>{{ $entry->type }}</td>
            <td>{{ $entry->file }}</td>
            <td>
                <form method="GET" action="{{ action('FinancialFilesController@edit', $entry->id) }}">
                <a href="#" onclick="this.closest('form').submit();return false;">
                    <svg class="icon-hover" data-src="{{ asset('/img/icons/pencil-square.svg') }}" type="image/svg+xml"></svg>
                </a>
                </form>
            </td>
            </tr>
        @endforeach
        </tbody>
        </table>
    </div>
    <br />
    <h4>Financial Quarters</h3>
    <div class="col-md-3 offset-md-9 text-right px-0">
            <a href="{{ action('FinancialQuartersController@create') }}" type="submit" class="btn btn-success">+ Nuevo archivo</a>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-sm table-abm">
        <thead>
            <tr>
            <th>Título</th>
            <th>Archivo</th>
            <th>Año</th>
            <th>Quarter</th>
            <th>Editar</th>
            <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($quarters as $entry)
            <tr>
            <td>{{ $entry->title }}</td>
            <td><a href="{{ asset('storage/files/financial_quarters/' . $entry->file) }}" target="_blank">{{ $entry->file }}</a></td>
            <td>{{ $entry->year }}</td>
            <td>{{ $entry->quarter }}</td>
            <td>
                <form method="GET" action="{{ action('FinancialQuartersController@edit', $entry->id) }}">
                <a href="#" onclick="this.closest('form').submit();return false;">
                    <svg class="icon-hover" data-src="{{ asset('/img/icons/pencil-square.svg') }}" type="image/svg+xml"></svg>
                </a>
                </form>
            </td>
            <td>
                <form method="POST" action="{{ action('FinancialQuartersController@destroy', $entry->id) }}">
                    @csrf
                    {{ method_field('DELETE') }}
                    <a href="#" onclick="this.closest('form').submit();return false;">
                        <svg class="icon-hover" data-src="{{ asset('/img/icons/trash.svg') }}"></svg>
                    </a>
                </form>
            </td>
            </tr>
        @endforeach
        </tbody>
        </table>
    </div>
@endif
@stop