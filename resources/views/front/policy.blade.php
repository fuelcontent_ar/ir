@extends('layouts.app')
@section('title', 'Adecoagro IR - Home')

@section('content')
<div class="img-directors" style="background:url({{ asset('img/legal/business-divisions.jpg') }}) no-repeat center/cover">
<div class="container">
    <h2>Privacy Policy</h2>
</div>
</div>
<section class="legal my-5 container">
    <h4>Introduction</h4>
    <p>ADECOAGRO (“We”) respect your privacy and are committed to protecting it through our compliance with this policy.</p>
    <p>This policy (together with our terms of use and any other documents referred to in it) describes the personal information that we collect from you, or that you provide on or through adecoagro.com (our “Website”) and how that information may be used or disclosed by us.</p>
    <p>This policy does not apply to information we collect through any other means, or to information collected on any other Company, affiliate or third-party website that may link to or be accessible from the Website.</p>
    <p>Please read the following carefully to understand our policies and practices regarding your personal information and how we will treat it. By using or accessing our Website, you agree to this privacy policy. This policy may change from time to time and your continued use of the Website is deemed to be acceptance of such changes, so please check the policy periodically for updates.</p>

    <h4>Children Under the Age of 13</h4>
    <p>Our Website is not intended for children under 13 years of age. No one under age 13 may provide any personal information to or on the Website. We do not knowingly collect personal information from children under 13. If you are under 13, please do not send any information about yourself to us, including your name, address, telephone number or e-mail address. In the event that we learn that we have collected personal information from a child under age 13 without verification of parental consent, we will delete that information. If you believe that we might have any information from or about a child under 13, please contact us: Telephone +54 11 4836-8651 or E-mail: info@adecoagro.com.</p>

    <h4>Information We Collect About You</h4>
    <p>We collect several types of information from and about users of our Website, including:</p>
    <p><i class="fas fa-caret-right"></i>Information you provide to us.</p>
    <p><i class="fas fa-caret-right"></i>Information we collect as you navigate through the site, including usage details, IP addresses [, web beacons] cookies.</p>
    <p><i class="fas fa-caret-right"></i>Information we receive from third parties, for example, our business partners.</p>
    <p>Information You Provide to Us</p>
    <p>We may collect and use the following information that you provide to us:</p>
    <p><i class="fas fa-caret-right"></i>Information that you provide by filling in forms on our Website.</p>
    <p><i class="fas fa-caret-right"></i>Records and copies of your correspondence (including e-mail addresses), if you contact us.</p>
    <h4>Usage Details, IP Addresses[,Web beacons] and Cookies</h4>
    <p><i class="fas fa-caret-right"></i>Usage Details and IP Addresses. As you navigate through the Website, we may also collect details of your visits to our Website, including, but not limited to, traffic data, location data, and other communication data and the resources that you access, as well as information about your computer and internet connection, including your IP address, operating system and browser type, for system administration and to report aggregate information to our advertisers. This is statistical data about our users‘ equipment, browsing actions and patterns, and does not identify any individual.</p>
    <p><i class="fas fa-caret-right"></i>Cookies. For the same reason, we may obtain information about your general internet usage by using cookies. A cookie is a small file stored on the hard drive of your computer. Use of cookies help us to improve our Website and to deliver a better and more personalized service by enabling us to:</p>
    <p>Estimate our audience size and usage patterns.</p>
    <p>Speed up your searches.</p>
    <p>Recognize you when you return to our Website.</p>
    <p>You may refuse to accept cookies by activating the appropriate setting on your browser. However, if you select this setting you may be unable to access certain parts of our Website. Unless you have adjusted your browser setting so that it will refuse cookies, our system will issue cookies when you direct your browser to our Website.</p>
    <p>We do not collect personally identifiable information automatically, but we may tie this information to your previously submitted personally identifiable information.</p>
    <h4>UUses Made of the Information</h4>
    <p>We use information that we collect about you or that you provide to us, including any personal information, in the following ways:</p>
    <p><i class="fas fa-caret-right"></i>To present our Website and its contents in a suitable and effective manner for you and for your computer.</p>
    <p><i class="fas fa-caret-right"></i>To provide you with information, products or services that you request from us.</p>
    <p><i class="fas fa-caret-right"></i>To carry out our obligations and enforce our rights arising from any contracts entered into between you and us.</p>
    <p><i class="fas fa-caret-right"></i>To notify you about changes to our service.</p>
    <h4>Disclosure of Your Information</h4>
    <p>We use information that we collect about you or that you provide to us, including any personal information, in the following ways:</p>
    <p>We may disclose personal information that you provide via this Website to the following third parties:</p>
    <p><i class="fas fa-caret-right"></i>Our subsidiaries and affiliates.</p>
    <p><i class="fas fa-caret-right"></i>Contractors and service providers we use to support our business.</p>
    <p><i class="fas fa-caret-right"></i>In the event of merger, acquisition, or any form of sale of some or all of ADECOAGRO's assets, in which case personal information held by ADECOAGRO about its customers will be among the assets transferred to the buyer.</p>
    <p><i class="fas fa-caret-right"></i>We may also disclose your personal information to third parties to:</p>
    <p><i class="fas fa-caret-right"></i>Comply with any court order or other legal obligation.</p>
    <p><i class="fas fa-caret-right"></i>Enforce or apply our terms of use and other agreements.</p>
    <p><i class="fas fa-caret-right"></i>Choices About How We Use and Disclose Your Information</p>
    <h4>Disclosure of Your Information</h4>
    <p>We strive to provide you with choices regarding the personal information you provide to us. We have created mechanisms to provide you with the following control over your information:</p>
    <p><i class="fas fa-caret-right"></i>Cookies. If you do not wish us to collect cookies, you may set your browser to refuse cookies, or to alert you when cookies are being sent. If you do so, please note that some parts of this site may then be inaccessible or not function properly.</p>
    <p>You should review this privacy policy carefully, because if you do not agree with our practices, your ultimate choice is not to use the Website. Remember, by using any part of the Website, you accept and agree to our privacy practices. If we update this privacy policy, your continued use of the Website (following the posting of the revised privacy policy) means that you accept and agree to the terms of the revised privacy policy.</p>
    <h4>Data Security</h4>
    <p>We have implemented measures designed to secure your personal information from accidental loss and from unauthorized access, use, alteration and disclosure.</p>
    <p>Unfortunately, the transmission of information via the internet is not completely secure. Although we do our best to protect your personal information, we cannot guarantee the security of your personal information transmitted to our Website. Any transmission of personal information is at your own risk.</p>
    <h4>Changes to Our Privacy Policy</h4>
    <p>It is our policy to post any changes we make to our privacy policy on this page. The date the privacy policy was last revised is identified at the bottom of the page.</p>
    <h4>Contact Information</h4>
    <p>To ask questions or comment about this privacy policy and our privacy practices, contact us at: info@adecoagro.com</p>
</section>
@endsection