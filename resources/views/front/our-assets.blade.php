@extends('layouts.app')
@section('title', 'Adecoagro IR - Home')

@section('content')
<section class="container my-3">
<div class="mapa">
<div class=" container">
	<div class="col-12 paises py-3">
		<h4 style="color: #fff; font-weight: bold;">Our Assets</h4>
			<ul>
				<li style="color: #fff"><a style="color: #fff;" href="#" id="btn_arg">Argentina</a></li>
				<li style="color: #fff"><a style="color: #fff;" href="#" id="btn_uru">Uruguay</a></li>
				<li style="color: #fff"><a style="color: #fff;" href="#" id="btn_bra">Brasil</a></li>
			</ul>
	</div>
</div>
<div class="">
	<div class="col-12" id="div_mapa">
		<iframe src="https://www.google.com/maps/d/u/0/embed?mid=1w0_XTOKTjfgFKCpCwtg88ka6-lRFvH8b" style="width: 100%; height: 60vh;"></iframe>
	</div>
</div>
</div>
<div class="bajada" id="bajada">
	<div class="assets-data" id="assets-data"></div>
	<div class="img-logo">
	    <div>
		    <img src="{{ asset('img/icons/marker_brown.png') }}" alt="marker">
			<span>S&E</span>
		</div>
		<div>
		    <img src="{{ asset('img/icons/marker_yellow.png') }}" alt="marker">
			<span>Crops</span>
		</div>
		<div>
			<img src="{{ asset('img/icons/marker_blue.png') }}" alt="marker">
			<span>Dairy</span>
		</div>
		<div>
		    <img src="{{ asset('img/icons/marker_green.png') }}" alt="marker">
			<span>Rice</span>
		</div>
		<div>
		    <img src="{{ asset('img/icons/marker_gray.png') }}" alt="marker">
			<span>Industry</span>
		</div>
	</div>
</div>
</section>
<script type="text/javascript">
	setTimeout(() => {
		$(document).ready(function(){
		$('#btn_arg').click(function(){
			$('#div_mapa').html('<iframe src="https://www.google.com/maps/d/u/0/embed?mid=1qE5UiXLotP-x2aymHbFyji_93edaiaoV" style="width: 100%; height: 60vh;"></iframe>');
		});

		$('#btn_uru').click(function(){
			$('#div_mapa').html('<iframe src="https://www.google.com/maps/d/u/0/embed?mid=1_Uh8GDcQ-rS9RhtGo1Va0qOuT1x3VVoY" style="width: 100%; height: 60vh;"></iframe>');
		});

		$('#btn_bra').click(function(){
			$('#div_mapa').html('<iframe src="https://www.google.com/maps/d/u/0/embed?mid=1bX5odap49js2Ub1H-l_asSRWbKK3MhRG" style="width: 100%; height: 60vh;"></iframe>');
		});
	});
	}, 3000);


</script>
@endsection
