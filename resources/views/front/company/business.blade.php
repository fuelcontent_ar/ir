@extends('layouts.app')
@section('title', 'Adecoagro IR - Strategy')

@section('content')
<div class="img-directors" style="background:url({{ asset('img/company/business-divisions.jpg') }}) no-repeat center/cover">
<div class="container">
    <h2>Business Divisions</h2>
</div>
</div>
<section class="business container my-5">
    <div>
        <a href="https://www.adecoagro.com/en" target="_blank">
           <img src="{{ asset('img/company/f1.png') }}" alt="">
           <div class="sugar">
               <img src="{{ asset('img/company/azucar.png') }}" alt="">
            <span>
                Sugar, Ethanol and Energy
            </span>
        </div>
        </a>
    </div>
    <div>
        <a href="https://www.adecoagro.com/en">
            <img  src="{{ asset('img/company/f3.png') }}" alt="">
            <div class="grains">
                <img  src="{{ asset('img/company/granos.png') }}" alt="">
            <span>
                Grains
            </span>
        </div>
        </a>
    </div>
    <div>
        <a href="https://www.adecoagro.com/en">
            <img src="{{ asset('img/company/f2.png') }}" alt="">
            <div class="milk">
              <img src="{{asset('img/company/leche.png')}}" alt="milk">
              <span>Milk</span>
            </div>
        </a>
    </div>
    <div>
        <a href="https://www.adecoagro.com/en">    
            <img src="{{ asset('img/company/f4.png') }}" alt="">
            <div class="rice">
            <img src="{{ asset('img/company/arroz.png') }}" alt="">
            <span>Rice</span>
            </div>
        </a>
    </div>
    <div>
        <a href="https://www.adecoagro.com/en">
            <img src="{{ asset('img/company/f5.png') }}" alt="">
            <div class="land">
                <img src="{{ asset('img/company/tierra.png') }}" alt="">
                <span>Land transformation</span>
            </div>
        </a>
    </div>
</section>
@endsection
