@extends('layouts.app')
@section('title', 'Adecoagro IR - Strategy')

@section('content')
<div class="img-directors" style="background:url({{ asset('img/company/organizational-structure.jpg') }}) no-repeat center/cover">
<div class="container">
    <h2>Organizational Structure</h2>
</div>
</div>
<section class="organizational container my-5">
<img  src="{{ asset('img/company/org-structure.png') }}" alt="organizational">
</section>
@endsection