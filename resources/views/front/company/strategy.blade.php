@extends('layouts.app')
@section('title', 'Adecoagro IR - Strategy')

@section('content')
<div class="img-directors" style="background:url({{ asset('img/company/strategy.jpg') }}) no-repeat center/cover">
<div class="container">
    <h2>Strategy</h2>
</div>
</div>
<section class="container strategy">
<p>We intend to maintain our position as a leading agricultural company in South America by expanding and consolidating each of our business lines, creating value for the Company´s shareholders.</p>
<p>The key elements of Adecoagro´s business strategy are:</p>
<div class="list">
    <p>
    <i class="fas fa-circle"></i>
    Expand our farming business through organic growth, leasing and strategic acquisitions.
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Consolidate our sugar and ethanol cluster in the state of Mato Grosso do Sul, Brazil.
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Further increase our operating efficiencies while maintaining a diversified portfolio.
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Continue to implement our land transformation strategy.
    </p>
</div>
</section>

@endsection