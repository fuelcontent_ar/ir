@extends('layouts.app')
@section('title', 'Adecoagro IR - Strategy')

@section('content')
<div class="img-directors" style="background:url({{ asset('img/company/investor-education-banner.jpg') }}) no-repeat center/cover">
<div class="container">
    <h2>Investor Day</h2>
</div>
</div>
<section>
    <div class="education container" id="day"></div>
    <p id="content"></p>
</section>
@endsection
