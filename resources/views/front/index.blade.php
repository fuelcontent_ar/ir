@extends('layouts.app')
@section('title', 'Adecoagro IR - Home')

@section('content')

<header  class="header">
    <video class="video" src="{{asset('/videos/adecoagro2.mp4')}}"  autoplay></video>
</header>

<!-------------------- investor & quarter------------------------- -->
<div class="investor container my-5" id="investor">
    <div>
        <div class="quarter">
            <div class="title">
                <img src="{{ asset('img/home/icon-paper_sheet.png') }}" alt="">
                <h2>
                Quarter release pack
                </h2>
            </div>
            <div id="quarter" class="files mt-5">

            </div>
        </div>
    </div>
    <div>
        <h2>Investor <br> Tools & Resource</h2>
        <div class="unit">
        <a href="" class="block" style="background-color:#005838;display:none">
        <div>
            <img src="{{ asset('img/home/icon-home-calc.png') }}" alt="calc">
            <span>Unit Converter</span>
        </div>
        </a>
        <a href="/company/education" class="block" style="background-color:#2d9067;width:91.5%">
        <div>
            <img src="{{ asset('img/home/icon-home-glasses.png') }}" alt="glasses">
            <span>Investor Education</span>
        </div>
        </a>
        </div>
        <div class="unit" >
        <a href="/contacto" class="block" style="background-color:#8ec549">
        <div>
        <img src="{{ asset('img/home/icon-home-contact.png') }}" alt="contact">
            <span>Contact IR</span>
        </div>
        </a>
        <a id="unit" class="block" style="background-color:#5e8a2d" download>
        <div>
            <img src="{{ asset('img/home/icon-home-factsheets.png') }}" alt="factsheets">
            <span>Factsheets</span>
        </div>
        </a>

        </div>
    </div>
</div>
{{-- Why to invest in Adecoagro --}}
<div class="adeco" style="margin-top:20px;">
    <h2 class="py-3">Why to invest in Adecoagro?</h2>
    <div class="invest container" >
        <div class="blog">
        <span class="title" ><span>1.</span><br> Low cost producer</span>
        <p class="hidden">We produce in specific regions where the combination of agro-ecological conditions, together with our expertise, would allow us to become the lowest cost producer.</p>
        <img src="{{ asset('img/home/icon-home-why01.png') }}"  alt="">
        </div>
        <div class="blog">
        <span class="title"><span>2.</span><br> Diversified operations across the region</span>
        <p class="hidden">Our assets spread across the most productive regions of Argentina, Brazil and Uruguay. We produce a wide range of agricultural products. Geographics and product diversification protect us from weather risk and commodity price volatility.</p>
        <img src="{{ asset('img/home/icon-home-why02.png') }}"  alt="mapa">
        </div>
        <div class="blog">
        <span class="title"><span>3.</span> <br> Best environmental, social and governance (ESG) practices</span>
        <p class="hidden"> We have three main pillars that interact among each other generating a virtuous cycle: a) economic sustainability, b) environmental sustainability; and c) social sustainability.</p>
        <img src="{{ asset('img/home/icon-home-why03.png') }}" alt="">
        </div>
        <div class="blog">
        <span class="title"><span>4.</span> <br> Commitment to shareholder value</span>
        <p class="hidden">
        A disciplined capital allocation strategy to generate the highest returns to our shareholders.
        </p>
        <img src="{{ asset('img/home/icon-home-why04.png') }}" alt="">
        </div>
        <div class="blog">
        <span class="title"><span>5.</span> <br> Positive free cash flow generation</span>
        <p class="hidden">Positive free cash flow generation since 2015, despite commodity cycle volatility.</p>
        <img src="{{ asset('img/home/icon-home-why05.png') }}" alt="">
        </div>
    </div>
    <div class="agro">
        <img src="{{ asset('img/home/icon-nyse_listed.png') }}" alt="">
        <div class="compromiso">
        <p>On top of our compromise,</p>
        <p> we are a NYSE listed company with clear trade </p>
        <p> background.</p>
        </div>
    </div>
</div>
{{-- productos  --}}
<section class="productos" id="productos">

</section>
{{-- graficos --}}
<section class="business container" id="business">

</section>
{{-- numeros  --}}
<section class="number">
    <div class="container content" id="num">
    </div>
</section>
 {{-- eventos --}}
<section class="eventos my-5 container">
    <div class="newsroom">
        <h2>Newsroom</h2>
        <div id="newsroom">
            <a href="">
                <span></span>
                <p></p>
            </a>
        </div>
    </div>
    <div class="feed">
	   <h3 class="h3">
        <i class="fab fa-twitter" style="color:#1d9bf0"></i><!-- <span class="bd-font">@AdecoagroIR</span>--></h3>
		<a href="https://twitter.com/adecoagroir" class="twitter-follow-button" data-show-count="false" data-lang="es" data-size="large">Seguir a @adecoagroir</a>
			<script>
				!function(d,s,id){
							var js,fjs=d.getElementsByTagName(s)[0];
							if(!d.getElementById(id)){
								js=d.createElement(s);
								js.id=id;js.src="//platform.twitter.com/widgets.js";
								fjs.parentNode.insertBefore(js,fjs);
							}
						}(document,"script","twitter-wjs");
            </script>
			<a class="twitter-timeline" href="https://twitter.com/AdecoagroIR?ref_src=twsrc%5Etfw">Tweets by AdecoagroIR</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
	</div>
    <div class="calendar">
        <h3>Investor Calendar</h3>
        <a href="/calendar">
            <i class="far fa-calendar-alt"></i>
            See full calendar
        </a>
    </div>
</section>

{{-- Subscribite  --}}
<section class="contact" style="background-image:url({{ asset('img/home/bg-newsletter.png') }})">
<div><p>Subscribe to our newsletter and get all our financial news and information in your inbox.</p></div>
<form action="https://adecoagro.us19.list-manage.com/subscribe/post?u=16db9ae7340cdee1208e21ab7&amp;id=3336aa5a56" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank">
    <input type="email" name="EMAIL" placeholder="Email Address" required>
    <input type="text" name="MMERGE6" placeholder="Full Name" required>
    <input type="text"  name="MMERGE7" placeholder="Company" required>
    <button class="btn"  type="submit">Subscribe</button>
</form>
</section>


@endsection
