@extends('layouts.app')
@section('title', 'Adecoagro IR - Board of Directors')

@section('content')
<div class="img-directors" style="background:url({{ asset('img/governance/annual-general-meeetings.jpg') }}) no-repeat center/cover">
<div class="container">
    <h2>Anual General Meetings</h2>
</div>
</div>
<section class="container agenda my-5">
    <div class="info">
        <h3>2021 ADECOAGRO ANNUAL GENERAL MEETING OF SHAREHOLDERS (the “AGM”)</h3>
        <h3 class="mt-4">GENERAL INFORMATION</h3>
        <p>
            <i class="fas fa-circle"></i>
            Date: Wednesday, April 21, 2021</p>
        <p>
        <i class="fas fa-circle"></i>
        Date: Wednesday, April 21, 2021
        </p>
        <p>
        <i class="fas fa-circle"></i>
        Location: Vertigo Naos Building, 6 Rue Eugène Ruppert, L - 2453 Luxembourg
        </p>
    </div>
    <div class="metings mt-5">
        <h2>Annual General Meetings</h2>
        <div class="annual" id="annual"> 
        </div>
    </div>
    <div class="info">
        <h3>PROCEDURES FOR ATTENDING THE MEETING AND VOTING BY PROXY</h3>
        <p>
            <i class="fas fa-circle"></i>
            On March 19, 2021 we mailed to our shareholders of record the Proxy Card and Convening Notice.</p>
        <p>
        <i class="fas fa-circle"></i>
        The Record Date of the Meetings is on March 5, 2021. 
        </p>
        <p>
        <i class="fas fa-circle"></i>
        In accordance with the Luxembourg law of 23 September 2020 on measures relating to the holding of meetings in companies and other legal entities, as amended, in order to vote, holders of Shares will need to complete proxy cards. Proxy cards must be received by the tabulation agent at the return address indicated on the proxy cards, Computershare Shareowner Services LLC, P.O. Box 43101, Providence, RI 02940, no later than 3:00 p.m. New York City Time on April 20, 2021 in order for such votes to count. 
        </p>
    </div>
</section>
@endsection