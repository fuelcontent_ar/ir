@extends('layouts.app')
@section('title', 'Adecoagro IR - Board of Directors')

@section('content')
<div class="img-directors" style="background:url({{ asset('img/governance/board-of-directors.jpg') }}) no-repeat center/cover">
<div class="container">
    <h2>Board of Directors</h2>
</div>
</div>
<section class="container directors my-4">
    <div class="">
        <p>The directors are appointed by the general meeting of shareholders for a period of up to three years; provided, however, the directors shall be elected on a staggered basis, with one-third of the directors being elected each year and provided further that such three year term may be exceeded by a period up to the annual general meeting held following the third anniversary of the appointment. </p>
        <p class="mt-5">Directors may be removed with or without cause (ad nutum) by the general meeting of shareholders by a simple majority of votes cast at a general meeting of shareholders. The directors are eligible for re-election indefinitely.</p>
        <p class="mt-5">The board of directors is empowered to manage Adecoagro S.A. and carry out our operations. They are vested with the broadest powers to manage the business of the Company and to authorize and/or perform all acts of disposal, management and administration falling within the purposes of Adecoagro S.A. and all powers not expressly reserved by Luxembourg law or by the Company´s articles of incorporation to the general meeting of shareholders is within the competence of the board of directors.n</p>
        <p class="mt-5">The board of directors comprises the following five committees: (i) Audit Committee, (ii) Risk and Commercial Committee, (iii) Strategy Committee (all of which have a meeting frequency of at least four times a year and as often as deemed necessary or appropriate in its judgment), (iv) Compensation Committee (which meets at least once a year and as needed on the initiative of the Chief Executive Officer or at the request of one of its members), and (v) ESG Committee (which meets at least eight times a year and as often as deemed necessary or appropriate in its judgment). For more information on Adecoagro’s committees please see the company’s 20-F report.</p>
        <p class="mt-5">The following table sets forth information for our directors:</p>
    </div>
    <div class="">
        <table class="table table-striped my-4">
            <thead>
                <th>#</th>
                <th>Name</th>
                <th>Position</th>
                <th>Date of Appointment</th>
                <th>Year Term Expires</th>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Plinio Musetti</td>
                    <td>Chairman</td>
                    <td>2017</td>
                    <td>2020</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Mariano Bosch</td>
                    <td>Director/CEO</td>
                    <td>2017</td>
                    <td>2020</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Daniel González</td>
                    <td>Director</td>
                    <td>2017</td>
                    <td>2020</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Guillaume van der Linden</td>
                    <td>Director</td>
                    <td>2018</td>
                    <td>2021</td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>Mark Schachter</td>
                    <td>Director</td>
                    <td>2018</td>
                    <td>2021</td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>Ivo Andrés Sarjanovic</td>
                    <td>Director</td>
                    <td>2018</td>
                    <td>2021</td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>Alan Leland Boyce</td>
                    <td>Director</td>
                    <td>2019</td>
                    <td>2022</td>
                </tr>
                <tr>
                    <td>8</td>
                    <td>Andrés Velasco Brañes</td>
                    <td>Director</td>
                    <td>2019</td>
                    <td>2022</td>
                </tr>
                <tr>
                    <td>9</td>
                    <td>Alejandra Smith</td>
                    <td>Director</td>
                    <td>2019</td>
                    <td>2022</td>
                </tr>
            </tbody>
        </table>
        <p>A description of the main tasks currently performed by each director as well as a description of each director’s employment history and education follows:</p>
    </div>
    <div class="mt-5">
        <h3 class="mb-4">Plínio Musetti.</h3>
        <p><i class="fas fa-circle"></i>Mr. Musetti has been a member of the Company’s board of directors since 2011 and an observer since 2010. Mr. Musetti is a Managing Partner of Janos Holding responsible for long term equity investments for Family offices in Brazil, following his role as Partner of Pragma Gestão de Patrimonio, since June 2010. From 2008 to 2009, Mr. Musetti served as the Chief Executive Officer of Satipel Industrial S.A., leading the company’s initial public offering process and aiding its expansion plan and merger with Duratex S.A. From 2002 to 2008, Mr. Musetti served as a partner at JP Morgan Partners and Chief Executive Officer of Vitopel S.A. (JP Morgan Partners’ portfolio company) where he led its private equity investments in Latin America. From 1992 to 2002, Mr. Musetti served as the Chief Executive Officer of Elevadores Atlas, during which time he led the company’s operational restructuring, initial public offering process and the sale to the Schindler Group. Prior to that, Mr. Musetti was the Chief Financial Officer of Indústrias Villares S.A., a Brazilian industrial company with operations throughout Latin America. Mr. Musetti has also served as a Director of Diagnósticos de America S.A. from 2002 to 2009. In addition, Mr. Musetti is currently serving as a Board member of Raia Drogasil S.A. Mr. Musetti graduated in Civil Engineering and Business Administration from Mackenzie University and attended the Program for Management Development at Harvard Business School in 1989. Mr. Musetti is a Brazilian citizen.</p>
    </div>
    <div class="mt-5">
        <h3 class="mb-4">Mariano Bosch.</h3>
        <p><i class="fas fa-circle"></i>Mr. Bosch is a co-founder of Adecoagro and has been the Chief Executive Officer and a member of the Company’s board of directors since inception (2002). From 1995 to 2002, Mr. Bosch served as the founder and Chief Executive Officer of BLS Agribusiness, an agricultural consulting, technical management and administration company. Mr. Bosch has over 22 years of experience in agribusiness development. He is involved in business organizations such as IDEA, YPO, AACREA, FPC and AAPRESID. He graduated with a degree in Agricultural Engineering from the University of Buenos Aires. In 2018, he received a Konex award.</p>
    </div>
    <div class="mt-5">
        <h3 class="mb-4">Alan Leland Boyce.</h3>
        <p><i class="fas fa-circle"></i>Mr. Boyce is a co-founder of Adecoagro and has been a member of the Company’s board of directors since 2002 and has been Chairman of the Risk and Commercial Committee since 2011. Mr. Boyce is co-founder and Chairman of Materra LLC, a California based owner and operator of 15,000 acres of farmland where it grows pistachios, dates, citrus and organic vegetables. Since 1985, Mr. Boyce has served as the Chief Financial Officer of Boyce Land Co. Inc., a farmland management company that runs 10 farmland limited partnerships in the United States. Mr. Boyce is co-founder and CEO of Westlands Solar Farms, the only farmer-owned utility scale solarPV developer in California and recently co-founded Prairie Harvest, which is developing hemp processing technologies to efficiently separate CBD in the field. Mr. Boyce formerly served as the director of special situations at Soros Fund Management from 1999 to 2007, where he managed an asset portfolio of the Quantum Fund and had principal operational responsibilities for the fund’s investments in South America. Mr. Boyce also served as managing director at the Community Reinvestment Act at Bankers Trust from 1986 to 1999 where he was in charge of fixed-income arbitrage proprietary trading, the bank’s mortgage portfolio and compliance. In addition, Mr Boyce was senior managing director for investment strategy at Countrywide Financial from 2007 to 2008, and worked at the U.S. Federal Reserve Board from 1982 to 1984. He graduated with a degree in Economics from Pomona College, and has a Masters in Business Administration from Stanford University. Mr. Boyce is an American citizen.</p>
    </div>
    <div class="mt-5">
        <h3 class="mb-4">Andres Velasco Brañes. </h3>
        <p><i class="fas fa-circle"></i>Mr. Velasco has been a member of the Company’s board of directors since 2011. Mr. Velasco was the Minister of Finance of Chile between March 2006 and March 2010, and was also the president of the Latin American and Caribbean Economic Association from 2005 to 2007. Prior to entering the government sector, Mr. Velasco was Sumitomo-FASID Professor of Development and International Finance at Harvard University’s John F. Kennedy School of Government, an appointment he had held since 2000. From 1993 to 2000, he was Assistant and then Associate Professor of Economics and the director of the Center for Latin American and Caribbean Studies at New York University. During 1988 to 1989, he was Assistant Professor at Columbia University. Currently Mr. Velasco serves as Adjunct Professor of Public Policy at Harvard University, and a Tinker Visiting Professor at Columbia University. He also performs consulting services on various economic matters rendering economic advice to an array of clients, including certain of our shareholders. Mr. Velasco has been appointed Dean of New School of Public Policy at London School of Economics. Mr. Velasco holds a Ph.D. in economics from Columbia University and was a postdoctoral fellow in political economy at Harvard University and the Massachusetts Institute of Technology. He received a B.A. in economics and philosophy and an M.A. in international relations from Yale University. Mr. Velasco is a Chilean citizen.</p>
    </div>
    <div class="mt-5">
        <h3 class="mb-4">Daniel C. Gonzalez.  </h3>
        <p><i class="fas fa-circle"></i>Mr. Gonzalez has been a member of the Company’s board of directions since April 16, 2014. Mr. Gonzalez holds a degree in Business Administration from the Argentine Catholic University. He served for 14 years in the investment bank Merrill Lynch & Co in Buenos Aires and New York, holding the positions of Head of Mergers and Acquisitions for Latin America and President for the Southern Cone (Argentina, Chile, Peru and Uruguay), among others. While at Merrill Lynch, Mr. Gonzalez played a leading role in several of the most important investment banking transactions in the region and was an active member of the firm’s global fairness opinion committee. He remained as a consultant to Bank of America Merrill Lynch after his departure from the bank. Previously, he was Head of Financial Planning and Investor Relations in Transportadora de Gas del Sur SA. Mr. Gonzalez is currently the Chief Executive Officer of YPF Sociedad Anónima, where he is also a member of its Board of Directors. Mr Gonzalez is also a member of the Board of Directors of Hidroeléctrica Piedra del Aguila S.A. Mr. González is an Argentine citizen.</p>
    </div>
    <div class="mt-5">
        <h3 class="mb-4">Guillaume van der Linden. </h3>
        <p><i class="fas fa-circle"></i>Mr. van der Linden has been a member of the Company’s board of directors since 2009. Since 2007, Mr. van der Linden has been Senior Investment Manager at PGGM Vermogensbeheer B.V., responsible for investments in emerging markets credit. From 1993 to 2007, Mr. van der Linden worked for ING Bank in various roles, including in risk management and derivatives trading. From 1988 to 1993, Mr. van der Linden was employed as a management consultant for KPMG and from 1985 to 1988 as a corporate finance analyst for Bank Mees & Hope. Mr. van der Linden graduated with Masters degrees in Economics from Erasmus University Rotterdam and Business Administration from the University of Rochester. Mr. van der Linden is a Dutch citizen.</p>
    </div>
    <div class="mt-5">
        <h3 class="mb-4">Mark Schachter.  </h3>
        <p><i class="fas fa-circle"></i>Mr. Schachter has been a member of the Company’s board of directors since 2009. Mr. Schachter has been a Managing Partner of Elm Park Capital Management since 2010. From 2004 to 2010, he was a Portfolio Manager with HBK Capital Management where he was responsible for the firm’s North American private credit activities. His responsibilities included corporate credit investments with a primary focus on middle-market lending and other special situation investment opportunities. From 2003 to 2004, Mr. Schachter worked for American Capital, a middle-market private equity and mezzanine firm and worked in the investment banking division of Credit Suisse Group from 2001 to 2003. Mr. Schachter received a degree in Business Administration from the Ivey Business School at the University of Western Ontario and completed the Program for Leadership Development at Harvard Business School. Mr. Schachter is a Canadian citizen and has permanent American residence</p>
    </div>
    <div class="mt-5">
        <h3 class="mb-4">Ivo Andrés Sarjanovic.  </h3>
        <p><i class="fas fa-circle"></i>Mr. Sarjanovic served for more than 25 years in Cargill International, starting as trader in the Grain and Oilseeds business. While in Cargill he held between years 2000-2011 the position of Vice-president and Global Trading Manager of Oilseeds in Geneva, coordinating worldwide trading and crushing activities, and between 2007-2011 he was also the Africa and Middle East General Manager of Agriculture. From 2011 to 2014 Mr. Sarjanovic held the position of Vice-president and World Manager of Cargill Sugar Operations, playing a leading role in the radical transformation of the organization that led to the strategic decision to spin-off in 2014 the sugar business of Cargill creating Alvean Sugar SL, a joint venture integrated with Copersucar, Brazil. Mr. Sarjanovic served as the Chief Executive Officer of Alvean until 2017, during which time he led the company to become the biggest sugar trader in the world. Mr. Sarjanovic is currently serving as non-executive Board member of Agflow S.A. and executive Board member of Sophicom, and also lectures at the University of Geneva’s Master in Commodities. Mr. Sarjanovic holds a B.A. in Economic Sciences, major in Accounting, from the National University of Rosario, Argentina. Additionally, he completed executive studies at IMD in Lausanne, at Oxford University and at Harvard Business School, and was a PhD candidate in Economics at New York University. Mr. Sarjanovic is an Argentine/Italian/Swiss citizen.</p>
    </div>
    <div class="mt-5">
        <h3 class="mb-4">Alejandra Smith.  </h3>
        <p><i class="fas fa-circle"></i>Ms. Smith is the founder of Edge Consultants, a management consulting firm, which helps its clients grow their businesses´ market share profitability, by leveraging strategic and operational expertise. Ms. Smith began her career in Procter & Gamble in 1983, where she acted as Director of the Health & Beauty Business Unit. She held posts in Mexico, Canada and throughout Latin America, where her team consistently drove company expansion into more profitable mass categories. In 1994 Ms. Smith joined PepsiCo, initially co-leading the beverages Go to Market technological transformation in the USA. She was later appointed to the team in charge of the M&A of bottlers in Mexico, the largest international division, where she also established a profitable bottled water category. Subsequently, Ms. Smith transferred to the company's Snacks & Food Division to work in the Latin America and Asia Pacific regions as Commercial SVP and Quaker CEO, . Ms. Smith worked for two multinational Fortune 100 Companies across the Americas and Asia Pacific. Ms. Smith concurrently serves as an independent member of the board of directors of BEPENSA (where she serves as president of the Compensation and Evaluation Committee), Zurich, City Express and Corona. Ms. Smith is fully bicultural and bilingual, holds degrees in Business Administration & Economics from the Instituto Tecnológico Autónomo de Mexico ( ITAM ). Ms. Smith is a Mexican citizen.</p>
    </div>
</section>
@endsection