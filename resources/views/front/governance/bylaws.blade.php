@extends('layouts.app')
@section('title', 'Adecoagro IR - Home')

@section('content')
<div class="img-directors" style="background:url({{ asset('img/governance/bylaws.jpg') }}) no-repeat center/cover">
<div class="container">
    <h2>ByLaws</h2>
</div>
</div>
<section class="container bylaws my-5">
<div class="part-one">
    <h2>PART I.   FORM, DENOMINATION, DURATION, REGISTERED OFFICE</h2>
    <div>
    <h3>Article 1. Form, Name</h3>
    <p> <i class="fas fa-caret-right"></i> There exists a company in the form of a société anonyme, under the name of Adecoagro S.A. (the "Company").</p>
    </div>
    <div>
    <h3>Article 2. Duration</h3>
    <p> <i class="fas fa-caret-right"></i>The Company is established for an undetermined duration. The Company may be dissolved at any time by a resolution of the Shareholders adopted in the manner required for amendment of these Articles of Incorporation.</p>
    </div>
    <div>
    <h3>Article 3. Registered office</h3>
    <p> <i class="fas fa-caret-right"></i>3.1 The Company has its registered office in the City of Luxembourg, Grand-Duchy of Luxembourg. It may be transferred to any other place in the Grand Duchy of Luxembourg by means of a resolution of a General Meeting deliberating in the manner provided for amendments to the Articles.</p>
    <p> <i class="fas fa-caret-right"></i>3.2 The address of the registered office may be transferred within the municipality by decision of the Board of Directors.</p>
    <p> <i class="fas fa-caret-right"></i>3.3 The Company may have offices and branches, both in Luxembourg and abroad.</p>
    <p> <i class="fas fa-caret-right"></i>3.4 In the event that the Board of Directors determines that extraordinary political, economic or social developments have occurred or are imminent that would interfere with the normal activities of the Company at its registered office, or with the ease of communication between such office and persons abroad, the registered office may be temporarily transferred abroad until the complete cessation of these abnormal circumstances; such temporary measures shall have no effect on the nationality of the Company which, notwithstanding the temporary transfer of its registered office, will remain a Luxembourg company. Such temporary measures will be taken and notified to any interested parties by the Board of Directors.</p>
    </div> 
</div>
<div class="part-two">
    <h2>PART II.  PURPOSE, OBJECT</h2>
    <div>
    <h3>Article 4. Purpose, Object</h3>
    <p> <i class="fas fa-caret-right"></i> 4.1 The object of the Company is the holding of participations, in any form whatsoever, in Luxembourg and foreign companies, or other entities or enterprises, the acquisition by purchase, subscription, or in any other manner as well as the transfer by sale, exchange or otherwise of stock, bonds, debentures, notes and other securities or rights of any kind including interests in partnerships, and the holding, acquisition, disposal, investment in any manner in, development, licensing or sub licensing, of any patents or other intellectual property rights of any nature or origin as well as the ownership, administration, development and management of its portfolio. The Company may carry out its business through branches in Luxembourg or abroad.</p>
    <p> <i class="fas fa-caret-right"></i> 4.2 The Company may borrow in any form and proceed to the issue by private or public of bonds, convertible bonds and debentures or any other securities or instruments it deems fit.</p>
    <p> <i class="fas fa-caret-right"></i>4.4 Finally, the Company can perform all commercial, technical and financial or other operations, connected directly or indirectly in all areas in order to facilitate the accomplishment of its purpose.</p>
    </div>
</div>
<div class="part-three">
    <h2>PART III. SHARE CAPITAL – SHARES</h2>
    <div>
    <h3>Article 5. Share capital</h3>
    <p> <i class="fas fa-caret-right"></i> 5.1.The Company has an issued share capital of one hundred and eighty-three million five hundred and seventy-two thousand seven hundred and twenty-two US Dollars and fifty cents (USD 183,572,722.50) represented by a total of one hundred and twenty-two million three hundred and eighty-one thousand eight hundred and fifteen (122,381,815) fully paid Shares, each with a nominal value of one US Dollar and fifty cents (USD1.5), with such rights and obligations as set forth in the present Articles.</p>
    <p> <i class="fas fa-caret-right"></i> 5.1.1 The Company has an authorized share capital of three billion US Dollars (3,000,000,000), including the issued share capital, represented by two billion (2,000,000,000) shares, each with a nominal value of one US Dollar and fifty cents (USD1.5). The Company’s share capital (and any authorization granted to the Board of Directors in relation thereto) shall be valid from 200th April, 2016 and until the fifth anniversary of publication in the Mémorial of the deed of the extraordinary General Shareholder’s Meeting held on 200th April 2016. The Board of Directors, or any delegate(s) duly appointed by the Board of Directors, may from time to time issue shares within the limits of the authorized share capital against contributions in cash, contributions in kind or by way of incorporation of available reserves at such times and on such terms and conditions, including the issue price, as the Board of Directors or its delegate(s) may in its or their discretion resolve and the General Shareholder’s Meeting waived and has authorized the Board of Directors to waive, suppress or limit, any pre-emptive subscription rights of shareholders provided for by law to the extent it deems such waiver, suppression or limitation advisable for any issue or issues of shares within the authorized share capital.</p>
    <p> <i class="fas fa-caret-right"></i>5.2 The Company may not issue fractional Shares. The Board of Directors shall be authorised at its discretion to provide for the payment of cash or the issuance of scrip in lieu of any fraction of a Share.</p>
    <p> <i class="fas fa-caret-right"></i>5.4 Any Share premium shall be freely distributable in accordance with the provision of these Articles.</p>
    </div>
   
    <div>
    <h3>Article 6. Securities in registered form only</h3>
    <p> <i class="fas fa-caret-right"></i> 6.1 Shares</p>
    <p> <i class="fas fa-caret-right"></i>6.1.1    Shares of the Company are in registered form only.</p>
    <p> <i class="fas fa-caret-right"></i>5.2 The Company may not issue fractional Shares. The Board of Directors shall be authorised at its discretion to provide for the payment of cash or the issuance of scrip in lieu of any fraction of a Share.</p>
    <p> <i class="fas fa-caret-right"></i>6.1.2    A register of Shares will be kept by the Company and will be available for inspection by any registered shareholder. Ownership of registered Shares will be established by inscription in the said register or in the event separate registrars have been appointed pursuant to Article 6.1.3, such separate register. Without prejudice to the conditions for transfer by book entry in the case provided for in Article 6.1.7 of the present Articles, a transfer of registered Shares shall be carried out by means of a declaration of transfer entered in the relevant register, dated and signed by the transferor and the transferee or by their duly authorised representatives. The Company may accept and enter in the relevant register a transfer on the basis of correspondence or other documents recording the agreement between the transferor and the transferee.</p>
    <p> <i class="fas fa-caret-right"></i>6.1.3    The Company may appoint registrars in different jurisdictions who will each maintain a separate register for the registered shares entered therein and the holders of shares may elect to be entered in one of the registers and to be transferred from time to time from one register to another regis­ter. The Board of Directors may however impose transfer restrictions for Shares that are registered, listed, quoted, dealt in, or have been placed in certain jurisdictions in compliance with the requirements applicable therein. The transfer to the register kept at the Company's registered office may always be requested.</p>
    <p> <i class="fas fa-caret-right"></i>6.1.4    Subject to the provisions of Article 6.1.7, the Company may consider the person in whose name the registered Shares are registered in the register(s) of Shareholders as the full owner of such registered Shares. The Company shall be completely free from any responsibility in dealing with such registered Shares towards third parties and shall be justified in considering any right, interest or claims of such third parties in or upon such registered shares to be non-existent, subject, however, to any right which such third party might have to demand the registration or change in registration of registered Shares. In the event that a holder of registered shares does not provide an address to which all notices or announcements from the Company may be sent, the Company may permit a notice to this effect to be entered into the regis­ter(s) of Shareholders and such holder's address will be deemed to be at the registered office of the Company or such other address as may be so entered by the Company from time to time, until a different address shall be provided to the Company by such holder. The holder may, at any time, change his address as entered in the register(s) of Share­holders by means of written notification to the Company or the relevant registrar.</p>
    <p> <i class="fas fa-caret-right"></i>6.1.5    The Board may decide that no entry shall be made in the register of Shareholders and no notice of a transfer shall be recognised by the Company or a registrar during the period starting on the fifth (5) business day before the date of a General Meeting and ending at the close of that General Meeting, unless the Board sets a shorter time limit.</p>
    <p> <i class="fas fa-caret-right"></i>6.1.6    All communications and notices to be given to a registered Shareholder shall be deemed validly made to the latest address communicated by the Shareholder to the Company.</p>
    <p> <i class="fas fa-caret-right"></i>6.1.7    Where Shares are recorded in the register of Shareholders on behalf of one or more persons in the name of a securities settlement system or the operator of such a system or in the name of a professional securities depositary or any other depositary (such systems, professionals or other depositaries being referred to hereinafter as "Depositaries") or of a sub-depositary designated by one or more Depositaries, the Company - subject to having received from the Depositary with whom those Shares are kept in  account a certificate in proper form - will permit those persons to exercise the rights attaching to those Shares, including admission to and voting at General Meetings.  The Board of Directors may determine the formal requirements with which such certificates must comply. Notwithstanding the foregoing, the Company will make dividend payments and any other payments in cash, Shares or other securities only to the Depositary or sub-depositary recorded in the register or in accordance with its instructions, and such payment will effect full discharge of the Company’s obligations.</p>
    <p> <i class="fas fa-caret-right"></i>6.1.9    The Shares are indivisible vis-à-vis the Company which will recognise only one holder per Share. In case a Share is held by more than one person, the persons claiming ownership of the Share will be required to name a single proxy to represent the Share vis-à-vis the Company. The Company has the right to suspend the exercise of all rights attached to such Share until one person has been so appointed. The same rule shall apply in the case of a conflict between an usufructuary and a bare owner or between a pledgor and a pledgee.</p>
    <p> <i class="fas fa-caret-right"></i>6.2 Other Securities</p>
    <p> <i class="fas fa-caret-right"></i>6.2.1    Securities (other than Shares which are covered by article 6.1) of the Company are in registered form only. </p>
    <p> <i class="fas fa-caret-right"></i>6.2.2    The provisions of article 6.1 shall apply mutatis mutandis. </p>
    </div>
    <div>
    <h3>Article 6. Securities in registered form only</h3>
    <p>
    <i class="fas fa-caret-right"></i>
    Subject as set forth in the present Articles, each Share shall be entitled to one vote at all General Meetings of Shareholders.
    </p>
    </div>
</div>
<div class="part-four">
    <h2>PART IV. MANAGEMENT OF THE COMPANY</h2>
    <div>
    <h3>Article 8. Management of the Company – Board of Directors</h3>
    <p> <i class="fas fa-caret-right"></i>  8.1 The Company shall be managed by a Board of Directors which is vested with the broadest powers to manage the business of the Company and to authorise and/or perform all acts of disposal, management and administration falling within the purposes of the Company.</p>
    <p> <i class="fas fa-caret-right"></i>8.2 All powers not expressly reserved by the law or by the Articles of the Company to the General Meeting shall be within the competence of the Board of Directors.</p>
    <p> <i class="fas fa-caret-right"></i>8.3 Except as otherwise provided herein or by law, the Board of Directors of the Company is authorised to take such action (by resolution or otherwise) and to adopt such provisions as shall be necessary, appropriate, convenient or deemed fit to implement the purpose of the Company.</p>
   
    <div>
    <h3>Article 9. Composition of the Board of Directors</h3>
    <p> <i class="fas fa-caret-right"></i> 9.1 The Company shall be managed by a Board of Directors composed of a minimum of three (3) Directors and a maximum of eleven (11) (unless otherwise provided for herein) who may but do not need to be Shareholders of the Company.</p>
    <p> <i class="fas fa-caret-right"></i>9.2 The Directors are appointed by the General Meeting of Shareholders for a period of up to three (3) years; provided however the Directors shall be elected on a staggered basis, with one third (1/3) of the Directors being elected each year and provided further that such three year term may be exceeded by a period up to the annual general meeting held following the third anniversary of the appointment. The Directors may be removed with or without cause (ad nutum) by the General Meeting of Shareholders by a simple majority vote of votes cast at a General Meeting of Shareholders. The Directors shall be eligible for re-election indefinitively.</p>
    <p> <i class="fas fa-caret-right"></i>9.3 In the event of a vacancy in the office of a Director because of death, retirement, resignation, dismissal, removal or otherwise, the remaining Directors may fill such vacancy and appoint a successor in accordance with applicable law.</p>
    </div>
    <div>
    <h3>Article 10. Chairman</h3>
    <p>
    <i class="fas fa-caret-right"></i>
    10.1 The Board of Directors shall, to the extent required by law and otherwise may, appoint the chairman of the Board of Directors amongst its members (the “Chairman”). The Chairman shall preside over all meetings of the Board of Directors and of Shareholders including class meetings. In the absence of the Chairman of the Board, a chairman determined ad hoc, shall chair the relevant meeting.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    10.2 In case of a tie the Chairman (or any other Board member) shall not have a casting vote.
    </p>
    </div>
    <div>
    <h3>Article 11. Board Proceedings</h3>
    <p>
    <i class="fas fa-caret-right"></i>
    11.1 The Board of Directors shall meet upon call by (or on behalf of) the Chairman or any two Directors. The Board of Directors shall meet as often as required by the interest of the Company.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    11.2 Notice of any meeting of the Board of Directors must be given by letter, cable, telegram, telephone, facsimile transmission, telex or e-mail advice to each Director, two (2) days before the meeting, except in the case of an emergency, in which event a twenty four (24) hours notice shall be sufficient. No convening notice shall be required for meetings held pursuant to a schedule previously approved by the Board and communicated to all Board members. A meeting of the Board may also be validly held without convening notice to the extent the Directors present or represented do not object and those Directors not present or represented have waived the convening notice in writing, by fax or email.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    11.2 Notice of any meeting of the Board of Directors must be given by letter, cable, telegram, telephone, facsimile transmission, telex or e-mail advice to each Director, two (2) days before the meeting, except in the case of an emergency, in which event a twenty four (24) hours notice shall be sufficient. No convening notice shall be required for meetings held pursuant to a schedule previously approved by the Board and communicated to all Board members. A meeting of the Board may also be validly held without convening notice to the extent the Directors present or represented do not object and those Directors not present or represented have waived the convening notice in writing, by fax or email.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    11.3 Meetings of the Board of Directors may be held physically or, in all circumstances, by way of conference call (or similar means of communication which permit the participants to communicate with each other).
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    11.4 sAny Director may act at any meeting of the Board of Directors by appointing in writing by letter or by cable, telegram, facsimile transmission or e-mail another Director as his proxy. A Director may represent more than one of the other Directors.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    11.5 The Board of Directors may deliberate and act validly only if the majority of the Board members (able to vote) are present or represented.  Decisions shall be taken by a simple majority of the votes validly cast by the Board members present or represented (and able to vote). 
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    11.6 Meetings of the Board of Directors may be validly held at any time and in all circumstances by means of telephonic conference call, videoconference or any other means, which permit the participants to communicate with each other. A Director attending in such manner shall be deemed present at the meeting for as long as he is connected.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    11.7 The Board of Directors may also in all circumstances with unanimous consent pass resolutions by circular means and written resolutions signed by all members of the Board will be as valid and effective as if passed at a meeting duly convened and held. Such signatures may appear on a single document or multiple copies of an identical resolution and may be evidenced by letters, cables, facsimile transmission, or e-mail.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    11.8 The minutes of any meeting of the Board of Directors (or copies or extracts of such minutes which may be produced in judicial proceedings or otherwise) shall be signed by the Chairman, the chairman (ad hoc) of the relevant meeting or by any two (2) Directors or as resolved at the relevant Board meeting or any subsequent Board meeting.
    </p>
    </div>
    <div>
    <h3>Article 12. Delegation of power, committees, secretary</h3>
    <p>
    <i class="fas fa-caret-right"></i>
    12.1 The Board may delegate the daily management of the business of the Company, as well as the power to represent the Company in its day to day business, to individual Directors or other officers or agents of the Company (with power to sub-delegate). In addition the Board of Directors may delegate the daily management of the business of the Company, as well as the power to represent the Company in its day to day business to an executive or other committee as it deems fit. The Board of Directors shall determine the conditions of appointment and dismissal as well as the remuneration and powers of any person or persons so appointed.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    12.2 The Board of Directors may (but shall not be obliged to unless required by law) establish one or more committees (including without limitation an audit committee, a risk and strategy committee, and a compensation committee) and for which it shall, if one or more of such committees are set up, appoint the members (who may be but do not need to be Board members), determine the purpose, powers and authorities as well as the procedures and such other rules as may be applicable thereto (subject as to the audit committee as set forth below).
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    11.2 Notice of any meeting of the Board of Directors must be given by letter, cable, telegram, telephone, facsimile transmission, telex or e-mail advice to each Director, two (2) days before the meeting, except in the case of an emergency, in which event a twenty four (24) hours notice shall be sufficient. No convening notice shall be required for meetings held pursuant to a schedule previously approved by the Board and communicated to all Board members. A meeting of the Board may also be validly held without convening notice to the extent the Directors present or represented do not object and those Directors not present or represented have waived the convening notice in writing, by fax or email.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    12.2.1. Audit Committee: in the case the Board of Directors decides to set up an audit committee (the “Audit Committee”), such Audit Committee shall be composed of at least three (3) members and the Board of Directors shall appoint one of the members of the Audit Committee as the chairperson of the Audit Committee. The Audit Committee shall (a) assist the Board of Directors in fulfilling its oversight responsibilities relating to the integrity of the Company’s financial statements, including periodically reporting to the Board of Directors on its activity and the adequacy of the Company’s systems of internal controls over financial reporting; (b) make recommendations for the appointment, compensation, retention and oversight of, and consider the independence of, the Company’s external auditors; (c) review Material Transactions between the Company or its subsidiaries with Related Parties (other than transactions that were reviewed and approved by the independent members of the Board of Directors (if any) or other governing body of any subsidiary of the Company or through any other procedures as the Board of Directors may deem substantially equivalent to the foregoing) to determine whether their terms are consistent with market conditions or are otherwise fair to the Company and its subsidiaries; and (d) perform such other duties imposed to it by the laws and regulations of the Regulated Market(s) on which the shares of the Company are listed applicable to the Company, as well as any other duties entrusted to it by the Board of Directors. The Board of Directors shall allocate to the Audit Committee the necessary resources and authority to fulfil its functions.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    12.2.2 Compensation Committee: in the case the Board of Directors decides to set up an compensation committee (the “Compensation Committee”), such Compensation Committee shall review and approve the compensation and benefits of the executive officers and other key employees of the Company and its group, and make recommendations to the Board of Directors regarding principles for compensation, performance evaluation, and retention strategies. The Compensation Committee (if any) shall be responsible for designing and administering the Company’s equity-based incentive plans of the Company and its group.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    12.2.3 Risk and Strategy Committee: in the case the Board of Directors decides to set up an risk and strategy committee (the “Risk and Strategy Committee”), such Risk and Strategy Committee shall assist the Board of Directors in fulfilling its oversight responsibilities with regard to (i) evaluating the risks inherent in the business of the Company and its group and the control processes with respect to such risks; (ii) the assessment and review of credit, market, commercial, fiduciary, liquidity, reputational and operational risks; and (iii) maintaining a cooperative, interactive strategic planning process with executive officers, including the identification and setting of strategic goals and the review of potential acquisitions, joint ventures, and strategic alliances; and dispositions.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    12.3 The Board of Directors may appoint a secretary of the Company who may but does not need to be a member of the Board of Directors and determine his responsibilities, powers and authorities.
    </p>
    </div>
    <div>
    <h3>Article 13. Binding Signature</h3>
    <p>
    <i class="fas fa-caret-right"></i>
    The Company will be bound by the sole signature of the Chairman or the joint signature of any two (2) Director or by the sole or joint signatures of any persons to whom such signatory power shall have been delegated by the Board of Directors. For the avoidance of doubt, for acts regarding the daily management of the Company the Company will be bound by the sole signature of the administrateur délégué ("Chief Executive Officer" or "CEO") or any person or persons to whom such signatory power shall be delegated by the Board of Directors.
    </p>
    </div>
    <div>
    <h3>Article 14. Board Compensation. Indemnification</h3>
    <p>
    <i class="fas fa-caret-right"></i>
    14.1 The compensation of the Board of Directors will be decided by the General Meeting.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    14.2 The Directors are not held personally liable for the indebtedness or other obligations of the Company. As agents of the Company, they are responsible for the performance of their duties. Subject to the exceptions and limitations listed in article 14.3, every person who is, or has been, a Director or officer of the Company shall be indemnified by the Company to the fullest extent permitted by law against liability and against all expenses reasonably incurred or paid by him in connection with any claim, action, suit or proceeding which he becomes involved as a party or otherwise by virtue of his being or having been such Director or officer and against amounts paid or incurred by him in the settlement thereof. The words "claim", "action", "suit" or "proceeding" shall apply to all claims, actions, suits or proceedings (civil, criminal or otherwise including appeals) actual or threatened and the words "liability" and "expenses" shall include without limitation attorneys' fees, costs, judgements, amounts paid in settlement and other liabilities.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    14.3 No indemnification shall be provided to any Director or officer:
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    14.3.1  Against any liability to the Company or its shareholders by reason of wilful misfeasance, bad faith, gross negligence or reckless disregard of the duties involved in the conduct of his office;
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    14.3.2  With respect to any matter as to which he shall have been finally adjudicated to have acted in bad faith and not in the interest of the Company; or
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    14.3.3  In the event of a settlement, unless the settlement has been approved by a court of competent jurisdiction or by the Board of Directors.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    14.4 The right of indemnification herein provided shall be severable, shall not affect any other rights to which any Director or officer may now or hereafter be entitled, shall continue as to a person who has ceased to be such Director or officer and shall inure to the benefit of the heirs, executors and administrators of such a person. Nothing contained herein shall affect any rights to indemnification to which corporate personnel, including directors and officers, may be entitled by contract or otherwise under law.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    14.5 Expenses in connection with the preparation and representation of a defence of any claim, action, suit or proceeding of the character described in this Article shall be advanced by the Company prior to final disposition thereof upon receipt of any undertaking by or on behalf of the officer or director, to repay such amount if it is ultimately determined that he is not entitled to indemnification under this article.
    </p>
    </div>
    <div>
    <h3>Article 15. Conflicts of Interest</h3>
    <p>
    <i class="fas fa-caret-right"></i>
    15.1 No contract or other transaction between the Company and any other company or firm shall be affected or invalidated by the fact that any one or more of the Directors or officers of the Company is interested in, or is a director, associate, officer, agent, adviser or employee of such other company or firm. Any Director or officer who serves as a director, officer or employee or otherwise of any company or firm with which the Company shall contract or otherwise engage in business shall not, by reason of such affiliation with such other company or firm only, be prevented from considering and voting or acting upon any matters with respect to such contract or other business.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    15.2 In the case of a personal conflict of interest of a Director, such Director shall indicate such conflict of interest to the Board and shall not deliberate or vote on the relevant matter. Any conflict of interest arising at Board level shall be reported to the next General Meeting of Shareholders before any resolution as and to the extent required by law.
    </p>
    </div>
</div>
<div class="part-five">
<h2>PART V. GENERAL MEETINGS OF SHAREHOLDERS</h2>
<div>
<h3>Article 16. General Meetings of Shareholders</h3>
<p>
<i class="fas fa-caret-right"></i>
16.1 Any regularly constituted General Meeting of Shareholders of the Company shall represent the entire body of Shareholders of the Company. It shall have the broadest powers to order, carry out or ratify acts relating to the operations of the Company.
</p>
<p>
<i class="fas fa-caret-right"></i>
16.2 The annual general meeting of Shareholders shall be held, in accordance with Luxembourg law, at the registered office of the Company, or at such other place in Luxembourg as may be specified in the notice of meeting on the third Wednesday of April of each year at 16.00 (local time) (or such other date as may be permitted by law). If such day is a legal holiday, the annual General Meeting shall be held on the next following business day.
</p>
<p>
<i class="fas fa-caret-right"></i>
16.3 Other meetings of Shareholders may be held at such place and time as may be specified in the respective notices of meeting.
</p>
<p>
<i class="fas fa-caret-right"></i>
16.4 General Meetings shall be convened in accordance with the provisions of law and in the case the Shares of the Company are listed on a Regulated Market, in accordance with the publicity requirements of such Regulated Market applicable to the Company. If all of the Shareholders are present or represented at a general meeting of Shareholders, the General Meeting may be held without prior notice or publication.
</p>
<p>
<i class="fas fa-caret-right"></i>
16.5 In case the shares of the Company are not listed in a any Regulated Market, all Shareholders recorded in the share register on the date of the General Meeting are entitled to be admitted in the General Meeting; provided, however, that in case the Shares of the Company are listed on a Regulated Market, the Board of Directors may determine a date preceding the General Meeting as the record date for admission to the General Meeting (the “Record Date”).
</p>
<p>
<i class="fas fa-caret-right"></i>
16.6 Where, in accordance with the provisions of Article 6.1.7 of the present Articles, Shares are recorded in the register(s) of Shareholders in the name of a Depositary or sub-depositary of the former, the certificates provided for in Article 6.1.7 must be received by the Company (or its agents as set forth in the convening notice) no later than the day preceding the fifth (5th) working day before the date of the General Meeting unless the Board fixes a different period.  Such certificates must (unless otherwise required by applicable law) certify the fact that the Shares in the account shall be blocked until the close of the General Meeting. All proxies must be received by the Company (or its agents) by the same deadline provided that the Board of Directors may, if it deems so advisable amend these periods of time for all Shareholders and admit Shareholders (or their proxies) who have provided the appropriate documents to the Company (or its agents as aforesaid) to the General Meeting, irrespective of these time limits.
</p>
<p>
<i class="fas fa-caret-right"></i>
16.7 The Board of Directors shall adopt all other regulations and rules concerning the attendance to the General Meeting, and availability of access cards, proxy forms and/or voting forms in order to enable Shareholders to exercise their right to vote.
</p>
<p>
<i class="fas fa-caret-right"></i>
16.8 Any Shareholder may be represented at a General Meeting by appointing as his or her proxy another person, who need not be a Shareholder.
</p>
</div>
<div>
<h3>Article 17. Majority and quorum at the General Meeting</h3>
<p>
<i class="fas fa-caret-right"></i>
17.1 At any General Meeting of Shareholders other than a General Meeting convened for the purpose of amending the Company’s Articles of Incorporation or voting on resolutions whose adoption is subject to the quorum and majority requirements for amendments of the Articles of Incorporation, no presence quorum is required and resolutions shall be adopted, irrespective of the number of Shares represented, by a simple majority of votes validly cast.
</p>
<p>
<i class="fas fa-caret-right"></i>
17.2 At any extraordinary General Meeting of Shareholders for the purpose of amending the Company’s Articles of Incorporation or voting on resolutions whose adoption is subject to the quorum and majority requirements for amendments of the Articles of  Incorporation, the quorum shall be at least one half of the issued share capital of the Company. If the said quorum is not present, a second Meeting may be convened at which there shall be no quorum requirement. In order for the proposed resolutions to be adopted at such a General Meeting, and save as otherwise provided by law, a two thirds (2/3) majority of the votes validly cast at any such General Meeting.
</p>
   </div>
</div>
<div class="part-six">
   <h2>PART VI. AMENDMENT OF ARTICLES</h2>
<div>
<h3>Article 18. Amendments of Articles</h3>
<p>
<i class="fas fa-caret-right"></i>
The Articles of Incorporation may be amended from time to time by a resolution of the General Meeting of Shareholders to the quorum and voting requirements provided by the laws of Luxembourg and as may otherwise be provided herein.
</p>
   </div>
</div>
<div class="part-seven">
   <h2>PART VIII. DISTRIBUTIONS, WINDING UP</h2>
<div>
<h3>Article 21. Distributions</h3>
<p>
<i class="fas fa-caret-right"></i>
21.1 From the annual net profits of the Company, five per cent (5%) shall be allocated to an un-distributable reserve required by law. This allocation shall cease to be required as soon and as long as such reserve amounts to ten per cent (10%) of the issued share capital of the Company.
</p>
<p>
<i class="fas fa-caret-right"></i>
21.2 The General Meeting of Shareholders, upon recommendation of the Board of Directors, will determine how the remainder of the annual net profits will be disposed of, including by way of stock dividend.
</p>
<p>
<i class="fas fa-caret-right"></i>
21.3 Interim distributions may be declared and paid (including by way of staggered payments) by the Board of Directors subject to observing the terms and conditions provided by law either by way of a cash distribution or by way of an in kind distribution.
</p>
<p>
<i class="fas fa-caret-right"></i>
21.4 In the event it is decided by the General Meeting, or in the case interim distributions declared by the Board, that a distribution be paid in Shares or other securities of the Company, the Board of Directors may exclude from such offer such Shareholders he deems necessary or advisable due to legal or practical problems in any territory or for any other reasons as the Board may determine.
</p>
</div>
<div>
<h3>Article 22. Liquidation</h3>
<p>
<i class="fas fa-caret-right"></i>
22.1 In the event of the dissolution of the Company for whatever reason or whatever time, the liquidation will be performed by liquidators or by the Board of Directors then in office who will be endowed with the powers provided by articles 144 et seq. of the Luxembourg law of 10th August 1915 on commercial companies. Once all debts, charges and liquidation expenses have been met, any balance resulting shall be paid to the holders of Shares in the Company in accordance with the provisions of these Articles.
</p>
</div>
</div>
<div class="part-eight">
<h2>PART IX. SOLE SHAREHOLDER, DEFINITIONS, APPLICABLE LAW</h2>
<div>
<h3>Article 23. Sole Shareholder</h3>
<p>
<i class="fas fa-caret-right"></i>
If, and as long as one Shareholder holds all the Shares of the Company, the Company shall exist as a single Shareholder company pursuant to the provisions of Company Law. In the event the Company has only one Shareholder or two Shareholders, the Company may at the option of the sole Shareholder or as the case may be the two Shareholders, be managed by one or two Directors as provided for by law and all provisions in the present Articles referring to the Board of Directors shall be deemed to refer to the sole Director or the two Directors (mutatis mutandis) who shall have all such powers as provided for by law and as set forth in the present Articles with respect to the Board of Directors.
</p>
</div>
<div>
<h3>Article 24. Definitions</h3>
<h4>Affiliate</h4>
<p>
<i class="fas fa-caret-right"></i>
Means, in relation to a person or entity, a person that directly or indirectly through one or more intermediaries, Controls, is Controlled by, or is under common Control with, such person or entity. The term “Affiliated with” has a meaning correlative to the foregoing.
</p>
</div>
<div>
<h4>Articles or Articles of Incorporation</h4>
<p>
<i class="fas fa-caret-right"></i>
Means the present articles of incorporation of the Company as amended from time to time
</p>
</div>
<div>
<h4>Board or Board of Directors</h4>
<p>
<i class="fas fa-caret-right"></i>
Means the Board of Directors (conseil d’administration) of the Company
</p>
</div>
<div>
<h4>Control</h4>
<p>
<i class="fas fa-caret-right"></i>
Means, in relation to a person or entity, the possession, directly or indirectly, of the power to direct or cause the direction of the management and policies of such person or entity, whether through ownership of voting securities, by contract or otherwise.
</p>
</div>
<div>
<h4>Director</h4>
<p>
<i class="fas fa-caret-right"></i>
Means a member of the Board of Directors or as the case may be, the sole Director of the Company
</p>
</div>
<div>
<h4>General Meeting</h4>
<p>
<i class="fas fa-caret-right"></i>
Means the general meeting of Shareholders
</p>
</div>
<div>
<h4>independent members of the Board of Directors</h4>
<p>
<i class="fas fa-caret-right"></i>
Means a Director who: (i) is not employed, and has not been employed within the five years immediately prior to the ordinary General Meeting at which the candidates to the Board of Directors will be voted upon, by the Company or any of its subsidiaries in an executive capacity; (ii) does not receive consulting, advisory or other compensatory fees from the Company or any of its subsidiaries (other than fees received as member of the Board of Directors or any committee thereof and fees received as member of the board of directors or other governing body, or any committee thereof, of any of the Company’s subsidiaries); (iii) does not Control the Company; (iv) has not (and does not Control a business entity that has) a material business relationship with the Company, any of its subsidiaries, or the person that directly or indirectly Controls the Company, if such material business relationship would be reasonably expected to adversely affect the director’s ability to properly discharge its duties; (v) does not Control, and is not, and has not been within the five-year period immediately prior to the ordinary shareholders’ meeting at which the candidates to the Board of Directors will be voted upon, employed by, a (present or former) internal or external auditor of the Company, any of its subsidiaries or the person that directly or indirectly Controls the Company; and (vi) is not a spouse, parent, sibling or relative up to the third degree of, and does not share a home with, any person above described from (i) to (iv).
</p>
</div>
<div>
<h4>Material Transactions</h4>
<p>
<i class="fas fa-caret-right"></i>
Means (i) any transaction (x) with an individual value equal to or greater than ten million United States Dollars (USD 10,000,000); (y) with an individual value lower than ten million United States Dollars (USD 10,000,000), when the aggregate sum of any series of transactions of such lower value reflected in the financial statements of the four fiscal quarters of the Company preceding the date of determination (excluding any transactions that were reviewed and approved by any of the Audit Committee (if any), the Board of Directors or the independent members of the Board of Directors or other governing body of any subsidiary of the Company, or through any other procedures as the Board of Directors may deem substantially equivalent to the foregoing), exceeds 1.5% of the Company’s consolidated net sales made in the fiscal year preceding the year on which the determination is made; or (ii) any corporate reorganization transaction (including a merger, a spin-off or a bulk transfer of a business) involving the Company or any of its subsidiaries for the benefit of, or involving, a Related Party.
</p>
</div>
<div>
<h4>Regulated Market</h4>
<p>
<i class="fas fa-caret-right"></i>
Means any official stock exchange or securities exchange market in the European Union, the United States of America or elsewhere
</p>
</div>
<div>
<h4>Related Party</h4>
<p>
<i class="fas fa-caret-right"></i>
Means, in relation to the Company or its direct or indirect subsidiaries, any of the following persons: (i) a member of the Board of Directors or of the board of directors or other governing body of any of the Company’s subsidiaries; (ii) any member of the board of directors or other governing body of an entity that Controls the Company; (iii) any Affiliate of the Company (other than the Company’s subsidiaries); (iv) any entity Controlled by any member of the Board of Directors, or of the board of directors or other governing body of any subsidiary of the Company; and (v) any spouses, parents, siblings or relatives up to the third degree of, and any persons that share a home with, any person referred to in (i) or (ii).
</p>
</div>
<div>
<h4>Shareholder</h4>
<p>
<i class="fas fa-caret-right"></i>
Means a duly registered holder of Shares of the Company
</p>
</div>
<div>
<h4>Shares</h4>
<p>
<i class="fas fa-caret-right"></i>
Means the shares (actions) of the Company
</p>
</div>
</div>

</section>
@endsection