@extends('layouts.app')
@section('title', 'Adecoagro IR - Home')

@section('content')
<div class="img-directors" style="background:url({{ asset('img/governance/code-of-ethics.jpg') }}) no-repeat center/cover">
<div class="container">
    <h2>Code of Business Conduct and Ethics</h2>
</div>
</div>
<section class="container ethics my-5">
    <p>
        <i class="fas fa-caret-right"></i>
        Adecoagro S.A., including its affiliates (the "Company") is committed to the highest standards of legal and ethical business conduct. This Code of Business Conduct and Ethics (the "Code") summarizes the legal, ethical and regulatory standards that the Company must follow and is a reminder to our directors, officers and employees, of the seriousness of that commitment. Compliance with this Code and high standards of business conduct is mandatory for every Company employee.
    </p>
    <h4>Introduction</h4>
    <p>
        <i class="fas fa-circle"></i>
        Our business is becoming increasingly complex, both in terms of the geographic areas in which we operate and the laws with which we must comply. To help our directors, officers, employees and, if the case, suppliers and contractors, understand what is expected of them and to carry out their responsibilities, we have created this Code of Business Conduct and Ethics. Additionally, we have created a new position of Company Ethics Officer to oversee adherence to the Code.
    </p>
    <p>
        <i class="fas fa-circle"></i>
        This Code is not intended to be a comprehensive guide to all of our policies or to all your responsibilities under law or regulation. It provides general parameters to help you resolve the ethical and legal issues you encounter in conducting our business. Think of this Code as a guideline, or a minimum requirement, that must always be followed. The Company may introduce more restrictive policies or practices that must adhere to. If you have any questions about anything in the Code or appropriate actions in light of the Code, you may contact the Company Ethics Officer.
    </p>
    <p>
        <i class="fas fa-circle"></i>
        We expect each of our directors, officers and employees to read and become familiar with the ethical standards described in this Code and to affirm your agreement to adhere to these standards by signing the Compliance Certificate that appears at the end of this Code. Violations of the law, our corporate policies, or this Code may lead to disciplinary action, including dismissal.
    </p>
    <h4>1) We Insist on Honest and Ethical Conduct by All of Our Directors, Officers, Employees and Other Representatives</h4>
    <p>
        <i class="fas fa-circle"></i>
        We have built our business based on excellence in products and services: not only quality products and services but quality employees and representatives who adhere to the very highest standards of honesty, ethics and fairness in our dealings with all of our business contacts. We place the highest value on the integrity of our directors, our officers and our employees and demand this level of integrity in all of our dealings. We insist not only on ethical dealings with others, but on the ethical handling of actual or apparent conflicts of interest between personal and professional relationships.
    </p>
    <h4>Fair Dealing</h4>
    <p>
        <i class="fas fa-circle"></i>
        Directors, officers and employees are required to deal honestly and fairly with our customers, employees, collaborators, competitors and other third parties.
    </p>
    <p>
        <i class="fas fa-circle"></i>
        Although sales and costs are the lifeblood of any organization, we market our products and services fairly and vigorously based on our honesty, creativity and ingenuity and the proven quality and reliability of the products. Effectively, serving our investors and customers is our most important goal — in the eyes of the customer you are the Company. In our dealings with customers and suppliers, we:
    </p>
    <p>
        <i class="fas fa-circle"></i>
        Prohibit bribes, kickbacks or any other form of improper payment, direct or indirect, to any representative of government, labor union, customer or supplier in order to obtain a contract, some other commercial benefit or government action;
    </p>
    <p>
        <i class="fas fa-circle"></i>
        Prohibit our directors, officers and employees from accepting any bribe, kickback or improper payment from anyone;
    </p>
    <p>
        <i class="fas fa-circle"></i>
        Prohibit gifts or favors of more than nominal value to or from our customers or suppliers without the prior authorization of Ethics Officer;
    </p>
    <p>
        <i class="fas fa-circle"></i>
        Limit marketing and client entertainment expenditures to those that are necessary, prudent, job-related and consistent with our policies;
    </p>
    <p>
        <i class="fas fa-circle"></i>
        Require clear and precise communication in our contracts, our advertising, our literature, and our other public statements and seek to eliminate misstatement of fact or misleading impressions;
    </p>
    <p>
        <i class="fas fa-circle"></i>
        Reflect accurately on all invoices to customers the sale price and terms of sales for products sold or services rendered;
    </p>
    <p>
        <i class="fas fa-circle"></i>
        Protect all proprietary data our customers or suppliers provide to us as reflected in our agreements with them; and
    </p>
    <p>
        <i class="fas fa-circle"></i>
        Prohibit our representatives from otherwise taking unfair advantage of our customers or suppliers, or other third parties, through manipulation, concealment, abuse of privileged information or misrepresentation of material facts or any other unfair-dealing practice.
    </p>
    <h4>Conflicts of Interest; Corporate Opportunities</h4>
    <p>
        <i class="fas fa-circle"></i>
        Our directors, officers and employees should not be involved in any activity that creates or gives the appearance of a conflict of interest between their personal interests and the interests of the Company. A "conflict of interest" occurs when a director's, officer's or employee's personal interest interferes with the Company's interest. In particular, without the specific permission of the Company's Ethics Officer, no director, officer or employee shall:
    </p>
    <p>
        <i class="fas fa-circle"></i>
        Be a consultant to, or a director, officer or employee of, or otherwise operate an outside business that: ll:
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
        Markets products or services in competition with our current or potential products and services;
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    Enters into a farm lease agreement or any similar transaction that might create or give an appearance of a conflict of interest; except for those transactions or investments made before becoming an employee of the Company or follow-on investments related to such transactions.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    Enter into any transaction with the Company's suppliers for the acquisition of goods and/or services to be used and/or rendered in properties or assets which are not the property of the Company.
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    Supplies products or services to the Company; 
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    Purchases products or services from the Company; or
    </p>
    <p>
    <i class="fas fa-caret-right"></i>
    Has any financial interest, including significant stock ownership, in any entity with which we do business that might create or give the appearance of a conflict of interest;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Divert to himself or herself or to others any opportunities that are discovered through the use of the Company's property or information or as a result of his or her position with the Company;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Seek or accept any personal loan, or services from any entity with which we do business, except from financial institutions or service providers offering similar loans or services to third parties under similar terms in the ordinary course of their respective businesses;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Be a consultant to, or a director, officer or employee of, or otherwise operate an outside business if the demands of the outside business would interfere with the director's, officer's or employee's responsibilities to us;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Accept any personal loan or guarantee of obligations from the Company, except to the extent such arrangements are legally permissible upon the prior written approval of the Company; or
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Conduct business on behalf of the Company with family members.
    </p>
    <p>
    In many cases a potential conflict of interest or violation of trust may be avoided by making a full disclosure of the facts to the Company prior to the completion of any transaction, thereby permitting the Company to make an informed, independent decision regarding the implication of any such transaction.
Directors, officers, and employees must notify the Ethics Officer, of any situation that could reasonably be expected to give rise to the existence of any actual or potential conflict of interest.
    </p>
    <h4>Confidentiality and Corporate Assets</h4>
    <p>
    <i class="fas fa-circle"></i>
    Our directors, officers and employees are entrusted with our confidential information and with the confidential information of our suppliers, customers or other business partners. This information may include (1) technical information about current and future products, services or research, (2) business or marketing plans or projections, (3) earnings and other internal financial data, (4) personnel information, (5) supply and customer lists and (6) other non-public information that, if disclosed, might be of use to our competitors, or harmful to the Company, our suppliers, customers or other business partners. This information is our property, or the property of our suppliers, customers or business partners and in many cases was developed at great expense. Our directors, officers and employees shall:
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Not discuss confidential information with or in the presence of any unauthorized persons, including family members and friends;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Use confidential information only for our legitimate business purposes and not for personal gain;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Not disclose confidential information to third parties; and
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Not use the Company property or resources for any personal benefit or the personal benefit of anyone else. The Company property includes the Company internet, email, and voicemail services, which should be used only for business related activities, and which may be monitored by the Company at any time without notice.
    </p>
    <h4>2) We Provide Full, Fair, Accurate, Timely and Understandable Disclosure</h4>
    <p>
    <i class="fas fa-circle"></i>
    We are committed to providing our investors with full, fair, accurate, timely and understandable disclosures. To this end, our directors, officers and employees shall:
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Not make false or misleading entries in our books and records for any reason;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Not condone any undisclosed or unrecorded bank accounts or assets established for any purpose;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Comply with generally accepted accounting principles in the relevant jurisdiction at all times;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Notify our Chief Financial Officer if there is an unreported transaction;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Maintain a system of internal accounting controls that will provide reasonable assurances to management that all transactions are properly recorded;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Maintain books and records that accurately and fairly reflect our transactions;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Prohibit the establishment of any undisclosed or unrecorded funds or assets;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Maintain a system of internal controls that will provide reasonable assurances to our management that material information about the Company is made known to management, particularly during the periods in which our periodic reports are being prepared;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Present information in a clear and orderly manner and avoid the use of unnecessary legal and financial language in our periodic reports; and
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Not communicate to the public any nonpublic information except through our Chief Executive Officer, Chief Financial Officer, Chief Legal Officer or Investor Relations Manager.
    </p>
    <h4>3) We Comply With all Laws, Rules and Regulations</h4>
    <p>
    <i class="fas fa-circle"></i>
    We will comply with all laws and governmental regulations that are applicable to our activities, and expect all our directors, officers and employees to obey the law. Specifically, we are committed to:
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Maintaining a safe and healthy work environment;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Promoting a workplace that is free from discrimination or harassment based on race, color, religion, sex, age, national origin, disability or other factors that are unrelated to the Company's business interests;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Supporting fair competition and laws prohibiting restraints of trade and other unfair trade practices;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Conducting our activities in full compliance with all applicable environmental laws;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Keeping the political activities of our directors, officers and employees separate from our business;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Prohibiting any illegal payments, gifts, or gratuities to any government officials or political party;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Prohibiting the unauthorized use, reproduction, or distribution of any third party's trade secrets, copyrighted information or confidential information;
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Prohibiting the sale or export, either directly or through our representatives, of our products to countries where technology related goods such as ours may not be sold; 
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Complying with all applicable state and federal securities laws or any provisions of applicable state or federal law relating to fraud against investors; and 
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Not tolerating any behavior that could constitute securities fraud, mail fraud, bank fraud, or wire fraud.
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Our directors, officers and employees are prohibited from trading our securities while in possession of material, nonpublic ("inside") information about the Company. Please see Adecoagro's Insider Trading Policy available at website (www.adecoagro.com) or ask for a copy to Human Resources Department or to the Ethics Officer. 
    </p>
    <h4>REPORTING AND EFFECT OF VIOLATIONS</h4>
    <p>
    <i class="fas fa-circle"></i>
    Compliance with the Code is, first and foremost, the individual responsibility of every director, officer and employee. We attempt to foster a work environment in which ethical issues and concerns may be raised and discussed with supervisors or with others without the fear of retribution. It is our responsibility to provide a system of reporting and access when you wish to report a suspected violation, or to seek counseling, when the normal chain of command cannot, for whatever reason, be used. 
    </p>
    <h4>Administration</h4>
    <p>
    <i class="fas fa-circle"></i>
    The Board of Directors has established the standards of business conduct contained in this Code and oversee compliance with this Code. They have also created empowered the Ethics Officer to ensure adherence to the Code. 
    Training on this code will be included in the orientation of new employees and provided to existing directors, officers, and employees on an on-going basis. To ensure familiarity with the Code, directors, officers, and employees shall read the Code and sign annually a Compliance Certificate of all policies of the Company (Code of Business Conducts and Ethics, Whistleblower Policy, Insider Trading Policy and Instructive for the Compliance of the U.S. Foreign Corrupt Practices Act - FCPA) in identical form as the one hereby attached as Exhibit I.
    </p>
    <h4>Reporting Violations and Questions</h4>
    <p>
    <i class="fas fa-circle"></i>
    Directors, officers, and employees must report, in person or in writing, any known or suspected violations of laws, governmental regulations or the Code, to the Ethics Officer. Additionally, directors, officers, and employees may contact the Ethics Officer with a question or concern about the Code or a business practice. Any questions or violation reports will be addressed immediately and seriously, and can be made anonymously. If you feel uncomfortable reporting suspected violations to this individual, you may report matters to Pillsbury Winthrop Shaw Pittman LLP, our outside counsel. The address and telephone number of these individuals are listed in Exhibit II attached to the Code.
    </p>
    <p>
    <i class="fas fa-circle"></i>
    It is not sufficient to report a suspected violation of the Code to a co-worker or to any person other than one of the individuals designated above.
    </p>
    <p>
    <i class="fas fa-circle"></i>
    Upon receipt of a complaint under the Code, the Company will investigate the complaint and will involve agencies and resources outside the Company if and/or when such outside involvement appears advisable or necessary. The report and investigation will be kept confidential to the extent consistent with the need for a thorough investigation and response and taking into consideration the Company's disclosure obligations and requirements.
    </p>
    <h4>Consequences of a Violation</h4>
    <p>
    <i class="fas fa-circle"></i>
    If it is determined that a director, officer or employee of the Company has violated the Code, the Company will take appropriate action including, but not limited to, disciplinary action, up to and including termination of employment. If it is determined that a non-employee (including any contractor, subcontractor, or other agent) has violated the Code, the Company will take appropriate corrective action, which could include severing the contractor, subcontractor, or agency relationship. In either event, the Company will take necessary corrective action reasonably calculated to address and to correct the alleged violation.
    </p>
    <h4>We will not allow any retaliation against a director, officer or employee who acts in good faith in reporting any violation.</h4>
    <p>
    <i class="fas fa-circle"></i>
    The Company is committed to maintaining an environment in which people feel free to report all suspected incidents of inaccurate financial reporting or fraud. No retaliatory action will be taken against any person who reports any conduct which he or she reasonably believes may violate the Code. In addition, no retaliatory action will be taken against any individual who in good faith assists or participates in an investigation, proceeding, or hearing relating to a complaint about the Company's auditing or financial disclosures, or who files, causes to be filed, testifies, or otherwise assists in such a proceeding against the Company.
    </p>
    <p>
    <i class="fas fa-circle"></i>
    If you have any questions on the scope and application of this or other policy, please contact the Ethics Officer.
    </p>
    <h4>ADECOAGRO S.A.</h4>
    <h4 class="mt-4">*Please read and consult all policies of the Company:</h4>
    <p>Code of Business Conduct and Ethics
Whistleblower Policy
Insider Trading Policy
Instructive for the Compliance of the U.S. Foreign Corrupt Practices Act - FCPA</p>
<h4>EXHIBIT I</h4>
<h4 class="mt-4">COMPLIANCE CERTIFICATE</h4>
<p><i class="fas fa-circle"></i>
    I have read and understand the following Policies and Procedures of the Company:
</p>
<p>
    <i class="fas fa-caret-right"></i>
    Code of Business Conduct and Ethics
</p>
<p>
    <i class="fas fa-caret-right"></i>
    CWhistleblower Policy
</p>
<p>
    <i class="fas fa-caret-right"></i>
    CInsider Trading Policy
</p>
<p>
    <i class="fas fa-caret-right"></i>
    CInstructive for the Compliance of the U.S. Foreign Corrupt Practices Act - FCPA
</p>
<p>
    <i class="fas fa-circle"></i>
    The Company has explained the scope and requirements of the different Policies and Procedures. 
</p>
<p>
I commit to comply with all aspects of the Policies and Procedures hereby described. Also, I understand that any breach to the Policies and Procedures is considered a serious violation that may lead to disciplinary measures, including job dismissal. 
</p>
<p>I hereby declare that I am in compliance and not in breach of the Policies and Procedures and that any exception to said compliance is declared in the Statement of Exceptions attached to this Compliance Certificate. </p>
<p>Signature: _____________________________</p>
<p>Name: ________________________________</p>
<p>Date: _________________________________</p>
<p>Work position: _________________________</p>
<h4>Check one of the following:</h4>
<p>
    <i class="fas fa-circle"></i>
    A Statement of Exceptions is attached.
</p>
<p>
    <i class="fas fa-circle"></i>
    No Statement of Exceptions is attached.
</p>
<h4>EXHIBIT II</h4>
<h4 class="mt-4">REPORTING CONTACTS</h4>
<h4 class="mt-4">Names and Addresses</h4>
<p>
    <i class="fas fa-caret-right"></i>
    Reporting Contacts:
</p>
<p>
    <i class="fas fa-caret-right"></i>
    Ethics Officer / Chief Legal Officer
</p>
<p>
    <i class="fas fa-circle"></i>
    Name: Emilio Gnecco
</p>
<p>
    <i class="fas fa-circle"></i>
    Address: Fondo de la Legua 936, B1640FWB,
   Martínez, Provincia de Buenos Aires,
</p>
<p>
    <span>Argentina</span> <br>
    <i class="fas fa-circle"></i>
    Phone: +5411 4836 8680
</p>
<p>
    <i class="fas fa-circle"></i>
    E-mail: egnecco@adecoagro.com
</p>
<p>
    <i class="fas fa-caret-right"></i>
    Additional Reporting Contact:
</p>
<p>
    <i class="fas fa-caret-right"></i>
    Our Outside Counsel:
</p>
<p>
    <i class="fas fa-circle"></i>
    Holland & Knight LLP
</p>
<p>
    <i class="fas fa-circle"></i>
    Address: 31 West 52nd Street
   New York, NY 10019
</p>
<p>
    <i class="fas fa-circle"></i>
    Phone: +1 212 513 3463
</p>
<p>
    <i class="fas fa-circle"></i>
    E-mail: Frank.Vivero@hklaw.com
</p>
</section>
@endsection