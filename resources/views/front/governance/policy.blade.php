@extends('layouts.app')
@section('title', 'Adecoagro IR - Home')

@section('content')
<div class="img-directors" style="background:url({{ asset('img/governance/dividend-policy.jpg') }}) no-repeat center/cover">
<div class="container">
    <h2>Dividend Policy</h2>
</div>
</div>
<section class="container policy my-5">
<div class="">
    <p><i class="fas fa-caret-right"></i>
     The amount and payment of dividends will be determined by a simple majority vote at a general shareholders’ meeting, typically but not necessarily, based on the recommendation of the board of directors. All shares of Adecoagro´s capital stock rank pari passu with respect to the payment of dividends. Pursuant to the company´s articles of incorporation, the board of directors has the power to distribute interim dividends in accordance with applicable Luxembourg law. Dividends may be lawfully declared and paid if our net profits and distributable reserves are sufficient under Luxembourg law.</p>
    <p class="mt-4">
    <i class="fas fa-caret-right"></i>
    Under Luxembourg law, at least 5% of Adecoagro´s net profits per year must be allocated to the creation of a legal reserve until such reserve has reached an amount equal to 10% of its issued share capital. If the legal reserve subsequently falls below the 10% threshold, at least 5% of net profits again must be allocated toward the reserve. The legal reserve is not available for distribution.
    </p>
    <p class="mt-4">
    <i class="fas fa-caret-right"></i>
    Adecoagro S.A. is a holding company and has no material assets other than its ownership of partnership interests in IFH. IFH, in turn, is a holding entity with no material assets other than its indirect ownership of shares in operating subsidiaries in foreign countries. If we were to distribute a dividend at some point in the future, we would cause the operating subsidiaries to make distributions to IFH, which in turn would make distributions to Adecoagro S.A. in an amount sufficient to cover any such dividends.
    </p>
    <p class="mt-4">
    <i class="fas fa-caret-right"></i>
    Adecoagro´s subsidiaries in Argentina and Brasil are subject to certain restrictions on their ability to declare or pay dividends.
    </p>
    <p class="mt-4">
    <i class="fas fa-caret-right"></i>
    Adecoagro implemented a distribution policy as of January 1, 2022. The Company intends to distribute to shareholders annually a minimum of 40% of the Adjusted Free Cash Flow from Operations generated during the previous year. The Distribution Policy shall consist of a minimum cash dividend payable on the Company’s outstanding common stock in the aggregate amount of $30 million per year, in addition to share repurchases under the Company’s existing share repurchase program from time to time as deemed appropriate. Dividend payments are intended to be made in two installments in May and November of each year. Any dividend distribution is subject to the conditions of applicable law and approval of Adecoagro´s shareholders.
    </p>
</div>
</section>
@endsection