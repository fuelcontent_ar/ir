@extends('layouts.app')
@section('title', 'Adecoagro IR - Home')

@section('content')
<div class="img-directors" style="background:url({{ asset('img/governance/executive-officers.jpg') }}) no-repeat center/cover">
<div class="container">
    <h2>Executive Directors</h2>
</div>
</div>
<section class="container officers my-5">
<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Position</th>
            <th>Year Design</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Mariano Bosch</td>
            <td>Chief Executive Officer & Co-founder</td>
            <td>2002</td>
        </tr>
        <tr>
            <td>2</td>
            <td>Carlos A. Boero Hughes</td>
            <td>Chief Financial Officer</td>
            <td>2008</td>
        </tr>
        <tr>
            <td>3</td>
            <td>Emilio Federico Gnecco</td>
            <td>Chief Legal Officer</td>
            <td>2005</td>
        </tr>
        <tr>
            <td>4</td>
            <td>Renato Junqueira Santos Pereira</td>
            <td>Director of Sugar and Ethanol Operations</td>
            <td>2002</td>
        </tr>
        <tr>
            <td>5</td>
            <td>Mario José Ramón Imbrosciano</td>
            <td>Director of Business Development</td>
            <td>2003</td>
        </tr>
        <tr>
            <td>6</td>
            <td>Leonardo Berridi</td>
            <td>Country Manager for Brazil</td>
            <td>2004</td>
        </tr>
        <tr>
            <td>7</td>
            <td>Ezequiel Garbers</td>
            <td>Country Manager for ARG/URU & Co-founder</td>
            <td>2004</td>
        </tr>
        <tr>
            <td>8</td>
            <td>Alejandro López Moriena</td>
            <td>Chief Sustainability Officer</td>
            <td>2019</td>
        </tr>
        <tr>
            <td>9</td>
            <td>Victoria Cabello </td>
            <td>Investor Relations Manager</td>
            <td>2021</td>
        </tr>
    </tbody>
</table>
<div>
    <h3>Mariano Bosch.</h3>
    <p> <i class="fas fa-circle"></i>
    Mr. Bosch is a co-founder of Adecoagro and has been the Chief Executive Officer and a member of the Company’s board of directors since inception. From 1995 to 2002, Mr. Bosch served as the founder and Chief Executive Officer of BLS Agribusiness, an agricultural consulting, technical management and administration company.  Mr. Bosch has over 20 years of experience in agribusiness development and agricultural production. He actively participates in organizations focused on promoting the use of best practices in the sector, such as the Argentine Association of Regional Consortiums for Agricultural Experimentation (AACREA) and the Conservational Production Foundation (Producir Conservando). He graduated with a degree in Agricultural Engineering from the University of Buenos Aires. Mr. Bosch is an Argentine citizen.
    </p>
    <h3>Carlos A. Boero Hughes.</h3>
    <p><i class="fas fa-circle"></i>Mr. Boero Hughes is our Chief Financial Officer, covering the company’s operations in Argentina, Brazil and Uruguay, and a member of Adecoagro’s Senior Management since 2008. He began working at Adecoagro in August 2008 overseeing our finance and administrative departments. Mr. Boero Hughes has over 20 years of experience in agricultural business and financial markets. Prior to joining us, he was Chief Financial Officer for South America and Co-Chief Executive Officer for Noble Group LTD operations in Argentina, Uruguay and Paraguay from October 2006 to July 2008. From 2003 to 2006, he worked at Noble Group LTD as Financial Director for Argentina and Structure Finance Manager for South America. He worked at Citibank N.A. from 1997 to 2003 as Relationship and Product Manager, focused in the agribusiness industry, and at Banco Privado de Inversiones S.A. as Relationship Manager. He also worked for six years at Carlos Romano Boero S.A.I.C., a flour and dairy cow feed mill family company, as Commercial Manager, Local Grain Elevator and Nursery Manager and finally as General Manager. Mr. Boero Hughes holds a degree in Business Administration from the University of Buenos Aires and a Masters in Business Administration from the Argentine Catholic University. He also graduated from INSEAD’s Executive Program in 2007.</p>
    <h3>Emilio Federico Gnecco.</h3>
    <p><i class="fas fa-circle"></i>Mr. Gnecco is our Chief Legal Officer for all operations in Argentina, Brazil and Uruguay and a member of Adecoagro’s Senior Management since 2005. He is responsible for all legal and corporate matters and compliance. Before joining us, he was a corporate law associate at the law firm of Marval, O’Farrell & Mairal for more than 8 years, where he specialized in mergers and acquisitions, project financing, structured finance, corporate financing, private equity, joint ventures and corporate law and business contracts in general. Mr. Gnecco was in charge of Adecoagro’s corporate matters including mergers and acquisitions since our inception in 2002. Prior to that, he worked at the National Civil Court of Appeals of the City of Buenos Aires for four years. Mr. Gnecco has a law degree from the University of Buenos Aires, where he graduated with honors.</p>
    <h3>Renato Junqueira Santos Pereira.</h3>
    <p><i class="fas fa-circle"></i>Renato Junqueira Santos Pereira is the Director of our Sugar, Ethanol & Energy business and has been a member of the senior management team since 2014. He began working at Adecoagro in 2010 as the Operations Manager for our Sugar, Ethanol & Energy business and has vast experience in the Brazilian sugarcane industry. Before joining Adecoagro, he served as the CFO of Moema Group, one of the largest sugarcane clusters in Brazil. His main responsibilities at Moema included designing the optimal capital structure to finance the construction of five greenfield mills, preparing the company for an IPO and coordinating the M&A process which culminated in a $1.5 billion dollar sale to Bunge Ltda. Previously, Mr. Pereira held responsibilities as Mill Director and Agricultural Manager in Moema’s mills. He is an Agricultural Engineer from Universidade de Sao Paulo and holds an MBA from the University of California, Davis. </p>
    <h3>Mario José Ramón Imbrosciano. </h3>
    <p><i class="fas fa-circle"></i>Mr. Imbrosciano is the head of our Business Development Department for all operations in Argentina, Brazil and Uruguay where he oversees all new business initiatives, and a member of Adecoagro’s Senior Management since 2003. He has over 17 years of experience in farm management and agriculture production. Prior to joining Adecoagro, Mr. Imbrosciano was the Chief Operating Officer of Beraza Hnos. S.C., a farming company that owns farms in the humid pampas region of Argentina. He was in charge of production, commercialization and logistics for a 60,000 hectare operation. Mr. Imbrosciano has also worked as a private consultant for various clients. Mr. Imbrosciano received a degree in Agricultural Production Engineering from the Argentine Catholic University and holds a Masters in Business Administration from the Instituto de Altos Estudios of the Austral University.</p>
    <h3>Leonardo Raúl Berridi. </h3>
    <p><i class="fas fa-circle"></i>Mr. Berridi is our Country Manager for Brazil and, prior to the Reorganization, had been Adecoagro’s Country Manager for Brazil since the beginning of its operations in Brazil and a member of Adecoagro’s Senior Management since 2004. He coordinates all of our operations and human resources development activities in Brazil. Mr. Berridi has over 27 years of international experience in agricultural business. Prior to joining us, Mr. Berridi was Vice President of Pago Viejo S.A., a company dedicated to agriculture production and dairy farming in the western part of the province of Buenos Aires, Argentina. He also worked for Trans-Continental Tobacco Corporation as Chief Operating Officer of Epasa (Exportadora de Productos Agrarios S.A.), a company dedicated to producing, processing and exporting tobacco in the north east and north west of Argentina, and Production Manager of World Wide Tobacco España S.A. in the Caceres and Zamora provinces in Spain. Mr. Berridi holds a degree in Forestry Engineering from the Universidad Nacional de La Plata.</p>
    <h3>Ezequiel Garbers.</h3>
    <p><i class="fas fa-circle"></i>Mr. Garbers is the Country Manager for Argentina and Uruguay and a member of Adecoagro’s Senior Management and the Country Manager since 2002. He coordinates all of our production and human resources development activities in Argentina and Uruguay. Mr. Garbers has over 20 years of experience in agriculture production. Prior to joining Adecoagro, he was the Chief Operating Officer of an agricultural consulting and investment company he co-founded, developing projects both within and outside of Argentina, related to crop production and the cattle and dairy business. Mr. Garbers holds a degree in Agronomic Engineering from the University of Buenos Aires and a Masters in Business Administration from the Instituto de Altos Estudios of the Austral University.</p>
    <h3>Alejandro López Moriena.</h3>
    <p><i class="fas fa-circle"></i>Mr. López Moriena is our Chief Sustainability Officer, covering the company’s operations in Argentina, Brazil and Uruguay. He has been part of Adecoagro since inception and a member of Adecoagro’s Senior Management since 2019. His responsibilities include defining, developing, evaluating and transferring the most efficient, profitable and sustainable technologies to be applied in each business line and in each region and country. Sustainability responsibilities include the definition of a corporate policy and strategy, its discussion at Board level and the coordination of its implementation at the operational level, in addition to educating and leading sustainable-based technical training programs. Mr. López Moriena has over 20 years of experience in agribusiness development and agricultural production. He has spoken in numerous Ag conferences, with special focus on Sustainability matters, such as The World Food Prize (Iowa, US), Sustainable Food Lab (London, UK), AAPRESID (Rosario, Argentina), World Agri Tech Summit (San Francisco, US), Global AgInvesting (New York, US), Ag Innovation Showcase (St Louis, US) and Large Farm Management (Kiev, Ukraine) among others. Mr. López Moriena holds a Masters degree in Agronomy from the University of Buenos Aires.</p>
</div>
</section>
@endsection