@extends('layouts.app')
@section('title', 'Adecoagro IR - Home')

@section('content')
<div class="img-directors" style="background:url({{ asset('img/governance/insider-trading-policy.jpg') }}) no-repeat center/cover">
<div class="container">
    <h2>Insider Trading Policy</h2>
</div>
</div>
<section class="insider container my-5">
    <p> This Insider Trading Policy provides the standards of Adecoagro S.A. (the "Company") on trading and causing the trading of the Company's securities or securities of other publicly-traded companies while in possession of confidential information. This policy is divided into two parts: the first part prohibits trading in certain circumstances and applies to all directors, alternate directors, officers, employees and consultants of the Company and the second part imposes special additional trading restrictions and applies to all (i) directors of the Company, (ii) officers of the Company and its subsidiaries and (iii) the employees listed on Appendix A (collectively, "Covered Persons").</p>
    <p>One of the principal purposes of the US federal securities laws is to prohibit so-called "insider trading." Simply stated, insider trading occurs when a person uses material non-public information obtained through involvement with the Company to make decisions to purchase, sell, give away or otherwise trade the Company's securities or to provide that information to others outside the Company. The prohibitions against insider trading apply to trades, tips and recommendations by virtually any person, including all persons associated with the Company, if the information involved is "material" and "non-public." These terms are defined in this Policy under Part I, Section 3 below. The prohibitions would apply to any director, officer or employee who buys or sells Company stock on the basis of material non-public information that he or she obtained about the Company, its customers, suppliers, or other companies with which the Company has contractual relationships or may be negotiating transactions.</p>
    <div class="part-one">
        <h4>PART I</h4>
        <h4 class="mt-5">1. Applicability</h4>
        <p>
            <i class="fas fa-circle"></i>
        This Policy applies to all transactions in the Company's securities, including common stock, options and any other securities that the Company may issue, such as preferred stock, notes, bonds and convertible securities, as well as to derivative securities relating to any of the Company's securities, whether or not issued by the Company. This Policy applies to all directors, alternate directors, officers, employees and consultants of the Company and its subsidiaries.
        </p>
        <h4>2. General Policy: No Trading or Causing Trading While in Possession of Material Non-public Information
        </h4>
        <p>
        <i class="fas fa-circle"></i>
        (a). No director, alternate director, officers, employee or consultant may purchase or sell any Company security, whether or not issued by the Company, while in possession of material non-public information about the Company. (The terms "material" and "non-public" are defined in Part I, Section 3(a) and (b) below.)
        </p>
        <p>
        <i class="fas fa-circle"></i>
        (b). No director, alternate director, officers, employee or consultant who knows of any material non-public information about the Company may communicate that information to any other person, including family and friends.
        </p>
        <p>
        <i class="fas fa-circle"></i>
        (c).  In addition, no director, alternate director, officers, employee or consultant may purchase or sell any security of any other company, whether or not issued by the Company, while in possession of material non-public information about that company that was obtained in the course of his or her involvement with the Company. No director, alternate director, officers, employee or consultant who knows of any such material non-public information may communicate that information to any other person, including family and friends.
        </p>
        <p>
        <i class="fas fa-circle"></i>
        (d). For compliance purposes, you should never trade, tip or recommend securities (or otherwise cause the purchase or sale of securities) while in possession of information that you have reason to believe is material and non-public unless you first consult with, and obtain the advance approval of, the Compliance Officer (which is defined in Part I, Section 3(c) below).
        </p>
        <p>
        <i class="fas fa-circle"></i>
        (e). Covered Persons must "pre-clear" all trading in securities of the Company in accordance with the procedures set forth in Part II, Section 3 below.
        </p>
        <h4>3. Definitions</h4>
        <p>
        <i class="fas fa-circle"></i>
        (a) Materiality. Insider trading restrictions come into play only if the information you possess is "material." Materiality, however, involves a relatively low threshold. Information is generally regarded as "material" if it has market significance, that is, if its public dissemination is likely to affect the market price of securities, or if it otherwise is information that a reasonable investor would want to know before making an investment decision.
        Information dealing with the following subjects is reasonably likely to be found material in particular situations:
        (i) significant changes in the Company's prospects;
        (ii) significant write-downs in assets or increases in reserves;
        (iii) developments regarding significant litigation or government agency investigations;
        (iv) potential financial liquidity problems;
        (v) changes in earnings estimates or unusual gains or losses in major operations;
        (vi) changes in management;
        (vii) changes in dividends;
        (viii) extraordinary borrowings;
        (ix) award or loss of a significant contract;
        (x) changes in debt ratings;
        (xi) proposals, plans, agreements or news, even if preliminary in nature, involving mergers, acquisitions, divestitures, recapitalizations, strategic alliances, licensing arrangements, tender offers, purchases or sales of substantial assets or subsidiaries;
        (xii) public offerings;
        (xiii) pending statistical reports (such as, commodity prices, money supply and yield estimates, or interest rate developments); and
        (xiv) changes in dividend policies or the declaration of a share split or the offering of additional securities.
        Tipping occurs when an individual passes material nonpublic information on to others or recommends to anyone the purchase or sale of any securities when they are aware of such information.  This practice also violates the securities laws and can result in the same civil and criminal penalties that apply to insider trading, even though the tipper did not trade and did not gain any benefit from another's trading.
        Material information is not limited to historical facts but may also include projections and forecasts. With respect to a future event, such as a merger, acquisition or introduction of a new product, the point at which negotiations or product development are determined to be material is determined by balancing the probability that the event will occur against the magnitude of the effect the event would have on a company's operations or stock price should it occur. Thus, information concerning an event that would have a large effect on stock price, such as a merger, may be material even if the possibility that the event will occur is relatively small. When in doubt about whether particular non-public information is material, presume it is material. If you are unsure whether information is material, you should consult the Compliance Officer before making any decision to disclose such information (other than to persons who need to know it) or to trade in or recommend securities to which that information relates.
        </p>
        <p>
        <i class="fas fa-circle"></i>
        (b) Non-public Information. Insider trading prohibitions come into play only when you possess information that is material and "non-public." The fact that information has been disclosed to a few members of the public does not make it public for insider trading purposes. To be "public" the information must have been disseminated in a manner designed to reach investors generally, and the investors must be given the opportunity to absorb the information. Even after public disclosure of information about the Company, you must wait until the close of business on the second trading day after the information was publicly disclosed before you can treat the information as public.
        Non-public information may include:
        (i) information available to a select group of analysts or brokers or institutional investors;
        (ii) undisclosed facts that are the subject of rumors, even if the rumors are widely circulated; and
        (iii) information that has been entrusted to the Company on a confidential basis until a public announcement of the information has been made and enough time has elapsed for the market to respond to a public announcement of the information (normally two or three days).
        </p>
        <h4>As with questions of materiality, if you are not sure whether information is considered public, you should either consult with the Compliance Officer or assume that the information is "non-public" and treat it as confidential.</h4>
        <p><i class="fas fa-circle"></i>
        c) Compliance Officer. The Company has appointed Emilio Gnecco, who may be contacted at +(54 11) 4836–8680,  as the Compliance Officer for this Policy. The duties of the Compliance Officer include, but are not limited to, the following:
        (i) assisting with implementation of this Policy;
        (ii) circulating this Policy to all employees and ensuring that this Policy is amended as necessary to remain up-to-date with insider trading laws;
        (iii) pre-clearing all trading in securities of the Company by Covered Persons in accordance with the procedures set forth in Part II, Section 3 below; and
        (iv) providing approval of any transactions under Part II, Section 4 below.</p>
        <h4>
        4. Violations of Insider Trading Laws
        </h4>
        <p>
        Penalties for trading on or communicating material non-public information can be severe, both for individuals involved in such unlawful conduct and their employers and supervisors, and may include jail terms, criminal fines, civil penalties and civil enforcement injunctions. Given the severity of the potential penalties, compliance with this Policy is absolutely mandatory.
        </p>
        <p>
        <i class="fas fa-circle"></i>
        (a) Legal Penalties. A person who violates insider trading laws by engaging in transactions in a company's securities when he or she has material non-public information can be sentenced to a substantial jail term and required to pay a penalty of several times the amount of profits gained or losses avoided.In addition, a person who tips others may also be liable for transactions by the tippees to whom he or she has disclosed material non-public information. Tippers can be subject to the same penalties and sanctions as the tippees, and the SEC has imposed large penalties even when the tipper did not profit from the transaction.The SEC can also seek substantial penalties from any person who, at the time of an insider trading violation, "directly or indirectly controlled the person who committed such violation," which would apply to the Company and/or management and supervisory personnel. These control persons may be held liable for up to the greater of $1 million or three times the amount of the profits gained or losses avoided. Even for violations that result in a small or no profit, the SEC can seek a minimum of $1 million from a company and/or management and supervisory personnel as control persons.
        </p>
        <p>
        <i class="fas fa-circle"></i>
        (b) Company-imposed Penalties. Employees who violate this Policy may be subject to disciplinary action by the Company, including dismissal for cause. Any exceptions to the Policy, if permitted, may only be granted by the Compliance Officer and must be provided before any activity contrary to the above requirements takes place.
        </p>
    </div>
    <div class="part-two">
        <h4>PART II</h4>
        <h4 class="mt-5">1. Blackout Periods</h4>
        <p>All Covered Persons are prohibited from trading in the Company's securities during blackout periods.</p>
        <p><i class="fas fa-circle"></i>
            Quarterly Blackout Periods. Trading in the Company's securities is prohibited during the two-week period ending on the close of business on the day following the date the Company’s quarterly financial results are publicly disclosed and Form 6-K is filed. During these periods, Covered Persons generally possess or are presumed to possess material non-public information about the Company's financial results. You will be informed at other times when a trading moratorium is in effect because of the existence of material non-public information concerning the Company. Nonetheless, you are still required to review any trading activity in advance with the Compliance Officer.
        </p>
        <p><i class="fas fa-circle"></i>
        Other Blackout Periods. From time to time, other types of material non-public information regarding the Company (such as negotiation of mergers, acquisitions or dispositions or new product developments) may be pending and not be publicly disclosed. While such material non-public information is pending, the Company may impose special blackout periods during which Covered Persons are prohibited from trading in the Company's securities. If the Company imposes a special blackout period, it will notify the Covered Persons affected.
        </p>
        <h4>3. Pre-clearance of Securities Transactions</h4>
        <p><i class="fas fa-circle"></i>
        (a). Because Covered Persons are likely to obtain material non-public information on a regular basis, the Company requires all such persons to refrain from trading, even during a trading window under Part II, Section 2 above, without first pre-clearing all transactions in the Company's securities.
        </p>
        <p><i class="fas fa-circle"></i>
        (b). Subject to the exemption in subsection (d) below, no Covered Person may, directly or indirectly, purchase or sell (or otherwise make any transfer, gift, pledge or loan of) any Company security at any time without first obtaining prior approval from the Compliance Officer. These procedures also apply to transactions by such person's spouse, other persons living in such person’s household and minor children and to transactions by entities over which such person exercises control.
        </p>
        <p><i class="fas fa-circle"></i>
        (c). The Compliance Officer shall record the date each request is received and the date and time each request is approved or disapproved. Unless revoked, a grant of permission will normally remain valid until the close of trading two business days following the day on which it was granted. If the transaction does not occur during the two-day period, pre-clearance of the transaction must be re-requested.
        </p>
        <h4>4. Prohibited Transactions</h4>
        <p><i class="fas fa-circle"></i>
        (a). Directors and officers of the Company are prohibited from, trading in the Company's equity securities during a blackout period imposed under an "individual account" retirement or pension plan of the Company, during which at least 50% of the plan participants are unable to purchase, sell or otherwise acquire or transfer an interest in equity securities of the Company, due to a temporary suspension of trading by the Company or the plan fiduciary.
        </p>
        <p><i class="fas fa-circle"></i>
        (b). A Covered Person, including such person's spouse, other persons living in such person's household and minor children and entities over which such person exercises control, is prohibited from engaging in the following transactions in the Company's securities unless advance approval is obtained from the Compliance Officer:
        </p>
        <p><i class="fas fa-circle"></i>
        (i) Short-term trading. Covered Persons who purchase Company securities may not sell any Company securities of the same class for at least six months after the purchase;
        (ii) Short sales. Covered Persons may not sell the Company's securities short;
        (iii) Options trading. Covered Persons may not buy or sell puts or calls or other derivative securities on the Company's securities;
        (iv) Trading on margin. Covered Persons may not hold Company securities in a margin account or pledge Company securities as collateral for a loan; and
        (v) Hedging. Covered Persons may not enter into hedging or monetization transactions or similar arrangements with respect to Company securities.
        </p>
        <h4>5. Acknowledgment and Certification</h4>
        <p>As you can see, the rules governing the purchase and sale of Company securities are complicated and the cost of an inadvertent violation can be very expensive and can result in civil penalties, criminal fines and imprisonment.  The rules apply regardless of how you hold your interest in Company shares, and the special rules applicable to directors, alternate directors, officers, employees and consultants apply to your spouse, minor children and other relatives living in your house.  Remember that the ultimate responsibility for adhering to company policy and the law and avoiding improper transactions rests with you.  It is imperative that you use your best judgment in these matters.</p>
    </div>
</section>
@endsection