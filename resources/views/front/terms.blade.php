@extends('layouts.app')
@section('title', 'Adecoagro IR - Home')

@section('content')
<div class="img-directors" style="background:url({{ asset('img/legal/business-divisions.jpg') }}) no-repeat center/cover">
<div class="container">
    <h2>Term of Use</h2>
</div>
</div>
<section class="legal my-5 container">
<h4>Terms of Use</h4>
<h4>Acceptance of Terms of Use</h4>
<p>Welcome to the website of ADECOAGRO (“Company”, “we” or “us”). The following terms and conditions (together with any documents referred to in them) ("Terms of Use") apply to your use of adecoagro.com, including any content, functionality and services offered on or through adecoagro.com (the “Website”).</p>
<p>Please read the Terms of Use carefully before you start to use the site. By using the Website, you accept and agree to abide by these Terms of Use and our Privacy Policy, incorporated here by reference. If you do not want to agree to these Terms of Use or the Privacy Policy, you must exit the Website.</p>
<h4>Changes to the Terms of Use</h4>
<p class="txt" >We may revise and update these Terms of Use from time to time in our sole discretion. You are expected to check this page from time to time to take notice of any changes we made, as they are binding on you. Your continued use of the Website following the posting of revised Terms of Use means that you accept and agree to the changes.</p>
<h4>Accessing the Website</h4>
<p class="txt">To use this Website, you represent and warrant that you are of legal age to form a binding contract with the Company.</p>
<p class="txt">Access to the Website is permitted on a temporary basis, and we reserve the right to withdraw or amend any service we provide on the Website in our sole discretion without notice. We will not be liable if for any reason all or any part of the Website is unavailable at any time or for any period. From time to time, we may restrict access to some parts of the Website, or the entire Website, to users.</p>
<p class="txt">You are responsible for making all arrangements necessary for you to have access to the Website.</p>
<p class="txt">You are responsible for ensuring that all persons who access the Website through your internet connection are aware of these Terms of Use, and that they comply with them.</p>
<p class="txt">To access the Website or some of the resources it offers, you may be asked to provide certain details or other information. It is a condition of your use of the Website that all the information you provide on the Website is correct, current and complete.</p>
<h4>Intellectual Property Rights</h4>
<p>he entire contents of the Website (including all information, software, text, displays, images, video and audio) and the design, selection and arrangement thereof, are owned by the Company or its licensors and are protected by United States and international laws regarding copyrights, trademarks, trade secrets and other intellectual property or proprietary rights.</p>
<p>You are permitted to use the Website for your personal, non-commercial use only. You must not copy, modify, create derivative works of, publicly display or perform, republish, download or store, or transmit any of the material on our site without the prior written consent of the Company, except to:</p>
<p><i class="fas fa-caret-right"></i>Store copies of such materials temporarily in RAM.</p>
<p><i class="fas fa-caret-right"></i>Store files that are automatically cached by your Web browser for display enhancement purposes.</p>
<p><i class="fas fa-caret-right"></i>Print a reasonable number of pages of the Website for a permitted use.</p>
<p>You must not:</p>
<p><i class="fas fa-caret-right"></i>Modify the paper or digital copies of any materials from this site.</p>
<p><i class="fas fa-caret-right"></i>Use any illustrations, photographs, video or audio sequences or any graphics separately from the accompanying text.</p>
<p><i class="fas fa-caret-right"></i>Delete or alter any copyright, trademark and other proprietary notices appearing on such materials.</p>
<p>You must not reproduce, sell or exploit for any commercial purposes any part of the Website, access to the Website or use of the Website or any services or materials available through the Website without obtaining a license to do so from the Company or its applicable licensor. If you wish to make any use of material on the Website other than that set out above, please address your request to: info@adecoagro.com</p>
<p>If you print, copy, modify, download or otherwise use any part of the Website in breach of the Terms of Use, your right to use the Website will cease immediately and you must, at our option, return or destroy any copies of the materials you have made. No right, title or interest in or to the Website or any content on the site are transferred to you, and all rights not expressly granted are reserved by the Company. Any use of the Website not expressly permitted by these Terms of Use is a breach of these Terms of Use and may violate copyright, trademark and other laws.</p>
<h4>Company Trademarks</h4>
<p>The Company name, the terms (NYSE:AGRO), Adecoagro and all related names, logos, product and service names, designs and slogans are trademarks of the Company or its affiliates or licensors. You must not use such marks without the prior written permission of the Company. All other names, brands and marks are used for identification purposes only and are the trademarks of their respective owners.</p>
<h4>Prohibited Uses</h4>
<p>You may use the Website only for lawful purposes and in accordance with these Terms of Use. You agree not to use the Website:</p>
<p><i class="fas fa-caret-right"></i>In any way that violates any applicable federal, state, local and international law or regulation (including, without limitation, any laws regarding the export of data or software to and from the US or other countries).</p>
<p><i class="fas fa-caret-right"></i>For the purpose of exploiting, harming or attempting to exploit or harm minors in any way by exposing them to inappropriate content, asking for personally identifiable information or otherwise.</p>
<p><i class="fas fa-caret-right"></i>To transmit, or procure the sending of, any advertising or promotional material without our prior written consent, including any “junk mail”, “chain letter” or “spam” or any other similar solicitation.</p>
<p><i class="fas fa-caret-right"></i>To impersonate or attempt to impersonate the Company or a Company employee, another user, or person or entity (including, without limitation, the use of e-mail addresses associated with any of the foregoing).</p>
<p><i class="fas fa-caret-right"></i>To engage in any other conduct that restricts or inhibits anyone's use or enjoyment of the Website, or which, as determined by us, may harm the Company or users of the Website or expose them to liability.</p>
<p><i class="fas fa-caret-right"></i>Additionally, you agree not to:</p>
<p><i class="fas fa-caret-right"></i>Use the Website in any manner that could disable, overburden, damage, or impair the site or interfere with any other party's use of the Website, including their ability to engage in real time activities through the Website.</p>
<p><i class="fas fa-caret-right"></i>Use any robot, spider or other automatic device, process or means to access the Website.</p>
<p><i class="fas fa-caret-right"></i>Use any manual process to monitor or copy any of the material on the Website or for any other unauthorized purpose without our prior written consent.</p>
<p><i class="fas fa-caret-right"></i>Use any device, software or routine that interferes with the proper working of the Website.</p>
<p><i class="fas fa-caret-right"></i>Introduce any viruses, trojan horses, worms, logic bombs or other material which is malicious or technologically harmful.</p>
<p><i class="fas fa-caret-right"></i>Attempt to gain unauthorized access to, interfere with, damage or disrupt any parts of the Website, the server on which the Website is stored, or any server, computer or database connected to the Website.</p>
<p><i class="fas fa-caret-right"></i>Attack the Website via a denial-of-service attack or a distributed denial-of-service attack.</p>
<p><i class="fas fa-caret-right"></i>Otherwise attempt to interfere with the proper working of the Website.</p>
<h4>Reliance on Information Posted</h4>
<p>The information presented on or through the Website is made available solely for general information purposes. We do not warrant the accuracy, completeness or usefulness of this information. Any reliance you place on such information is strictly at your own risk. We disclaim all liability and responsibility arising from any reliance placed on such materials by you or any other visitor to the Website, or by anyone who may be informed of any of its contents.</p>
<h4>Changes to the Website</h4>
<p>We may update the Website from time to time, but its content is not necessarily complete or up-to-date. We may change the Website at any time with or without notice. We may suspend access to the Website, or close it indefinitely. Any of the material on the Website may be out of date at any given time, and we are under no obligation to update such material.</p>
<h4>Information About You and Your Visits to the Website</h4>
<p>We collect and use information about you in accordance with our Privacy Policy. By using the Website, you consent to such collection and use and you represent and warrant that all data provided by you is accurate.</p>
<h4>Linking to the Website</h4>
<p>You may link to our homepage, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it, but you must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part where none exists.</p>
<p>You must not establish a link from any website that is not owned by you.</p>
<p>The Website must not be framed on any other site, nor may you create a link to any part of the Website other than the homepage</p>
<p>You agree to cooperate with us in causing any unauthorized framing or linking immediately to cease. We reserve the right to withdraw linking permission without notice.</p>
<h4>Links from the Website</h4>
<p>If the Website contains links to other sites and resources provided by third parties, these links are provided for your convenience only. This includes links from advertisers, including banner advertisements. We have no control over the contents of those sites or resources, and accept no responsibility for them or for any loss or damage that may arise from your use of them. If you decide to access any of the third party websites linked to this Website, you do so entirely at your own risk and subject to the terms and conditions of use for such websites.</p>
<h4>Disclaimer of Warranties</h4>
<p>You understand that we cannot and do not guarantee or warrant that files available for downloading from the internet or the Website will be free of viruses or other destructive code. You are responsible for implementing sufficient procedures and checkpoints to satisfy your particular requirements for anti-virus protection and accuracy of data input and output, and for maintaining a means external to our site for any reconstruction of any lost data. WE WILL NOT BE LIABLE FOR ANY LOSS OR DAMAGE CAUSED BY A DISTRIBUTED DENIAL-OF-SERVICE ATTACK, VIRUSES OR OTHER TECHNOLOGICALLY HARMFUL MATERIAL THAT MAY INFECT YOUR COMPUTER EQUIPMENT, COMPUTER PROGRAMS, DATA OR OTHER PROPRIETARY MATERIAL DUE TO YOUR USE OF THE WEBSITE OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE OR TO YOUR DOWNLOADING OF ANY MATERIAL POSTED ON IT, OR ON ANY WEBSITE LINKED TO IT.</p>
<p>YOUR USE OF THE WEBSITE, ITS CONTENT AND ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE IS AT YOUR OWN RISK. THE WEBSITE, ITS CONTENT AND ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS, WITHOUT ANY WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. NEITHER THE COMPANY NOR ANY PERSON ASSOCIATED WITH THE COMPANY MAKES ANY WARRANTY OR REPRESENTATION WITH RESPECT TO THE COMPLETENESS, SECURITY, RELIABILITY, QUALITY, ACCURACY OR AVAILABILITY OF THE WEBSITE. WITHOUT LIMITING THE FOREGOING, NEITHER THE COMPANY NOR ANYONE ASSOCIATED WITH THE COMPANY REPRESENTS OR WARRANTS THAT THE WEBSITE, ITS CONTENT OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE WILL BE ACCURATE, RELIABLE, ERROR-FREE OR UNINTERRUPTED, THAT DEFECTS WILL BE CORRECTED, THAT OUR SITE OR THE SERVER THAT MAKES IT AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS OR THAT THE WEBSITE OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE WILL OTHERWISE MEET YOUR NEEDS OR EXPECTATIONS.</p>
<p>THE COMPANY HEREBY DISCLAIMS ALL WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, STATUTORY OR OTHERWISE, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT AND FITNESS FOR PARTICULAR PURPOSE.</p>
<p>THE FOREGOING DOES NOT AFFECT ANY WARRANTIES WHICH CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE LAW</p>
<h4>Limitation on Liability</h4>
<p>IN NO EVENT WILL THE COMPANY, ITS AFFILIATES AND ITS LICENSORS, SERVICE PROVIDERS, EMPLOYEES, AGENTS, OFFICERS OR DIRECTORS BE LIABLE FOR DAMAGES OF ANY KIND, EITHER JOINTLY OR SEVERALLY, UNDER ANY LEGAL THEORY, ARISING OUT OF OR IN CONNECTION WITH YOUR USE, OR INABILITY TO USE, THE WEBSITE, ANY WEBSITES LINKED TO IT, ANY CONTENT ON THE WEBSITE OR SUCH OTHER WEBSITES OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE OR SUCH OTHER WEBSITES, INCLUDING ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, INCLUDING BUT NOT LIMITED TO, PERSONAL INJURY, PAIN AND SUFFERING, EMOTIONAL DISTRESS, LOSS OF REVENUE, LOSS OF PROFITS, LOSS OF BUSINESS OR ANTICIPATED SAVINGS, LOSS OF USE, LOSS OF GOODWILL, LOSS OF DATA, AND WHETHER CAUSED BY TORT (INCLUDING NEGLIGENCE), BREACH OF CONTRACT OR OTHERWISE, EVEN IF FORESEEABLE.</p>
<p>THE FOREGOING DOES NOT AFFECT ANY LIABILITY WHICH CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE LAW.</p>
<h4>Indemnification</h4>
<p>You agree to defend, indemnify and hold harmless the Company, its affiliates and licensors and their respective officers, directors, employees, contractors, agents, licensors and suppliers from and against any claims, liabilities, damages, judgments, awards, losses, costs, expenses or fees (including reasonable attorneys' fees) resulting from your violation of these Terms of Use or your use of the Website, including, without limitation, any use of the Website's content, services and products other than as expressly authorized in these Terms of Use or your use of any information obtained from the Website.</p>
<h4>Waiver and Severability</h4>
<p>No waiver of these Terms of Use by the Company shall be deemed a further or continuing waiver of such term or condition or any other term or condition, and any failure of the Company to assert a right or provision under these Terms of Use shall not constitute a waiver of such right or provision.</p>
<p>If any provision of these Terms of Use is held by a court of competent jurisdiction to be invalid, illegal or unenforceable for any reason, such provision shall be eliminated or limited to the minimum extent such that the remaining provisions of the Terms of Use will continue in full force and effe</p>
<h4>Entire Agreement</h4>
<p>The Terms of Use and our Privacy Policy constitute the sole and entire agreement between you and ADECOAGRO with respect to the Website and supersede all prior and contemporaneous understandings, agreements, representations and warranties, both written and oral, with respect to the Website.</p>
<h4>Your Comments and Concerns</h4>
<p>All other feedback, comments, requests for technical support and other communications relating to the Website should be directed to: info@adecoagro.com</p>
</section>
@endsection