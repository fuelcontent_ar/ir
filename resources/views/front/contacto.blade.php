@extends('layouts.app')
@section('title', 'Adecoagro IR - Home')
<style>

.contact-form{
    --form-ok-color:#4caf50;
    --form-error-color:#f44336;
    margin-left:auto ;
    margin-right: auto;
    width: 80%;
}

.contact-form legend,
.contact-form-response{
    font-size: 1.5rem;
    font-weight: bold;
    text-align: center;
}

.contact-form [required]:valid{/*cuando sea valido*/
    border: thin solid var(--form-ok-color);
}
.contact-form [required]:invalid{/*cuando sea invalido*/
    border: thin solid var(--form-error-color);
}
.contact-form-error{
    margin-top: 1rem;
    font-size: 80%;
    background-color: var(--form-error-color);
    color: #fff;
    transition: all 800ms ease;
}
.contact-form-error.is-active{
    display: block;
    animation: show-message 1s 1 normal 0s ease-out both;
}
@keyframes show-message{
    0%{
        visibility: hidden;
        opacity: 0;
    }
    100%{
        visibility: visible;
        opacity: 1;
    }
}
</style>
@section('content')
<div class="contacto container my-5">
<form  class="form contact-form" id="form">
    <h2>Contact IR</h2>
    <label for="first_name">First name</label>
    <input type="text" name="first_name" title="EL NOMBRE SOLO ACEPTA LETRAS" pattern="^[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]+$"  required>
    <label for="last_name">Last name</label>
    <input type="text" title="EL APELLIDO SOLO ACEPTA LETRAS" name="last_name" pattern="^[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]+$" required>
    <label for="email">Email Adress</label>
    <input type="email" title="email incorrecto" name="email" pattern="^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$" required>
    <label for="phone">Phone number</label>
    <input type="tel" name="phone" required>
    <label for="company">Company</label>
    <input type="text" name="company" required>
    <label for="message">What's your enquiry to the IR team?</label>
    <textarea name="message"></textarea>
    <button type="submit" class="btn">Send Form</button>
    <div class="loader none">
        <img src="{{ asset('img/circles.svg') }}" alt="loader">
    </div>
    <div class="contact-form-response none">
        <p>Los datos han sido enviados</p>
    </div>
</form>
<div class="contact">
    <span>
        <strong>Victoria Cabello</strong>
    </span>
    <span>Investor Relations Manager</span>
    <span>Phone: (5411) 4836-8651</span>
    <span>Email: vcabello@adecoagro.com</span>
    <span class="mt-5">Email: ir@adecoagro.com</span>
</div>
</div>
@endsection
<!-- <script type="text/javascript">
    window.onload = function () {
        const form = document.querySelector('.form'),
  loader = document.querySelector(".loader")
  
  form.addEventListener("submit",(e)=>{
    e.preventDefault();
    loader.classList.remove("none");
  
  fetch("/api/contact",{
    method:"POST",
    body:new FormData(e.target)
  })
  .then(res => res.json())
  .then(json => {
  loader.classList.add("none");
    alert("Mensaje Enviado")
  })
  .catch(err =>{
    alert("Error: Ingrese bien los datos");
    loader.classList.add("none");
  }) 
  }) 
    }
</script> -->
<script>
        const d = document;
        function contactForm(){
            const $form =d.querySelector(".contact-form"),
                $inputs = d.querySelectorAll(".contact-form [required]");
            $inputs.forEach(input => {
                const $span = d.createElement("span");
                $span.id = input.name;
                $span.textContent = input.title;
                $span.classList.add("contact-form-error","none");
                input.insertAdjacentElement("afterend",$span);
            });
        d.addEventListener("keyup", (e) =>{
                if(e.target.matches(".contact-form [required]")){
                    let $input = e.target,
                        pattern = $input.pattern || $input.dataset.pattern; 
                    if(pattern && $input.value !== ""){
                        let regex = new RegExp(pattern); 
                        return !regex.exec($input.value)
                            ? d.getElementById($input.name).classList.add("is-active")
                            : d.getElementById($input.name).classList.remove("is-active");
                    }
                    if(!pattern){
                        return $input.value === ""
                            ? d.getElementById($input.name).classList.add("is-active")
                            : d.getElementById($input.name).classList.remove("is-active");
                    }
                }
            });
        d.addEventListener("submit",(e) =>{
            e.preventDefault();
            alert("enviando formulario");
            const $loader = d.querySelector(".contact-form-loader"),
            $response = d.querySelector(".contact-form-response");
            $loader.classList.remove("none");

            
            fetch("/api/contact",{
                method:"POST",
                body: new FormData(e.target)
            })
            .then(res => res.ok ? res.json(): Promise.reject(res))
            .then(json => {
                $loader.classList.add("none");
                $response.classList.remove("none");
                //$response.innerHTML = `<p>${json.message}</p>`;
                $form.reset();
            })
            .catch(err =>{
                let message = err.statusText || "Ocurrio un error al enviar";
                $response.innerHTML = `<p>Error ${err.status}: ${message} </p>`
            })
            .finally(() => setTimeout(() => {
                $response.classList.add("none");
                $response.innerHTML = "";
            }, 3000));
        });
        d.addEventListener('DOMContentLoaded', (e) =>{
        contactForm();
        });
        }
    </script>