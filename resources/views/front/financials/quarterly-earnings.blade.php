@extends('layouts.app')
@section('title', 'Adecoagro IR - Quarterly Earnings')

@section('content')
<div class="img-directors" style="background:url({{ asset('img/financials/header_financials.png') }}) no-repeat center/cover">
<div class="statements">
    <div class="financial">
        <a href="" id="statements">
        Financial <br> Statements
        </a>
    </div>
    <div class="release">
        <a href="" id="release">
            Earnings <br> Release
        </a>
    </div>
    </div>
<div class="container">
    <h2>
        <a href="" id="call">
            Conference Call
        </a>
    </h2>
</div>
</div>
<section class="container my-5">
    <div class="quartely  mb-5" id="quartely"></div>
    <div class="row mb-4">
      <span id="anio"></span>
    </div>
    <div class="earnings" id="earnings">
      <div class="group_quar">
        <h4>Q4</h4>
        <div class="quar" id="q4"></div>
      </div>
      <div class="group_quar">
        <h4>Q3</h4>
        <div class="quar" id="q3"></div>
      </div>
      <div class="group_quar">
        <h4>Q2</h4>
        <div class="quar" id="q2"></div>
      </div>
      <div class="group_quar">
        <h4>Q1</h4>
        <div class="quar" id="q1"></div>
      </div>
    </div>
    <div id="years"></div>
    <div class="my-4">
      <blockquote style="color:#999">Some of the information or materials made available on this website may contain forward-looking statements. Statements including words such as "believe," "may," "will," "estimate," "continue," "anticipate," "intend," "expect" or similar expressions are intended to identify forward-looking statements. These forward-looking statements are subject to assumptions, risks and uncertainties that could cause actual events or actual future results to differ materially from the expectations set forth in the forward-looking statements. Some of the factors which could cause Adecoagro’s results to differ materially from its expectations include, but are not limited to, those described in detail in the Risk Factors page. Shareholders may receive a hard copy of the Company's 2018 Annual Report and Audited Consolidated Financial Statements free of charge by requesting a copy within a reasonable period of time from Adecoagro's Investor Relations office, at ir@adeacoagro.com </blockquote>
    </div>
    <div class="filings">
        <h2>SEC Filings</h2>
        <select name="" id="filings" onclick="filings()">

        </select>
    </div>
    <table class="table table-striped">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Filling Type</th>
                    <th>Description</th>
                    <th>View</th>
                </tr>
            </thead>
            <tbody id="info-filings">

            </tbody>
    </table>
    <div class="cvm mt-5" >
        <h2>Information sent to CVM</h2>
        <select name="" id="cvm" onclick="cvm()">
        </select>
    </div>
    <table class="table table-striped">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Description</th>
                    <th>View</th>
                </tr>
            </thead>
            <tbody id="info-cvm">

            </tbody>
    </table>
</section>
@endsection

<script type="text/javascript">

 /********************quartely***************************************** */
window.onload = function() {
  const year = new Date().getFullYear();
  const mes = new Date().getMonth();
  const years = document.getElementById('years');
  const quart1 = document.getElementById('q1');
  const quart2 = document.getElementById('q2');
  const quart3 = document.getElementById('q3');
  const quart4 = document.getElementById('q4');
  const quartely = document.getElementById('quartely');
  

  $('#anio').text(year);
 

  function mostrarResultados(year)
  {
    axios.get(`/api/quarterlyEarnings?year=${year}`)
    .then(response => {
      let q1 = [];
      let q2 = [];
      let q3 = [];
      let q4 = [];

      response.data['1Q'].map(item =>
      q1.push(item)
      )
      response.data['2Q'].map(item =>
      q2.push(item)
      )
      response.data['3Q'].map(item =>
      q3.push(item)
      )
      response.data['4Q'].map(item =>
      q4.push(item)
      )

      if(q1.length > 0){
        quart1.innerHTML =  q1.map( el =>{
          const subs = el.file.split(".")
          let ultimo = subs[subs.length-1]
          const render = `
          <div className="quar_item">
            <img src="/img/icons/${ultimo}.png" alt="">
            <a class="link-quarte" href="${el.file}">${el.title}</a>
          </div>
          `
          return render;
        }).join('')
      }else {
        $('#q1').parent().addClass("ocultar")
      }

      if(q2.length > 0){
        quart2.innerHTML =  q2.map( el =>{
          const subs = el.file.split(".")
          let ultimo = subs[subs.length-1]
          const render = `
          <div className="quar_item">
            <img src="/img/icons/${ultimo}.png" alt="">
            <a class="link-quarte" href="${el.file}">${el.title}</a>
          </div>
          `
          return render;
        }).join('')
      }else {
        $('#q2').parent().addClass("ocultar")
        $('#q1').parent().addClass("quarSelected")
      }

      if(q3.length > 0){
        quart3.innerHTML =  q3.map( el =>{
          const subs = el.file.split(".")
          let ultimo = subs[subs.length-1]
          const render = `
          <div className="quar_item">
            <img src="/img/icons/${ultimo}.png" alt="">
            <a class="link-quarte" href="${el.file}">${el.title}</a>
          </div>
          `
          return render;
        }).join('')
      }else {
        $('#q3').parent().addClass("ocultar")
        $('#q2').parent().addClass("quarSelected")
      }


      if(q4.length > 0 ){
        quart4.innerHTML =  q4.map( el =>{
          const subs = el.file.split(".")
          let ultimo = subs[subs.length-1]
          const render = `
          <div className="quar_item">
            <img src="/img/icons/${ultimo}.png" alt="">
            <a class="link-quarte" href="${el.file}">${el.title}</a>
          </div>
          `
          $('#q4').parent().addClass("quarSelected")
          return render;
        }).join('')
      }else {
        $('#q4').parent().addClass("ocultar")
        $('#q3').parent().addClass("quarSelected")
      }

    })

  }


  function mostrarAnios()
  {
    axios.get(`/api/quarterlyEarnings?getYears=1`)
    .then(res => years.innerHTML = res.data.map(el =>{
      let render = `
      <button class="listado_anios" type="button">
        ${el.year}
      </button>
      `
      return render;
    }).join(''))
  }



  $('body').on('click','.listado_anios',function(){
    if ($(".group_quar").hasClass("quarSelected")) {
      $('.group_quar').removeClass("quarSelected")
    }
    if ($(".group_quar").hasClass("ocultar")) {
      $('.group_quar').removeClass("ocultar")
    }
    let anio_renderiza = $(this).text().trim();
    $('#anio').text(anio_renderiza);
    mostrarResultados(anio_renderiza)
  });

function getEntrevistas() {
  fetch('/api/entrevistas')
.then(res => res.json())
.then(data =>  quartely.innerHTML = `
<div class="content">
    <p>${data.content}</p>
    <h4 class="mt-4">${data.footer}</h4>
     <span>${data.date}</span>
</div>
    <img src="${data.file}" alt="">
` );
}
  mostrarResultados(year);
  mostrarAnios();
  getEntrevistas();

  /*******************financial********* */
  const statements = document.getElementById('statements');
  const release = document.getElementById('release');
  const call = document.getElementById('call');
fetch('/api/quarterlyEarnings?lastQuarter=1')
.then(res => res.json())
.then(data =>data.map(el => {
  if (el.title == 'Conference Call') {
    call.href = el.file;
  }
  if (el.title == 'Financial Statements') {
    statements.href = el.file;
  }
  if (el.title == 'Earning Release') {
    release.href = el.file;
  }
}))

/********************años filings***************************************** */
function filingsFilter() {
  axios.get('/api/SECFilings')
    .then(response => {
     let array = [];
     
      response.data.map(el =>
      {
        let filingsanio = el.date.split('-');
        array.push(filingsanio[2])
      }
      )
      let result = array.filter((item,index)=>{
      return array.indexOf(item) === index;
    })
  return result
  })
    .then(elem => elem.map(element =>  `<option value="${element}">${element}</option>` ))
    .then(elementos => 
    {const secFilings = document.getElementById('filings');
    secFilings.innerHTML = elementos}
    )
}
filingsFilter()

/********************años cvm***************************************** */
function cvmFilter() {
  axios.get('/api/cvm')
    .then(response => {
     let array = [];
     
      response.data.map(el =>
      {
        let cvmanio = el.date.split('-');
        array.push(cvmanio[2])
      }
      )
      let result = array.filter((item,index)=>{
      return array.indexOf(item) === index;
    })
  return result
  })
    .then(elem => elem.map(element =>  `<option value="${element}">${element}</option>` ))
    .then(elementos => 
    {const cvm = document.getElementById('cvm');
      cvm.innerHTML = elementos}
    )
}
cvmFilter()

};
/********************sec filings***************************************** */
function filings() {
  const secFilings = document.getElementById('filings');
  const infoFilings = document.getElementById('info-filings');
  const valueFilings = secFilings.value;
  fetch(`/api/SECFilings?year=${valueFilings}`)
.then(res => res.json())
.then(data => data.map(el =>
`<tr>
<td>${el.date}</td>
<td>${el.type}</td>
<td>${el.description}</td>
<td> <a href="${el.link}" target="_blank">See more</a></td>
</tr>
` 
).join(''))
.then(elem => infoFilings.innerHTML = elem)
}

/********************Information cvm***************************************** */
function cvm() {
  const cvm = document.getElementById('cvm');
  const infoCvm = document.getElementById('info-cvm');
  const valueCvm = cvm.value;
  fetch(`/api/cvm?year=${valueCvm}`)
.then(res => res.json())
.then(data => data.map(el =>
`<tr>
<td>${el.date}</td>
<td>${el.description}</td>
<td> <a href="${el.file}" target="_blank">See more</a></td>
</tr>
` 
).join(''))
.then(elem => infoCvm.innerHTML = elem)
}



</script>

