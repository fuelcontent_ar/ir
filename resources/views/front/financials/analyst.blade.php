@extends('layouts.app')
@section('title', 'Adecoagro IR - Quarterly Earnings')

@section('content')
<section class="container analyst">
    <div class="analyst-title">
        <h3 class="my-4">Analyst Coverage</h3>
        <h4>Equity</h4>
    </div>
    <div class="table">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>FIRM</th>
                    <th>ANALYST</th>
                    <th>PHONE</th>
                    <th>EMAIL</th>
                    <th>RECOMMENDATION</th>
                    <th>TP</th>
                    <th>DATE</th>
                </tr>
            </thead>
            <tbody  id="info-table">

            </tbody>
        </table>
    </div>
    <p class="legales my-5">
    Adecoagro is covered by the analyst(s) listed above. Please note that any opinions, estimates or forecasts regarding Adecoagro?s performance made by these analysts are theirs alone and do not represent opinions, forecasts or predictions of Adecoagro or its management. Adecoagro does not by its reference above or distribution imply its endorsement of or concurrence with such information, conclusions or recommendations.
    </p>
    <div class="analyst-title">
        <h4 class="my-4">Fixed Income</h4>
    </div>
    <div class="table mb-5">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>FIRM</th>
                    <th>ANALYST</th>
                    <th>PHONE</th>
                    <th>EMAIL</th>
                </tr>
            </thead>
            <tbody  id="info-fixed">

            </tbody>
        </table>
    </div>
</section>
@endsection