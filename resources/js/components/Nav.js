const d = document,
subcompany = d.querySelector(".sub-company"),
company = d.querySelector(".company"),
subgovernance = d.querySelector(".sub-governance"),
governance = d.querySelector(".governance"),
subfinancials = d.querySelector(".sub-financials"),
financials = d.querySelector(".financials"),
hambur = d.getElementById('hambur'),
listMenu = d.getElementById('menu'),
sub = d.getElementById('sub'),
submenuCompany = d.getElementById('submenuCompany'),
submenuGovernance = d.getElementById('submenuGovernance'),
submenuFinancials = d.getElementById('submenuFinancials')

if (screen.width >= 992 ) {
    company.addEventListener('click',(e)=>{
        e.preventDefault()
            subcompany.classList.toggle('ocultar');
            sub.innerHTML = `
            <ul class="container">
            <li><a href="/company/strategy">Strategy</a></li>
            <li><a href="/company/organizational">Organizational Structure</a></li>
            <li><a id="insti" href="/storage/files/ipresentation/Adecoagro Institutional Presentation.pdf" download>Institutional Presentation</a></li>
            <li><a href="/company/business">Business Divisions</a></li>
            <li><a href="/company/education">Investor Education</a></li>
            <li><a href="/company/investor">Investor Day</a></li>
            <li><a href="/storage/files/our_numbers/Our_Numbers_2020_FV_PV.xlsx">Our Numbers</a></li>
        </ul>
            `
        })
        governance.addEventListener('click',(e)=>{
        e.preventDefault()
        subcompany.classList.toggle('ocultar');
        sub.innerHTML = `    
        <ul class="container">
        <li><a href="/governance/board-of-directors">Board of Directors</a></li>
        <li><a href="/governance/officers">Executive Officers</a></li>
        <li><a href="/governance/policy">Dividend policy</a></li>
        <li><a href="/governance/bylaws">Bylaws</a></li>
        <li><a href="/governance/ethics">Code of Ethics</a></li>
        <li><a href="/governance/insider">Insider Trading Policy</a></li>
        <li><a href="/governance/annual">Annual General Meeting</a></li>
        </ul>`
        })
        financials.addEventListener('click',(e)=>{
        e.preventDefault()
        subcompany.classList.toggle('ocultar');
        sub.innerHTML = `<ul class="container">
        <li><a href="/financials/quarterly-earnings">Quarterly Earnings</a></li>
        <li><a href="/financials/quarterly-earnings#filings">SEC Filings</a></li>
        <li><a href="/financials/analyst">Analyst Coverage</a></li>
        <li><a href="/financials/quarterly-earnings#cvm">CVM</a></li>
        </ul>`
        })
}
if(screen.width <= 991){
    company.addEventListener('click',(e)=>{
        e.preventDefault();
        submenuCompany.classList.toggle('ocultar')
    })
    governance.addEventListener('click',(e)=>{
        e.preventDefault();
        submenuGovernance.classList.toggle('ocultar')
    })
    financials.addEventListener('click',(e)=>{
        e.preventDefault();
        submenuFinancials.classList.toggle('ocultar')
    })
}
hambur.addEventListener('click',(e)=>{
    e.preventDefault()
    listMenu.classList.toggle('ocultarmenu')
})
