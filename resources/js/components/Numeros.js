const num = document.getElementById('num');

 fetch('/api/numeros')
.then(res => res.json())
.then(data => data.map(el =>
    `
    <div class="my-5">
    <img src="/img/home/numeros/${el.id}.png" alt="${el.title}">
    <span class="numero">${el.number}</span>
    <span>${el.title}</span>
    </div>
    `
 ))
 .then( element =>{const cadena = new String(element)
    const sincomilla = cadena.replace(/,/g,"")
    return sincomilla;})
 .then(elem => num.innerHTML = elem);
 