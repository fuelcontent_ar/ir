const infoTable = document.getElementById('info-table');
const infoFixed = document.getElementById('info-fixed');


fetch('/api/acequity')
.then(res => res.json())
.then(data => data.map(el =>`
        <tr>
          <td>
          ${el.firm}
          </td>
          <td>
          ${el.analyst}
          </td>
          <td>
          ${el.phone}
          </td>
          <td>
          ${el.email}
          </td>
          <td>
          ${el.recommendation}
          </td>
          <td>
          ${el.tp}
          </td>
          <td>
          ${el.date}
          </td>
        </tr>
        `

))
.then(elem => {
  const cadena = new String(elem)
  const sincomilla = cadena.replace(/,/g,"")
  return sincomilla;
})
.then(item => infoTable.innerHTML = item);



fetch('/api/acfixedincome')
.then(res => res.json())
.then(data => data.map(el =>`
        <tr>
          <td>
          ${el.firm}
          </td>
          <td>
          ${el.analyst}
          </td>
          <td>
          ${el.phone}
          </td>
          <td>
          ${el.email}
          </td>
        </tr>
        `

))
.then(elem => {
  const cadena = new String(elem)
  const sincomilla = cadena.replace(/,/g,"")
  return sincomilla;
})
.then(item => infoFixed.innerHTML = item);
