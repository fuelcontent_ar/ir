const assetsData = document.getElementById('assets-data')
const array = ['Report','PDF']
fetch('/api/ourAssets')
.then(res => res.json())
.then(data => data.map(el => 
    `  <div class="my-4">
    <img src="/img/icons/${el.id}.png" alt="${el.title}">
    <p>${el.title}</p>
    <a href="${el.file}">Download ${el.id == '1' ? array[0] : array[1]}<i class="fas fa-arrow-down"></i>  </a>
    </div>`
    ).join(''))
.then(elem => assetsData.innerHTML = elem);