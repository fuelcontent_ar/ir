const annual = document.getElementById('annual');

fetch('/api/annualGeneralMeetings')
.then(res => res.json())
.then(data => data.map(el => 
    `

    <a href="${el.file}" download class="my-4"><i class="fas fa-caret-right"></i>${el.title}</a>

    `
))
.then(elem => {
    const cadena = new String(elem)
    const sincomilla = cadena.replace(/,/g,"")
    return sincomilla;
  })
.then(element => annual.innerHTML = element);