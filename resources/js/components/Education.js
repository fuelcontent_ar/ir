const education = document.getElementById('education')

axios.get('/api/investorEducation')
 .then(res => res.data.map(el =>{
    let video = false ; 
    let file = false;
    if(el.video.length > 0 ){
       video = "https://vimeo.com/"+el.video;
    }
    if(el.file.length > 0 ){
       file = el.file;
    }
    const result = `
    <div class="dataEducation my-5 content">
    <div class="data-img">
    <img src="${el.thumb}" alt="">
    </div>
     <div class="data-info">
        <h3>${el.title}</h3>
        <p class="py-3">${el.description}</p>
        ${(video != false) ? '<a href="'+video+'"><img src="/img/icons/video.png" alt="">Play</a>' : ''}
        ${(file != false) ? '<a href="'+file+'"><img src="/img/icons/xls.png" alt="">Download xls</a>' : ''}
    </div>
   </div>
    `
    return result;
 }).join(''))
 .then(elem => education.innerHTML = elem)



