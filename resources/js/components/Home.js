const productos = document.getElementById('productos'),
business = document.getElementById('business'),
investor = document.getElementById('investor'),
newsroom = document.getElementById('newsroom'),
quarter = document.getElementById('quarter'),
unit = document.getElementById('unit')


fetch('/api/headers')
.then(res => res.json())
.then(data => data.map(el =>  `
        <div style="background:url(${el.image}) no-repeat center/cover;background-attachment:fixed;">
         <div class="crops container">
   <h3 class="my-5">${el.title}</h3>
   <div class="my-5">
   <p>${el.text}</p>
   </div>
</div>
</div>`
))
.then(element => productos.innerHTML = element);

fetch('/api/graficos')
.then(res => res.json())
.then(data => data.map(el => `
        <div class="grafic">
           <h2>${el.title}</h2>
           <img src="${el.file}" alt="graficos">
        </div> `
))
.then(element => business.innerHTML = element);

fetch('/api/noticias')
.then(res => res.json())
.then(data => data.map(el =>`
    <a href="${el.link}">
    <i class="fas fa-caret-right"></i>
       <span>${el.date}</span>
       <p>${el.title}</p>
    </a>
`))
.then( element =>{const cadena = new String(element)
        const sincomilla = cadena.replace(/,/g,"")
        return sincomilla;})
.then(elem => newsroom.innerHTML = elem)


fetch('/api/quarterlyEarnings?lastQuarter=1')
.then(res => res.json())
.then(data => data.map(el => {
        const subs = el.file.split(".")
        let ultimo = subs[subs.length-1] 
        const render = `<div class="my-3 render">
        <img src="/img/icons/${ultimo}.png" alt="">
        <a href="${el.file}">${el.title}</a>
        <div>
        `;
        return render;
}))
.then( element =>{const cadena = new String(element)
        const sincomilla = cadena.replace(/,/g,"")
        return sincomilla;})
.then(elem => quarter.innerHTML = elem)

fetch('/api/factsheets')
.then(res => res.json())
.then(data => unit.href = `${data.file}`)
